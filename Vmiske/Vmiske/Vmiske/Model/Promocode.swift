//
//  Promocode.swift
//  Vmiske
//
//  Created by Roman Haiduk on 06.01.2020.
//  Copyright © 2020 Noty Team. All rights reserved.
//

struct Promocode: Codable {
    let name: String
    private let type: String
    let summary: Double
    let minimumSummary: Double
    
    var `Type`: PromocodeType {
        return PromocodeType(rawValue: type) ?? .None
    }
    
    enum CodingKeys: String, CodingKey {
        case name, type
        case summary = "summ"
        case minimumSummary = "minsumm"
    }
}

enum PromocodeType: String {
    case MinusSum = "F"
    case Percantage = "P"
    case None
}
