//
//  HeaderSection.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//


struct HeaderSection {
    var name: String
    var items: [String]
    var collapsed: Bool
    
    init(name: String, items: [String], collapsed: Bool = false) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}
