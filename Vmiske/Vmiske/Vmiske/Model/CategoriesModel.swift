//
//  CategoriesModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct Category: Codable {
    let id: Int
    let name: String
    let image: String
    let subCategoriesCount: Int
    let productCount: Int
    
    var ImagePath: String {
        return ConstantProject.Network.BASE_IMAGE_URL + image
    }
    
    enum CodingKeys:String, CodingKey {
        case id = "cId"
        case name
        case image = "img"
        case subCategoriesCount = "pcount"
        case productCount = "prodcount"
    }
}
