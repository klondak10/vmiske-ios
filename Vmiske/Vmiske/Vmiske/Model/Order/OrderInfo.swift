//
//  OrderInfo.swift
//  Vmiske
//
//  Created by Roman Haiduk on 09.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct OrderInfo: Codable{
    let address: String
    let paytype: String
    let delivery: String
    let ordId: Int
    let status: String
    let date: String
    let total: String
    let ambar: Int
    let ambars: [AmbarInfo]
    let products: [OrderInfo.Product]
    
    var Ambar: Ambar {
        return ambar == 1 ? .yes : .no
    }
    
    struct Product: Codable {
        let orderId: Int
        let productId: Int
        let orderProductId: Int
        let name: String
        let quantity: Int
        let price: String
        let totalPrice: String
        let tax: String?
        let reward: Int
        let image: String
        let inAmarId: Int
        
        var Image: String {
            return (ConstantProject.Network.BASE_IMAGE_URL + image).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        }
        
        enum CodingKeys: String, CodingKey {
            case orderId = "order_id"
            case productId = "product_id"
            case orderProductId = "order_product_id"
            case totalPrice = "total"
            case inAmarId = "inAmbar"
            case name, quantity, price, reward, image, tax
        }
    }
    
    struct AmbarInfo: Codable {
        
        let info: Info
        let products: [Product]
        
        struct Info: Codable {
            let ambarId: Int
            let orderId: Int
            let date: String
            let customerText: String?
            let adminText: String?
            let status: String?
            let sent: Int
            let sentError: Int
            
            enum CodingKeys: String, CodingKey {
                case ambarId  = "ambar_id"
                case orderId  = "order_id"
                case date  = "date"
                case customerText  = "txt_for_customer"
                case adminText  = "txt_for_admin"
                case status  = "status"
                case sent  = "sent"
                case sentError  = "error_sent"
            }
        }
        struct Product: Codable {
            let name: String
            let price: Int
            let product_id: String
            let order_product_id: Int
            let image: String
            let quantity: Int
            
            var Image: String {
                return (ConstantProject.Network.BASE_IMAGE_URL + image).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            }
        }
    }
}
