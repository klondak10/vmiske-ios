//
//  OrderHelpTypes.swift
//  Vmiske
//
//  Created by Roman Haiduk on 09.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct Region: Codable {
    
    let name: String
    let area: String
    let zone_id: String
}

struct City: Codable {
    let name: String
    let city_id: String
    let Ref: String
}

struct NewPost: Codable {
    let name: String
    let Ref: String
}

struct DeliveryPaymentType: Codable {
    let type: String
    let name: String
}

struct CreateOrderResponse: Codable {
    let status: Bool
    let data: Data?
    
    struct Data: Codable {
        let orderId: Int
        let amount: Int
        let currency: String
        let description: String
        let phone: String
        let payWay: String
        
        enum CodingKeys:String, CodingKey {
               case orderId = "order_id"
               case amount, currency, description
               case phone = "default_phone"
               case payWay = "pay_way"
           }
      
        var toDictionary: [String: Any] {
            return [
                "currency": currency,
                "amount": amount,
                "description": description,
                "version": 3,
                "action": "pay",
                "order_id": orderId
            ]
        }
    }
}
