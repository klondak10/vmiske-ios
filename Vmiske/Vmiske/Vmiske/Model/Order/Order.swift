//
//  Order.swift
//  Vmiske
//
//  Created by Roman Haiduk on 09.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct Order: Codable {
    let ordId: Int
    let status: String?
    let date: String
    let total: String
    let ambar: Int
    let order_status: Int?
    let payment_code: String?
    
    var Ambar: Ambar {
        return ambar == 1 ? .yes : .no
    }
}

