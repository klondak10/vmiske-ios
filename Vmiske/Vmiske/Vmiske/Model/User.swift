//
//  User.swift
//  Vmiske
//
//  Created by Roman Haiduk on 25.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation


struct User: Codable {
    
    let firstName: String
    let lastName: String
    let email: String
    let phone: String

    let status: String
    let bonus: String?
    let vogakId: String?
    let vogakName: String?
    
    
    enum CodingKeys: String, CodingKey {

        case firstName = "firstname"
        case lastName = "lastname"
        case phone = "telephone"
    
        case vogakName = "vogak"
        case vogakId = "vogak_id"
        
        case email, status, bonus
    }
}
