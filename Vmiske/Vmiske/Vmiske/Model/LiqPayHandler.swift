//
//  LiqPayHandler.swift
//  Vmiske
//
//  Created by Roman Haiduk on 11.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import UIKit

class LiqPayManager: NSObject, LiqPayCallBack {
    
    private let privatKey = "4ef3Gr1GsVPKEdezpfrdUng8E6kCHtr19HjwaYz1"
    private let publicKey = "i82408937272"
    
    private let navController: UINavigationController
    private let statusBarColor: UIColor
    
    private let params: [String: Any]
    private let complition: (Bool) -> ()
    
    
    lazy var liqpayMob: LiqpayMob = {
        LiqpayMob(liqPayWithDelegate: self)
    }()
    
    init(navController: UINavigationController,
         statusBarColor: UIColor, parameters: [String: Any], complition: @escaping (Bool) -> () ) {
        self.navController = navController
        self.statusBarColor = statusBarColor
        var _param = parameters
        _param["public_key"] = publicKey
        self.params = _param
        self.complition = complition
    }
    
    func pay() {
        liqpayMob.checkout(params, privateKey: privatKey,  delegate: self)
    }
    
    func navigationController() -> UINavigationController {
        navController
    }
    
    func onResponseSuccess(_ response: String!) {
        guard let dataBytes = response.data(using: .utf8),
            let data = try? JSONSerialization.jsonObject(with: dataBytes, options: .mutableContainers) as? [String: String],
            let infoBytes = data["data"]?.data(using: .utf8),
            let info = try? JSONSerialization.jsonObject(with: infoBytes, options: .mutableContainers) as? [String: Any]
            else { complition(false); return }
        complition((info["status"] as? String) == "success")
    }
    
    
    func onResponseError(_ errorCode: Error!) {
        complition(false)
    }
    
    func getStatusBarColor() -> UIColor! {
        statusBarColor
    }
}
