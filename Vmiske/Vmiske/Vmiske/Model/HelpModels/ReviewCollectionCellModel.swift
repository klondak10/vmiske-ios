//
//  ReviewCollectionCellModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/7/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct ReviewCollectionCellModel {
    
    private let review: Review
    private var isExpandable = false
    
    init(_ review: Review) {
        self.review = review
    }
    
    var Review: Review {
        return review
    }
    
    var IsExpandable: Bool {
        get{
            return isExpandable
        }
        set {
            isExpandable = newValue
        }
    }
}
