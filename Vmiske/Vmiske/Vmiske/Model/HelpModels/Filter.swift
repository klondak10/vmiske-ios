//
//  Filter.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct Filters: Codable {
    let name: String
    let value: [FilterItem]
    
    struct FilterItem: Codable {
        let id: Int
        let subname: String
    }
}
