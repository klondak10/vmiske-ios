//
//  ReviewModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation


struct Review: Codable {
    
    let rating: String
    let author: String
    let text: String
    let createAt: String
    
    let answer: Answer?
    
    enum CodingKeys: String, CodingKey {
        case rating, author, text, answer
        case createAt = "date"
    }
    
    struct Answer: Codable {
        let answerName: String?
        let answerText: String?
        let answerDate: String?
        
        var AnswerDate: String {
            let formatFromString = "yyyy-MM-dd HH:mm:ss"
            let formatToString = "dd MMM yyyy"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = formatFromString
            let date = dateFormatter.date(from: answerDate ?? "") ?? Date()
            dateFormatter.locale = Locale(identifier: "uk_UA")
            dateFormatter.dateFormat = formatToString
            
            return dateFormatter.string(from: date)
        }
        
        enum CodingKeys: String, CodingKey {
            case answerName = "name"
            case answerText = "text"
            case answerDate = "date"
        }
    }
    
    var CreateAt : String {
        
        let formatFromString = "yyyy-MM-dd HH:mm:ss"
        let formatToString = "dd MMM yyyy"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatFromString
        let date = dateFormatter.date(from: createAt) ?? Date()
        dateFormatter.locale = Locale(identifier: "uk_UA")
        dateFormatter.dateFormat = formatToString
    
        return dateFormatter.string(from: date)
    }
}

//"date_added": "2019-06-28 09:12:43"
