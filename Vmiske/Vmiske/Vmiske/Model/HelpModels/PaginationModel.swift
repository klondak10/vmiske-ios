//
//  PaginationModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation


struct PaginationModel: Codable {
    
    let count: Int
    let limit: Int
    var offset: Int
}

extension PaginationModel {
    func getInrementItem() -> PaginationModel {
        PaginationModel(count: self.count, limit: self.limit, offset: self.offset + 1)
    }

    static let zero = PaginationModel(count: 0, limit: 20, offset: 0)
}
