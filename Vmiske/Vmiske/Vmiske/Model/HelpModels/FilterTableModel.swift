//
//  FilterTableModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 08.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct FilterTableModel {
    let filter: Filters
    var isCollapsed: Bool
}
