//
//  SortingModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//


enum Sorting: String {
    
    enum Direction: String {
        case ASC = "asc"
        case DESC = "desc"
    }
    
    case price = "price"
    case name = "name"
}


struct SortingState {
    var sortType: Sorting
    var direction: Sorting.Direction
}
