//
//  BaseResponse.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct BaseResponse<T: Codable>: Codable {
    
    let answer: T
    let error: Bool
    let message: String?
}
