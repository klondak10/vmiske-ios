//
//  BasketProduct.swift
//  Vmiske
//
//  Created by Roman Haiduk on 23.11.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//


struct BasketProduct: Codable, Equatable {
    
    var count: Int
    var baskId: Int
    let productId: Int
    let name: String
    let price: Double
    let status: String?
    let barcode: String
    let artikul: String
    let image: String
    let quantity: Int
    let stockStatus: Int
    let discount: Product.Discount?
    let opt: Optional
    
    var Image: String {
        (ConstantProject.Network.BASE_IMAGE_URL + image)
        .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "" 
    }
    
    enum CodingKeys: String, CodingKey {
        case productId = "product_id"
        case image
        case stockStatus = "stock_status_id"
        case name, price, barcode, artikul, quantity, baskId, count, opt, status, discount
    }
    
    struct Optional: Codable {
        let price: Double
        let name: String?
        let value: String?
    }
    
    static func == (lhs: Self, rhs: CategoryProductList.ProductList ) -> Bool {
        if lhs.opt.name == nil {
            return rhs.id == lhs.productId
        } else {
            return rhs.id == lhs.productId && lhs.opt.name! == rhs.currentType?.name
        }
    }
    
    static func == (lhs: Self, rhs: Product) -> Bool {
        rhs.info.id == lhs.productId && rhs.currentType?.name == lhs.opt.name
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        rhs.productId == lhs.productId && rhs.opt.name == lhs.opt.name
    }
}
