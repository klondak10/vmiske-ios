//
//  ProductList.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct CategoryProductList: Codable {
    
    var products: [ProductList]
    let filters: [Filters]
    var pagination: PaginationModel
    
    enum CodingKeys: String, CodingKey {
        case products
        case filters = "filters"
        case pagination = "page"
    }
    
    struct ProductList: Codable, Equatable {
        
        let id: Int
        let name: String
        let image: String
        let price: Double
        let viewed: Int
        let stockStatus: Int
        let maxQuantity: Int
        let discount: Product.Discount?
        let type: [Product.ProductType]
        let reviewEstimate: Double
        var currentType: Product.ProductType?
        
        var imagePath: String {
            return (ConstantProject.Network.BASE_IMAGE_URL + image).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        }
        
        enum CodingKeys: String, CodingKey {
               
            case id = "product_id"
            case stockStatus = "stock_status_id"
            case maxQuantity = "quantity"
            case type = "subprice"
            case reviewEstimate = "rew"
            case name, discount ,image ,price, viewed
        }
        
        func convertToProduct() -> Product {
            return Product(from: self)
        }
        
        static func == (lhs: Self, rhs: Self) -> Bool {
            if lhs.currentType != nil {
                return lhs.id == rhs.id && lhs.currentType!.name == rhs.currentType?.name
            } else {
                return lhs.id == rhs.id
            }
        }
        static func == (lhs: Self, rhs: BasketProduct) -> Bool {
            if lhs.currentType != nil {
                return lhs.id == rhs.productId && lhs.currentType!.name == rhs.opt.name
            } else {
                return lhs.id == rhs.productId
            }
        }
    }
}

