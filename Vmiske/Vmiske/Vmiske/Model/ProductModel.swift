//
//  ProductModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation


struct Product: Codable, Equatable {
   
    let info: Info
    let reviews: [Review]
    let specification: [Specification]
    let link: String?
    
    var currentType: ProductType?
    
    var ImagesPath: [String] {
        return info.images.map { (ConstantProject.Network.BASE_IMAGE_URL + $0)
            .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "" }
    }
    
    init(from product: CategoryProductList.ProductList) {
        reviews = []
        specification = []
        info = Info(from: product)
        currentType = product.currentType
        link = "https://vmiske.ua"
    }
    
    var Link: String {
        if let link = link, let _ = URL(string: link) {
            return link
        } else { return "https://vmiske.ua" }
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.info.id == rhs.info.id && lhs.currentType?.name == rhs.currentType?.name
    }
    
    static func == (lhs: Self, rhs: BasketProduct) -> Bool {
        if lhs.currentType == nil {
            return lhs.info.id == rhs.productId
        } else {
            return lhs.info.id == rhs.productId && lhs.currentType!.name == rhs.opt.name
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case specification = "spec"
        case reviews, info, currentType, link
    }
}

extension Product {
    
    class Info: Codable {
        
        let id: Int
        let name: String
        let price: Double
        let barcode: String
        let artikul: String
        let images: [String]
        let viewed: Int
        var quantity: Int
        let stockStatus: Int
        let description: String
        let reviewCount: ReviewCount
        let discount: Discount?
        let types: [ProductType]
        
        enum CodingKeys: String, CodingKey {
            case id = "product_id"
            case images = "image"
            case stockStatus = "stock_status_id"
            case types = "subprice"
            case reviewCount = "rew"
            case name, price, viewed, description,
            discount, barcode, artikul, quantity
        }
        
        init(from product: CategoryProductList.ProductList) {
            id = product.id
            name = product.name
            price = product.price
            barcode = ""
            artikul = ""
            images = [product.image]
            viewed = 0
            quantity = product.maxQuantity
            stockStatus = product.stockStatus
            description = ""
            reviewCount = ReviewCount(from: product.reviewEstimate)
            discount = product.discount
            types = product.type
            
        }
    }
    
    class Discount: Codable {
        let price: Double
        let dateStart: String
        let dateEnd: String
        
        enum CodingKeys: String, CodingKey {
            case dateStart = "date_start"
            case dateEnd = "date_end"
            case price
        }
    }
    
    struct ProductType: Codable {
        let id: Int
        let price: Double
        let name: String
        let barcode: String?
        let artikul: String?
        let quantity: Int
        let povId: Int
        let poId: Int
        let value: String
        
        enum CodingKeys: String, CodingKey {
            case id = "ovId"
            case price, name, barcode, artikul, quantity, povId, poId, value
        }
    }
    
    class ReviewCount: Codable {
        let rating: Float
        let count: Int
        
        init(from: Double) {
            rating = Float(from)
            count = 0
        }
    }
    
    class Specification: Codable {
        let text: String
        let name: String
    }
}
