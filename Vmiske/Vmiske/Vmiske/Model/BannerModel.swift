//
//  BannerModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

struct Banner: Codable {
    let title: String
    let image: String
    let description: String
    let link: String
    
    var imagePath: String {
        return image != "" ? (ConstantProject.Network.BASE_IMAGE_URL + image).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)! : ""
    }
}
