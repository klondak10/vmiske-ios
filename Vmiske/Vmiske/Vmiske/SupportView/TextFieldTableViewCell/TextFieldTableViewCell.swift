//
//  TextFieldTableViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/23/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift

class TextFieldTableViewCell: UITableViewCell {
    
    var textField: VmiskeTextField!
    var disposeBag = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    private func addTextField() {
        selectionStyle = .none
        
        textField = VmiskeTextField()
        contentView.addSubview(textField)
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor).isActive = true
        textField.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        textField.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        textField.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        
        textField.minimumPlaceholderFontSize = 11
        textField.originalPlaceholderFontSize = 16
        textField.font = UIFont(name: "Rubik-Regular", size: 16)
        textField.addToolbar()
    }
    
    func config(_ placeholder: String, text: String = "", keyboardType: UIKeyboardType = .default) {
        if textField == nil {
            addTextField()
        }
        
        textField.tweePlaceholder = placeholder
        textField.text = text
        textField.keyboardType = keyboardType
    }
}
