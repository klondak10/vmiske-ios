//
//  RoundedButton.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedButton: UIButton {
    
    @IBInspectable public var OrangeButton: Bool = true {
        didSet {
            if OrangeButton {
                vmiskeColor = #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1)
                setTitleColor(.white, for: .normal)
            } else {
                vmiskeColor = .white
                setTitleColor(#colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1), for: .normal)
                setTitleColor(#colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1) , for: .disabled)
            }
        }
    }
    
    private var vmiskeColor: UIColor = #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1) {
        didSet { self.backgroundColor = vmiskeColor}
    }
    
    override var isEnabled: Bool {
        didSet {
            if OrangeButton {
                backgroundColor = !isEnabled ? #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1) : vmiskeColor
            }
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        
        layer.masksToBounds = false
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.height/2).cgPath
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 2.5
        layer.shadowOpacity = 0.3
        layer.shadowOffset = CGSize(width: 0, height: 2)
        
        titleEdgeInsets = UIEdgeInsets(top: 4, left: 8, bottom: 4, right: 8)
        titleLabel?.adjustsFontSizeToFitWidth = true
        imageView?.contentMode = .scaleAspectFit
        self.contentMode = .center
    }
}


@IBDesignable
class RoundedBorderedButton: UIButton {
    
    @IBInspectable public var borderColor: UIColor = #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1) {
        didSet {
            layer.borderColor = borderColor.cgColor
            self.setTitleColor(borderColor, for: .normal)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
        layer.borderWidth = 1
    }
}
