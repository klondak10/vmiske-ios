//
//  LeftMenuTableViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {

    static let identifier = "left"
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ovalImageView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        numberLabel.adjustsFontSizeToFitWidth = true
    }
    
    func setBadge(_ number: Int) {
        if number == 0 {
            ovalImageView.isHidden = true
            numberLabel.isHidden = true
        } else {
            numberLabel.text = "\(number)"
            ovalImageView.isHidden = false
            numberLabel.isHidden = false
        }
    }
}
