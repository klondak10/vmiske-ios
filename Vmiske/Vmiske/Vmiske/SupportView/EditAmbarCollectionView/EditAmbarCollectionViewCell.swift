//
//  EditAmbarCollectionViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EditAmbarCollectionViewCell: UICollectionViewCell {
    static let identifier = "EditAmbarCollectionViewCell"
    
    //MARK: IBOutlets
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var desctiptionLabel: UILabel!
    @IBOutlet weak var checkmarkButton: UIButton!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    //MARK: Properties
    private var disposeBag = DisposeBag()
    
    private var maxValue = 1
    private let counter = PublishSubject<Int>()
    private var counterValue = 1
    private var prodId = 0
    var selectedProd: ((Int, Int)->())?
    var deselectedProd: ((Int)->())?
    
    //MARK: Override Methods
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addCornerAndShadow()
        typeView.layer.cornerRadius = typeView.bounds.height / 2
    }
    
    //MARK: Functions
    private func configureCounterRx() {
        
        let isCounter = counter.asObservable().share(replay: 1)
        isCounter.map{ "\($0)"}
            .bind(to: counterLabel.rx.text)
            .disposed(by: disposeBag)
        isCounter.map { $0 != 0}
            .bind(to: minusButton.rx.isEnabled)
            .disposed(by: disposeBag)
        isCounter.map { $0 != self.maxValue }
            .bind(to: plusButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        
        minusButton.rx.tap
            .subscribe { [unowned self](_) in
                self.counterValue -= 1
                self.counter.onNext(self.counterValue)
                self.selectedProd?(self.prodId, self.counterValue)
            }.disposed(by: disposeBag)
        
        plusButton.rx.tap
            .subscribe(onNext: { [unowned self] (_) in
                self.counterValue += 1
                self.counter.onNext(self.counterValue)
                self.selectedProd?(self.prodId, self.counterValue)
            }).disposed(by: disposeBag)
    }
    
    private func configureRx() {
        
        checkmarkButton.rx.tap
            .subscribe(onNext: { [unowned self] (_) in
                self.checkmarkButton.isSelected.toggle()
                if self.checkmarkButton.isSelected == true {
                    self.selectedProd?(self.prodId, self.counterValue)
                } else {
                    self.deselectedProd?(self.prodId)
                }
            }).disposed(by: disposeBag)
    }
    
    func configure(_ order: OrderInfo.Product, _ orderInfo: OrderInfo, _ ambarId: Int?,
                   _ currentChoisenCount: Int?) {
        prodId = order.orderProductId
        productImageView.sd_setImage(with: URL(string: order.Image), placeholderImage: #imageLiteral(resourceName: "placeholder_photo"))
        desctiptionLabel.text = order.name
        typeView.isHidden = true
        quantityLabel.text = ""
        let ambarsProdCounts = orderInfo.ambars
            .map { $0.products
                .filter { Int($0.product_id) == order.productId }
                .map { $0.quantity}
        }
        let summaryProdAmbars = ambarsProdCounts.reduce(0, {$0 + $1.reduce(0, { $0 + $1})})
        let allProd = orderInfo.products
            .first(where: { $0.productId == order.productId})?.quantity ?? 0
        
        if let ambarId = ambarId,
            let currentAmbar = orderInfo.ambars.first(where: { $0.info.ambarId == ambarId}){
            maxValue = allProd - summaryProdAmbars + (currentAmbar.products
                .first(where: {Int($0.product_id) == order.productId })?.quantity ?? 0)
        } else {
            maxValue = allProd - summaryProdAmbars
        }
  
        checkmarkButton.isUserInteractionEnabled = maxValue != 0
        configureCounterRx()
        configureRx()
        if let quant = currentChoisenCount {
            counterValue = quant
            counter.onNext(quant)
        } else {
            counterValue = maxValue
            counter.onNext(maxValue)
        }
        priceLabel.setPrice(Int(Double(order.price) ?? 0.0))
    }
}
