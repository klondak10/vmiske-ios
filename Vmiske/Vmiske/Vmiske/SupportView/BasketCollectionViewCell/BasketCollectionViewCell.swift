//
//  BasketCollectionViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BasketCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "BasketCollectionViewCell"

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var typeLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet private weak var counterMinusButton: UIButton!
    
    @IBOutlet private weak var counterPlusButton: UIButton!
    @IBOutlet private weak var counterLabel: UILabel!
    
    private let counter = PublishSubject<Int>()
    private var isCounter: Observable<Int>  {
        return counter.asObservable()
    }
    
    private var counterValue = 0 {
        didSet {
            if counterValue != 0 {
                counter.onNext(counterValue)
                counterLabel.text = "\(counterValue)"
            }
        }
    }
    
    var updateCount: ((_ count: Int) -> ())?
    
    var basketDisposeBag = DisposeBag()
    private var disposeBag = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        basketDisposeBag = DisposeBag()
        imageView.image = nil
        typeLabel.text = ""
        typeLabel.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addCornerAndShadow()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .clear
        configureConterRx()
    }

    private func configureConterRx() {
        let minValue =  isCounter.map { $0 != 1}
        minValue.bind(to: counterMinusButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        counterMinusButton.rx.tap
            .subscribe { [weak self](_) in
                self?.counterValue -= 1
                self?.updateCount?( self?.counterValue ?? 1)
        }.disposed(by: disposeBag)
        
        counterPlusButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                self?.counterValue += 1
                self?.updateCount?( self?.counterValue ?? 1)
            }).disposed(by: disposeBag)
        
    }
    
    func config(_ product: Product) {
        if let type = product.currentType {
            typeLabel.text = type.name
            typeLabel.isHidden = false
        } else { typeLabel.isHidden = true }
        if product.ImagesPath.count > 0 {
            imageView.sd_setImage(with: URL(string: product.ImagesPath[0]), placeholderImage: #imageLiteral(resourceName: "placeholder_photo"))
        }
        descriptionLabel.text = product.info.name
        priceLabel.setPrice(Int(product.info.price + (product.currentType?.price ?? 0)))
        counterValue = product.info.quantity
    }
    
    func config(_ product: BasketProduct) {
        if let name = product.opt.value {
            typeLabel.text = name
            typeLabel.isHidden = false
        } else { typeLabel.isHidden = true }
        imageView.sd_setImage(with: URL(string: product.Image), placeholderImage: #imageLiteral(resourceName: "placeholder_photo"))
        descriptionLabel.text = product.name
        priceLabel.setPrice(Int(product.price + product.opt.price))
        counterValue = product.count
    }
    
    func config(_ img: UIImage, count: Int) {
        self.imageView.image = img
        counterValue = count
    }
    
}
