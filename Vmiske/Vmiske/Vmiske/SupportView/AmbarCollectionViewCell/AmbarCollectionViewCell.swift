//
//  AmbarCollectionViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class AmbarCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "AmbarCollectionViewCell"
    
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var seeMoreButton: UIButton!
    @IBOutlet weak var seeMoreImageView: UIImageView!
    
    @IBOutlet weak var productTableView: UITableView!{
        didSet {productTableView.register(UINib(nibName: "OnePurchaseTableViewCell", bundle: nil), forCellReuseIdentifier: OnePurchaseTableViewCell.identifier) }
    }
    @IBOutlet weak var editButton: RoundedButton!
    
    private let standartHeight: CGFloat = 176
    private let extraHeightForViewWithTable: CGFloat = 59
    
    @IBOutlet weak var cellWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var cellHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewWithTableHeightConstraint: NSLayoutConstraint!
    
    private var isExpanded = false
    var action:  () -> Void = {}
    var editAction: () -> Void = {}
    
    private var tableItemHeight: CGFloat = 125
    private var products: [OrderInfo.AmbarInfo.Product] = [] {
        didSet { productTableView.reloadData() }
    }
    
    private let dateToFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()
    
    private let dateFromFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tableItemHeight = productTableView.bounds.width * 125 / 354
        productTableView.superview?.makeDashedBorderLine()
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 10).cgPath
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        contentView.backgroundColor = .clear
        
        viewWithTableHeightConstraint.constant = 0
        cellHeightConstraint.constant = standartHeight
        cellWidthConstraint.constant = UIScreen.main.bounds.width - 25
        
        if let view = cellHeightConstraint.firstItem as? UIView {
            view.layer.cornerRadius = 10
            layer.masksToBounds = false
            layer.cornerRadius = 10
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowRadius = 3
            layer.shadowOpacity = 0.18
            layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 10).cgPath
            layer.shadowOffset = CGSize(width: 0, height: 0.5)
        }
        
        productTableView.delegate = self
        productTableView.dataSource = self
        editButton.addTarget(self, action: #selector(showEditViewController), for: .touchUpInside)
    }
    
    @objc private func showEditViewController() {
        editAction()
    }
    
    @IBAction func seeMoreAction(_ sender: Any) {
        isExpanded = !isExpanded
        seeMoreImageView.rotate(isExpanded ? .pi : 0)
        
        self.cellHeightConstraint.constant = self.isExpanded ? self.standartHeight + self.extraHeightForViewWithTable + (CGFloat(self.products.count) * self.tableItemHeight) + 6 : self.standartHeight
        self.viewWithTableHeightConstraint.constant = self.isExpanded ? self.extraHeightForViewWithTable + (CGFloat(self.products.count) * self.tableItemHeight) : 0
        UIView.animate(withDuration: 0.3) {
            self.seeMoreButton.setTitle(self.isExpanded ? "Показати меньше" : "Деталі", for: .normal)
            self.layoutIfNeeded()
        }
        action()
    }
    
    func configure(_ ambar: OrderInfo.AmbarInfo) {
        orderNumberLabel.text = String(ambar.info.orderId)
        if let dateFrom = dateFromFormatter.date(from: ambar.info.date) {
            dateLabel.text = dateToFormatter.string(from: dateFrom)
        }
        statusLabel.text = ambar.info.status ?? ""
        commentLabel.text = ambar.info.customerText
        products = ambar.products
        productTableView.reloadData()
    }

}

extension AmbarCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OnePurchaseTableViewCell.identifier) as! OnePurchaseTableViewCell
        
        let prod = products[indexPath.row]
        cell.config(img: prod.Image, description: prod.name, quantity: prod.quantity,
                    price: prod.price)
        cell.typeNameLabel.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        cell.typeView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableItemHeight
    }
}
