//
//  ValidationEnum.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//


enum ValidateRegex: Int {
    case email
    case userName
    case password
}

extension ValidateRegex {
    func returnRegex() -> String {
        switch self {
        case .email:
            return "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$"
        case .userName:
            return "^[А-ЯЁа-яё]{1,32}|[A-Za-z]{1,32}$"
        case .password:
            return "^[A-ZА-Яа-яa-z0-9_-Ёё]{4,20}$"
        }
    }
    
    func returnErrorMessage() -> String {
        switch self {
        case .email:
            return "Обов'язкове поле".L
        case .userName:
            return "Обов'язкове поле".L
        case .password:
            return "Некоректний пароль".L
        }
    }
}

