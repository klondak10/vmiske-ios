//
//  VmiskeTextView.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import TweeTextField
import RxSwift
import RxCocoa

@IBDesignable
class VmiskeTextField: TweeAttributedTextField {
    
    @IBInspectable public var ValidationType: Int {
        get {
            guard let validType = validationType else { return -1}
            return validType.rawValue
        } set {
           validationType = ValidateRegex(rawValue: newValue)
        }
    }
    
    var silenceValidation: Bool = false
    
    private var validationType: ValidateRegex?
    internal let isValidText = BehaviorRelay<Bool>(value: false)
    var validText: Observable<Bool> {
        return isValidText.asObservable()
    }
    
    private var keyboardFrame: CGRect?
    
    private let disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        baseSetup()
        setupRx()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        baseSetup()
        setupRx()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        placeholderLabel.text = placeholderLabel.text?.L
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func checkTextValidation() {
        guard let validType = self.validationType else {
             self.isValidText.accept(true)
            return
        }
        do {
            if try NSRegularExpression(pattern: validType.returnRegex(),
                                       options: .caseInsensitive).firstMatch(in: text!, options: [], range: NSRange(location: 0, length: text!.count)) != nil {
                self.isValidText.accept(true)
                self.hideInfo()
            } else {
                self.isValidText.accept(false)
                self.showInfo(validType.returnErrorMessage())
            }
        } catch {
            self.isValidText.accept(false)
            self.showInfo(validType.returnErrorMessage())
            
        }
    }
    
    private func setupRx() {
        
        //Валидация вводимого текста
        self.rx.text.orEmpty
            .throttle(0.5, scheduler: MainScheduler.instance)
            .subscribe(onNext: {[weak self] (text) in
                guard let `self` = self else { return }
                
                self.checkTextValidation()
            }).disposed(by: disposeBag)
        
        //Отслеживание клавиатурі, чтобі не біл под ней
        rx.controlEvent(.editingDidBegin)
            .subscribe(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                guard let keyboard = self.keyboardFrame else { return }
                var dif:CGFloat = 0
                if keyboard.minY == UIScreen.main.bounds.height {
                    dif = keyboard.minY - keyboard.height - self.frame.maxY
                } else {
                     dif = keyboard.minY - self.frame.maxY
                }
                if dif < 0 {
                    self.superview?.frame.origin.y = dif - 20
                }
            }).disposed(by: disposeBag)
        
        //опускание супервью если оно было поднято
        rx.controlEvent(.editingDidEnd)
            .subscribe(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                if let nextView = self.superview?.viewWithTag(self.tag + 1) {
                    nextView.becomeFirstResponder()
                }
            }).disposed(by: disposeBag)
    }
    
    //MARK:Base setup
    private func baseSetup() {
        infoAnimationDuration = 0.3
        infoLabel.font = FontHelper.font(type: .regular, size: .small)
        infoTextColor = #colorLiteral(red: 0.9185729623, green: 0.1846595407, blue: 0.1107173935, alpha: 1)
        
        
        activeLineColor = .blue
        activeLineWidth = 1
        animationDuration = 0.3
        
        borderStyle = .none
        
        lineColor = #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1)
        lineWidth = 1
        font = FontHelper.font(type: .regular, size: .medium)
        placeholderColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        minimumPlaceholderFontSize = 12
        originalPlaceholderFontSize = 16
        placeholderDuration = 0.3
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        // To obtain the size of the keyboard:
        keyboardFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
    }
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {
        self.superview?.frame.origin.y = 0
    }
    
    func addToolbar() {
        //Добавить тубар над дата пикером
        let toolbar = UIToolbar ();
        toolbar.sizeToFit ()
        //кнопки для Готово и Отмена для тулбара
        let doneButton = UIBarButtonItem (title: "Done", style: .plain,
                                          target: nil, action: nil)
        doneButton.rx.tap
            .subscribe { [unowned self] _ in
               self.superview?.endEditing(true)
            }.disposed(by: self.disposeBag)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
      
        toolbar.setItems([spaceButton, doneButton], animated: false)
        
        //Привязка тулбара к текстфилду
        inputAccessoryView = toolbar
    }
}
