//
//  SecureVmiskeTextField.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class SecureVmiskeTextField: VmiskeTextField {
    
    let showImage = #imageLiteral(resourceName: "042-eye")
    let hideImage = #imageLiteral(resourceName: "043-eye-1")
    
    @IBInspectable var isSecure: Bool {
        get {
           return isSecureTextEntry
        }
        set {
            isSecureTextEntry = newValue
            if isSecureTextEntry { addShowHideButton()}
            else { secureButton.isHidden = true}
        }
    }
    
     private var secureButton: UIButton = UIButton(frame: .zero)
    
    private func addShowHideButton(){
        
        let height = self.frame.size.height;
        let frame = CGRect(x: 0,y: 0, width: 0, height: height)
        
        secureButton.frame = frame
        secureButton.isHidden = false
        secureButton.backgroundColor = .clear
        
        secureButton.setImage(hideImage, for: UIControl.State())
        secureButton.sizeToFit()
        let insetOn = bounds.height / 1.8
        secureButton.imageEdgeInsets = UIEdgeInsets(top: insetOn, left: 2 * insetOn - 4, bottom: insetOn, right: 4)
      
        
        secureButton.addTarget(self, action: #selector(toggleShowPassword), for: .touchUpInside)
        
        self.rightViewMode = .whileEditing
        self.rightView = secureButton
    }
    
    @objc private func toggleShowPassword() {
        
        self.isSecureTextEntry = !self.isSecureTextEntry
        
        secureButton.alpha = 0
        UIView.animateKeyframes(withDuration: 0.3, delay: 0.0, options: [], animations: {
            
            self.secureButton.alpha = 1
            self.secureButton.setImage(!self.isSecureTextEntry ? self.showImage : self.hideImage, for: UIControl.State())
            
        }, completion: nil)
    }
    
}
