//
//  PhoneTextField.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import TweeTextField
import RxSwift
import RxCocoa

class PhoneNumberTextField: VmiskeTextField, UITextFieldDelegate {
    
    let startMask = "+38 ("
    
    private let disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        delegate = self
        self.text = startMask
        keyboardType = .numberPad
        tweePlaceholder = "Номер"
        addToolbar()
        self.isValidText.accept(false)
    }
    
    private let closingBrasketIndex = 5 + 3
    private let spaceIndex = 5 + 3 + 2 + 3
    private let secondSpaceIndex = 5 + 3 + 2 + 3 + 3
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isValidText.accept(false)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.text!.count == 19 {
            isValidText.accept(true)
            self.hideInfo()
        } else {
            isValidText.accept(false)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text, range.length == 0 && string != "" {
            
            if text.count == closingBrasketIndex {
                self.text!.append(") ")
            }
            if text.count == spaceIndex || text.count == secondSpaceIndex {
                self.text!.append("-")
            }
            if textField.text!.count > 18 {
                return false
            }
            self.text?.append(string)
            if self.text!.count == 19 {
                isValidText.accept(true)
                self.hideInfo()
                return false
            }
          } else if range.length == 1 { //обработка удаления
            if text!.count > startMask.count {
                text!.removeLast()
            }
            if text!.count == closingBrasketIndex + 2 {
                text!.removeLast(2)
            }
            if text!.count == spaceIndex + 1 {
                text!.removeLast()
            }
            if text!.count == secondSpaceIndex + 1 {
                text!.removeLast()
            }
        }
        isValidText.accept(false)
        if !silenceValidation {
            self.showInfo("Обов'язкове поле")
        }
        return false
    }
}

