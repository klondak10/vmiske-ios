//
//  OnePurchaseTableViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import SDWebImage

class OnePurchaseTableViewCell: UITableViewCell {
    
    static let identifier = "OnePurchaseTableViewCell"
    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var typeNameLabel: UILabel!
    
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        typeView.layer.cornerRadius = typeView.bounds.height / 2
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func config(img: String, description: String, typeName: String? = nil, quantity: Int, price: Int) {
        
        productImage.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "placeholder_photo"))
        descriptionLabel.text = description
        quantityLabel.text = "\(quantity) шт"
        priceLabel.setPrice(price * quantity)
        if typeName != nil {
            typeNameLabel.text = typeName
            typeView.isHidden = false
        } else { typeView.isHidden = true}
    }
}
