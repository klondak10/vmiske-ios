//
//  StartSearchingView.swift
//  Vmiske
//
//  Created by Roman Haiduk on 09.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit


class StartSearchingView: UIView {
    
    private var label: UILabel!
    private var imageView: UIImageView!

    func configView(for start: Bool) {
        
        backgroundColor = .white
        
        let image = start ? UIImage(named: "start_search") : UIImage(named: "search-failed")
    
        if imageView == nil {
            imageView = UIImageView()
            self.addSubview(imageView)
        }
        imageView.image = image
        if label == nil {
            label = UILabel()
            self.addSubview(label)
        }
        label.text = start ? "Почніть шукати товари".L : "По вашому запиту нічого не знайдено".L
        label.font = FontHelper.font(type: .regular, size: .medium)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        label.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate(
            [
                imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -100),
                imageView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),
                imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor),
                
                label.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                label.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16)
        ])
        
        layoutIfNeeded()
    }
    
}
