//
//  BasketBottomDefaultView.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BasketBottomDefaultView: UIView {

    @IBOutlet weak var havePromocodeButton: RoundedBorderedButton!
    
    @IBOutlet weak var bonusesButton: RoundedBorderedButton!
    
    @IBOutlet weak var arrowForPromoView: UIImageView!
    
    @IBOutlet weak var haveBonusesLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var createOrderButton: RoundedButton!
    
    @IBOutlet weak var returnToShopping: UIButton!
    
    @IBOutlet var bonusesViews: [UIView]!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let _ = loadViewFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let _ = loadViewFromNib()
    }
    
    private func loadViewFromNib() -> UIView {
        
        let view = UINib(nibName: "BasketBottomDefaultView", bundle: nil)
            .instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        return view
    }
}
