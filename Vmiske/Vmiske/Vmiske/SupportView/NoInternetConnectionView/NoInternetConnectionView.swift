//
//  NoInternetConnectionView.swift
//  Vmiske
//
//  Created by Roman Haiduk on 15.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class NoInternetConnectionView: UIView {

    var restoreAction: (() -> Void)?
    
    
    @IBAction func tryAgainAction(_ sender: Any) {
       let url = NSURL(string: "https://google.com")
        let task = URLSession.shared.dataTask(with: url! as URL) { [weak self] (_, _, error) in
            if error == nil {
                DispatchQueue.main.async {
                    self?.restoreAction?()
                    self?.removeFromSuperview()
                }
            }
        }
        task.resume()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let _ = loadViewFromNib()
        URLSessionConfiguration.default.timeoutIntervalForRequest = 5
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let _ = loadViewFromNib()
        URLSessionConfiguration.default.timeoutIntervalForRequest = 5
    }
    
    private func loadViewFromNib() -> UIView {
        
        let view = UINib(nibName: "NoInternetConnectionView", bundle: nil)
            .instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        return view
    }
}
