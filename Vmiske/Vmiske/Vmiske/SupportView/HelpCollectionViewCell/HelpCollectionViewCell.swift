//
//  HelpCollectionViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxGesture

class HelpCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "HelpCollectionViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    @IBOutlet weak var widthCellContraint: NSLayoutConstraint!
    @IBOutlet weak var heightCellConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightDescriptionViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTitleViewConstraint: NSLayoutConstraint!
    
    private var descriptionTextHeight: CGFloat = 0
    private let titleViewExtraHeight: CGFloat = 36
    private let descriptionViewExtraHeight: CGFloat = 15
    
    private var disposeBag = DisposeBag()
    
    var action: () -> Void = {}
    private var isCollapsed = false
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.layer.shadowPath = UIBezierPath(roundedRect: contentView.bounds, cornerRadius: 10).cgPath
        if let view = heightCellConstraint.firstItem as? UIView {
            view.layer.cornerRadius = 10
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        contentView.backgroundColor = .white
        widthCellContraint.constant = ConstantProject.HelpViewController.width
        
        contentView.layer.cornerRadius = 10
        contentView.layer.masksToBounds = false
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.shadowRadius = 4
        contentView.layer.shadowOpacity = 0.18
        contentView.layer.shadowPath = UIBezierPath(roundedRect: contentView.bounds, cornerRadius: 10).cgPath
        contentView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        configureRx()
    }

    func configure(title: String, description: String, isCollapsed: Bool = false) {
        
        titleLabel.text = title
        heightTitleViewConstraint.constant = calculateHeight(titleLabel,
                                                             for: ConstantProject.HelpViewController.width * 0.8) + titleViewExtraHeight
        
        descriptionTextView.text = description
        descriptionTextHeight = calculateHeight(descriptionTextView,
                                                for: ConstantProject.HelpViewController.width * 0.8) + descriptionViewExtraHeight
        heightDescriptionViewConstraint.constant = isCollapsed ? descriptionTextHeight : 0
        
        heightCellConstraint.constant = heightCellConstraint.constant + heightDescriptionViewConstraint.constant
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    private func configureRx() {
        self.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                self.isCollapsed = !self.isCollapsed
                self.arrowImageView.rotate(!self.isCollapsed ? 0 : .pi )
                self.heightDescriptionViewConstraint.constant = self.isCollapsed ? self.descriptionTextHeight : 0
                self.heightCellConstraint.constant = self.heightCellConstraint.constant + self.heightDescriptionViewConstraint.constant
                self.action()
            }).disposed(by: disposeBag)
    }
    
    private func calculateHeight(_ view: UIView,for width: CGFloat) -> CGFloat {
        
        let size = CGSize(width: width, height: .infinity)
        return view.sizeThatFits(size).height
    }
}
