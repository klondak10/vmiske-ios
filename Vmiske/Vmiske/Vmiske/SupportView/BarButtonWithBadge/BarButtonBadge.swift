//
//  BarButtonBadge.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BarButtonBadge: UIView {
////////////////////////////////////////////////////
    static var basketCountItems: Int  { UserDataManager.shared.IsAuthorized ? UserDataManager.shared.ProductInBasket?.count ?? 0 : UserDataManager.shared.TempProductInBasket?.count ?? 0 }
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var ovalImage: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let _ = loadViewFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let _ = loadViewFromNib()
        numberLabel.adjustsFontSizeToFitWidth = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(getNewValue(_:)),
                                               name: .UpdateBasketCountItems, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func loadViewFromNib() -> UIView {
        
        let view = UINib(nibName: "BarButtonBadge", bundle: nil)
            .instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        setBadge(BarButtonBadge.basketCountItems)
        return view
    }
    
    @objc
    private func getNewValue(_ notification: Notification) {
        setBadge(BarButtonBadge.basketCountItems)
    }
    
    private func setBadge(_ number: Int) {
        if number == 0 {
            ovalImage.isHidden = true
            numberLabel.isHidden = true
        } else {
            numberLabel.text = "\(number)"
            ovalImage.isHidden = false
            numberLabel.isHidden = false
        }
        layoutIfNeeded()
    }
}
