//
//  PurchaseCollectionViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PurchaseCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "PurchaseCollectionViewCell"

    @IBOutlet weak var purchasedNumberLabel: UILabel!
    @IBOutlet weak var datePurchasedLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var inAmbarImageView: UIImageView!

    
    private let dateToFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()
    
    private let dateFromFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowPath = UIBezierPath(roundedRect: contentView.bounds, cornerRadius: 10).cgPath
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .clear
        layer.masksToBounds = false
        layer.cornerRadius = 10
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.18
        layer.shadowOffset = CGSize(width: 0, height: 0.5)
    }
    
    func config(numberPurchased: Int, date: String, price: String, status: String, inAmbar: Ambar) {
        setStatus(status)
        inAmbarImageView.image = UserDataManager.shared.Language == ConstantProject.Language.RU ?
            UIImage(named: "inambrRU") : UIImage(named: "in-ambar")
        inAmbarImageView.isHidden = inAmbar == .no
        
        purchasedNumberLabel.text = "№ \(numberPurchased)"
        if let dateFrom = dateFromFormatter.date(from: date) {
            datePurchasedLabel.text = dateToFormatter.string(from: dateFrom)
        }
        priceLabel.setPrice(Int(Double(price) ?? 0.0))
    }
    
    private func setStatus(_ delivered: String) {
        statusLabel.text = delivered
        statusLabel.textColor = delivered == "Доставлено" ? #colorLiteral(red: 0.5557940602, green: 0.8436309695, blue: 0.1671362221, alpha: 1) : #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1)
    }
}
