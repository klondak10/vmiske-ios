//
//  ProductListCollectionViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductListCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "productListCell"
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var typeChangeView: UIView!
    @IBOutlet weak var typeChangeLabel: UILabel!
    
    @IBOutlet weak var basketButton: UIButton!
    @IBOutlet var starRateCollection: [UIImageView]!
    
    @IBOutlet weak var notAvailableLabel: UILabel!
    @IBOutlet weak var salesView: UIView!
    @IBOutlet weak var salesLabel: UILabel!
    
    private var product: CategoryProductList.ProductList?
    
    var currentType: Product.ProductType? {
        didSet {
            if let type = currentType, let product = product {
                
                typeChangeLabel.text = type.name
                let discountPrice = product.discount?.price == nil ? nil : product.discount!.price + type.price
                setupPrice(product.price + type.price, discountPrice)
                self.product?.currentType = type
                typeChangeView.isHidden = false
            } else {
                typeChangeView.isHidden = true
            }
            basketCountChanged()
        }
    }
    
    var showLoader: (() -> ())?
    var removeLoader: (() -> ())?
    var showError: ((_ message: String) -> ())?
    
    
    let basketViewModel = BasketViewModel()
    let basketDisposeBag = DisposeBag()
    
    var disposeBag = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        basketButton.setImage(nil, for: .normal)
        disposeBag = DisposeBag()
        currentType = nil
        product = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        basketButton.setImage(nil, for: .normal)
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.contentView.translatesAutoresizingMaskIntoConstraints = true
        typeChangeView.layer.borderWidth = 1
        typeChangeView.layer.borderColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1).cgColor
        notAvailableLabel.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(basketCountChanged),
                                               name: .UpdateBasketCountItems, object: nil)
    }
    
    override func layoutSubviews() {
        typeChangeView.layer.cornerRadius = typeChangeView.bounds.height / 2
        super.layoutSubviews()
    }
    
    @objc
    func basketCountChanged() {
        if let prod = self.product, UserDataManager.shared.hasInBasket(prod) {
            self.basketButton.isSelected = true
            self.basketButton.setImage(UIImage(named: "in-basket"), for: .normal)
        } else {
            self.basketButton.isSelected = false
            self.basketButton.setImage(UIImage(named: "add-to-basket"), for: .normal)
        }
    }
    
    func configure(product: CategoryProductList.ProductList, currentTypeIndex: Int?) {
        
        self.product = product
        
        productImageView.sd_setImage(with: URL(string: product.imagePath), placeholderImage: #imageLiteral(resourceName: "placeholder_photo"))
        descriptionLabel.text = product.name
        setupPrice(product.price, nil)//product.discount?.price)
        basketButton.imageView?.image = #imageLiteral(resourceName: "add-to-basket")
        currentType = product.type.count == 0 ? nil : currentTypeIndex == nil ? product.type[0] : product.type[currentTypeIndex!]
        starRateCollection.setupReviewEstimate(Int(product.reviewEstimate.rounded()))
        configureRx()
        basketCountChanged()
    }
    
    private func configureRx() {
        basketButton.rx.tap
            .subscribe(onNext: { [unowned self] (_) in
                if UserDataManager.shared.IsAuthorized {
                    self.addToRemoteBasket()
                } else {
                    self.unAuthorizedAddToBasket()
                }
            }).disposed(by: disposeBag)
        
        basketViewModel.onAddedProduct
            .subscribe(onNext: { [weak self] (result) in
                guard let `self` = self else { return }
                self.removeLoader?()
                switch result {
                case .success(let res):
                    if res { self.showLoader?(); self.basketViewModel.getBasketList() }
                    else { self.showError?("Ошибка добавления товара.")}
                default:
                    self.showError?("Ошибка добавления товара.")
                }
            }).disposed(by: basketDisposeBag)
        
        basketViewModel.onBasketList
            .subscribe(onNext: { [unowned self] (res) in
                switch res {
                case .success(let list):
                    UserDataManager.shared.ProductInBasket = list
                    self.removeLoader?()
                    self.layoutIfNeeded()
                case .failure:
                    self.showError?("Ошибка добавления в корзину.")
                }
            }).disposed(by: disposeBag)
        
        basketViewModel.onDeletedItem
            .subscribe(onNext: { [weak self] (result) in
                guard let `self` = self else { return }
                self.removeLoader?()
                switch result {
                case .success(let res):
                    if res { self.showLoader?(); self.basketViewModel.getBasketList() }
                    else { self.showError?("Ошибка добавления товара.")}
                default:
                    self.showError?("Ошибка добавления товара.")
                }
            }).disposed(by: basketDisposeBag)
    }
    
    private func addToRemoteBasket() {
        guard let product = product else { return }
        showLoader?()
        
        if let baskProd = UserDataManager.shared.ProductInBasket?
            .first(where: { $0 == product }) {
            basketViewModel.deleteFromBasket(basketId: baskProd.baskId)
        } else {
            if self.product?.type.count == 0 {
                basketViewModel.addProduct(productId: product.id,
                                           quantity: 1,
                                           povId: nil, poId: nil,
                                           subName: nil,
                                           subDescription: nil,
                                           subPrice: nil)
            } else {
                let currentT = product.type[self.getCurrentTypeIndex() ?? 0]
                basketViewModel.addProduct(productId: product.id,
                                           quantity: 1,
                                           povId: currentT.povId,
                                           poId: currentT.poId,
                                           subName: currentT.name,
                                           subDescription: currentT.value,
                                           subPrice: Int(currentT.price))
            }
        }
    }
    
    private func unAuthorizedAddToBasket() {
        var saveIt = self.product!.convertToProduct()
        saveIt.currentType = self.currentType
        saveIt.info.quantity = 1
        if var basket = UserDataManager.shared.TempProductInBasket {
            if let _ = basket.first(where: { $0 == saveIt }) {
                UserDataManager.shared.TempProductInBasket?.removeAll(where: { $0 == saveIt })
            } else {
                basket.append(saveIt)
                UserDataManager.shared.TempProductInBasket = basket
            }
        } else {
            UserDataManager.shared.TempProductInBasket = [saveIt]
        }
    }
    
    func changeType(_ type: Product.ProductType) {
        currentType = type
    }
    
    private func setupPrice(_ price: Double, _ discount: Double?) {
//
//        if let discount = discount {
//            oldPriceLabel.isHidden = false
//            priceLabel.setPrice(Int(discount))
//            oldPriceLabel.setOldPrice(Int(price))
//        } else {
            oldPriceLabel.isHidden = true
            priceLabel.setPrice(Int(price))
//        }
    }
    
    private func setupSales(_ to: Product.Discount?) {
        guard let discount = to else { salesView.isHidden = true; return }
        salesView.isHidden = false
        
        
        
        guard let timeTo = discount.dateEnd.getTimeInterval() else { salesView.isHidden = true; return }
        
        let intTo = Int(timeTo)
        let timer = Observable<Int>
            .interval(1, scheduler: SerialDispatchQueueScheduler.init(qos: .userInitiated))
        
        timer
            .map { "Знижка: \(self.stringFromTimeInterval(ms: intTo - $0))" }
            .observeOn(MainScheduler.instance)
            .bind(to: salesLabel.rx.text)
            .disposed(by: disposeBag)
    }
    
    func stringFromTimeInterval(ms: NSInteger) -> String {
        return String(format: "%0.2d:%0.2d:%0.2d",
                      arguments: [(ms / 600) % 600, (ms % 600 ) / 10, ms % 10])
    }
}

extension ProductListCollectionViewCell: TypeChangeDelegate {
    func typeChanged(on index: Int) {
        currentType = product?.type[index]
    }
    
    func getCurrentTypeIndex() -> Int? {
        return product?.type.firstIndex(where: { $0.id == currentType?.id })
    }
}
