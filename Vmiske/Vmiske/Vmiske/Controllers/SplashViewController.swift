//
//  SplashViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 29.01.2020.
//  Copyright © 2020 Noty Team. All rights reserved.
//

import UIKit
import Lottie

class SplashViewController: UIViewController {

    @IBOutlet weak var animationView: AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animationView.loopMode = .playOnce
    }
}
