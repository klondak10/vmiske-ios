//
//  PaymentTypeViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/23/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol PaymentTypeDelegate {
    func postPaymentType(_ type: DeliveryPaymentType)
}

class PaymentTypeViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet { tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")}
    }
    @IBOutlet weak var nextButton: RoundedButton!
    
    var delegate: PaymentTypeDelegate?
    var deliveryType: DeliveryReturn?
    
    private var paymentTypes: [DeliveryPaymentType] = [] {
        didSet { tableView.reloadData() }
    }
    
    private var loading = 0 {
        didSet {
            if loading != 0 { self.showLoader() }
            else { self.removeLoader() }
        }
    }
    
    private let viewModel = OrderViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.deleteSeparatorOnEmptyCell()
        nextButton.isEnabled = false
        viewModel.onPayment
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(var list):
                    guard let deliv = self?.deliveryType else { return }
                    
                    switch deliv {
                    case .newPost:
                        list.removeAll { $0.type == "cheque" }
                    default:
                        list.removeAll { $0.type == "cod" }
                    }
                    self?.paymentTypes = list
                }
            }).disposed(by: disposeBag)
        self.loading += 1
        viewModel.getPaymentType()
    }
    
    
    @IBAction func nextButtonAction(_ sender: Any) {
        if let index = tableView.indexPathForSelectedRow?.row {
            delegate?.postPaymentType(paymentTypes[index])
            navigationController?.popViewController(animated: true)
        }
    }
}

extension PaymentTypeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.config(nil, text: paymentTypes[indexPath.row].name)
        cell.accessoryType = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        nextButton.isEnabled = true
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
}
