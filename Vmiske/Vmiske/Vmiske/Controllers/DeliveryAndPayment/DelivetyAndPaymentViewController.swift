//
//  DelivetyAndPaymentViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/23/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture
import WebKit

class DelivetyAndPaymentViewController: BaseViewController {

    let data = UserDataManager.shared
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var ambarSwitcher: UISwitch!
    @IBOutlet weak var tableView: UITableView! {
        didSet {tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell") }
    }
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var createOrderButton: RoundedButton!
    @IBOutlet weak var commentLabel: UILabel!
    
    var promocode: Promocode?{
        didSet {
            if promocode == nil { promocodeText = nil}
        }
    }
    var promocodeText: String?
    var bonuses: Int? 
    
    private var loading = 0 {
           didSet {
               if loading != 0 { self.showLoader() }
               else { self.removeLoader() }
           }
       }
    
    var deliveryType: DeliveryReturn? {
        didSet {
            commentLabel.setCourierComment(delivery: deliveryType, payment: paymentType)
            commentLabel.isHidden = false
        }
    }
    var paymentType: DeliveryPaymentType?{
        didSet {
            commentLabel.setCourierComment(delivery: deliveryType, payment: paymentType)
        }
    }
    private var liqPay: LiqPayManager?
    
    private let viewModel = OrderViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        commentLabel.addCornerAndShadow()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let textRange = NSMakeRange(0, detailLabel.text!.count)
        let attributedText = NSMutableAttributedString(string: detailLabel.text!)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        detailLabel.attributedText = attributedText
        setPrice()
        commentLabel.isHidden = true
        commentLabel.backgroundColor = .white
        
        createOrderButton.addTarget(self, action: #selector(createOrderAction), for: .touchUpInside)
        configureRx()
    }
    
    private func configureRx() {
        detailLabel.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { (_) in
                if UserDataManager.shared.Language == ConstantProject.Language.RU {
                UIApplication.shared.open(URL(string: "https://vmiske.ua/ambar")!,
                                          options: [:], completionHandler: nil)
                } else {
                     UIApplication.shared.open(URL(string: "https://vmiske.ua/ambar-uk")!,
                                               options: [:], completionHandler: nil)
                }
            }).disposed(by: disposeBag)
        
        viewModel.onConfirmOrder
            .subscribe(onNext: { [unowned self] (reslut) in
                //self.loading -= 1
                switch reslut{
                case .failure(let error):
                    self.showError(message: error.localizedDescription + "\nЗв'яжіться зі службою підтримки.".L)
                case .success(_):
                    self.showConfirmOrderAlert()
                }
            }).disposed(by: disposeBag)
        
        viewModel.onCreateOrder
            .subscribe(onNext: { [unowned self] (result) in
                self.loading -= 1
                UserDataManager.shared.UsingBonuses = nil
                switch result {
                case .failure(let error):
                    self.showError(message: error.localizedDescription)
                case .success(let created):
                    if created.status && created.data?.payWay != "liqpay" {
                        self.showConfirmOrderAlert()
                        UserDataManager.shared.ProductInBasket = nil
                    } else if created.status {
                       
                        guard let nav = self.navigationController  else {return }
                        self.liqPay = LiqPayManager(navController: nav, statusBarColor: .black, parameters: created.data!.toDictionary) { (result) in
                            if result {
                                self.loading += 1
                                self.viewModel.confirmOrderPayment(id: created.data!.orderId)
                                UserDataManager.shared.ProductInBasket = nil
                            } else {
                                self.showErrorOrderAlert()
                            }
                        }
                        self.liqPay?.pay()
                    } else {
                        self.showErrorOrderAlert()
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    private func setPrice() {
        var price = 0.0
        price = data.ProductInBasket?
            .map { ($0.price + $0.opt.price) * Double($0.count) }
            .reduce(0,+) ?? 0
        
        var newPrice = price
        if let promo = promocode {
            if price > promo.minimumSummary {
                if promo.Type == .Percantage {
                    newPrice = price - (price * promo.summary / 100)
                } else { newPrice = price - promo.summary }
            } else {
                showError(message: String(format: "Сума повинна бути більше: %d".L, Int(promo.minimumSummary)))
                promocode = nil
            }
        }
        if let bonuse = bonuses {
            newPrice -= Double(bonuse)
        }
        newPrice.round()
        price.round()
        if price != newPrice {
            priceLabel.togetherPrice(Int(newPrice), with: Int(price))
        } else {
            priceLabel.togetherPrice(Int(price), with: nil)
        }
    }
    
    
    func showConfirmOrderAlert() {
        let messTrue = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
        messTrue.settings(image: #imageLiteral(resourceName: "Group 2"),
                          message: "Дякуємо за покупку! \nНаш менеджер скоро зв’яжеться з вами".L,
                          controllerTitle: "", buttonTitle: "Продовжити покупки".L,
                          withLogo: true, removeBackArrow: true) {
            self.sideMenuController?.setContentViewController(to: UIViewController.getFromMainStoryboard(id: "baseNavVC"))
        }
        navigationController?.pushViewController(messTrue, animated: true)
    }
    
    func showErrorOrderAlert() {
        let messFalse = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
        messFalse.settings(image: #imageLiteral(resourceName: "attention-image"),
                           message: "Щось пішло не так з оплатою. Але не хвилюйтеся, ми отримали ваше замовлення. Наш менеджер зателефонує вам і вирішить проблему.".L,
                           controllerTitle: "", buttonTitle: "Повторити замовлення".L,
                           withLogo: true, removeBackArrow: true) {
            self.sideMenuController?.setContentViewController(to: UIViewController.getFromMainStoryboard(id: "baseNavVC"))
        }
        navigationController?.pushViewController(messFalse, animated: true)
    }
    
    @objc
    private func createOrderAction() {
        if deliveryType == nil { showDeliveryErrorAlert(isDelivery: true); return}
        if paymentType == nil { showDeliveryErrorAlert(isDelivery: false); return}
        var _address = ""
        var _cityId = ""
        var _regionId = ""
        var _comment = ""
        switch deliveryType {
        case .newPost(let region, let city, let department):
            _address = department.name
            _regionId = region.zone_id
            _cityId = city.city_id
        case .courier(let region, let city, let address):
            _address = (address.street!)  + (address.house!) + "дім".L
            _address = address.flat == "" ? _address : _address + address.flat! + "кв."
            _regionId = region.zone_id
            _cityId = city.city_id
            _comment = address.comment!
        case .pickUp(let address):
            _address = address
        default:
            break
        }
        loading += 1
        viewModel.createOrder(ambar: ambarSwitcher.isOn ? .yes : .no, address: _address, cityId: _cityId,
                              regionId: _regionId, comment: _comment, paymant: paymentType!.type,
                              shippingCode: deliveryType!.code, promocode: promocodeText, bonuses: bonuses)
        promocode = nil
    }
    
    private func showDeliveryErrorAlert(isDelivery: Bool) {
        let message = isDelivery ? "Виберіть адресу доставки".L : "Виберіть метод оплати".L
        showError(message: message)
    }
}

extension DelivetyAndPaymentViewController: UITableViewDelegate,
UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        if indexPath.row == 0 {
            cell.config(nil, text: "Доставка")
        } else {
            cell.config(nil, text: "Оплата")
            cell.textLabel?.textColor = deliveryType == nil ? #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1) : .black
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let vc = UIViewController.getFromMainStoryboard(id: "deliveryVC")
            (vc as? DeliveryViewController)?.delegate = self
            navigationController?.pushViewController(vc, animated: true)
        } else {
            guard let delivery = self.deliveryType else { return }
            let vc = UIViewController.getFromMainStoryboard(id: "paymentTypeVC")
            (vc as? PaymentTypeViewController)?.delegate = self
            (vc as? PaymentTypeViewController)?.deliveryType = delivery
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension DelivetyAndPaymentViewController: DeliveryTypeDelegate, PaymentTypeDelegate {
    
    func postPaymentType(_ type: DeliveryPaymentType) {
        paymentType = type
        tableView.reloadData()
        createOrderButton.isEnabled = true
    }
    
    func postDeliveryType(_ type: DeliveryReturn) {
        paymentType = nil
        deliveryType = type
        tableView.reloadData()
    }
}
