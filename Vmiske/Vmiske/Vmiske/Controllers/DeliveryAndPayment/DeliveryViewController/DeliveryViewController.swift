//
//  DeliveryViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/23/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum DeliveryReturn: Equatable, CustomStringConvertible {
    
    case pickUp(address: String)
    case courier(region: Region, city: City, address: Address)
    case newPost(region: Region, city: City, department: NewPost)

    static func ==(lhs: DeliveryReturn, rhs: DeliveryReturn) -> Bool {
        switch (lhs, rhs) {
        case ( .pickUp, .pickUp):
            return true
        case ( .courier, .courier):
            return true
        case ( .newPost, .newPost):
            return true

        default:
            return false
        }
    }
    
    var description: String {
        switch self {
        case .pickUp:
            return "Самовивіз".L
        case .newPost:
            return "Нова Пошта".L
        case .courier:
            return "Кур'єр".L
        }
    }
    
    var code: String {
        switch self {
        case .pickUp:
            return "pickup.pickup"
        case .newPost:
            return "NovaPoshta.NovaPoshta"
        case .courier:
            return "citylink.citylink"
        }
    }
}

protocol DeliveryTypeDelegate {
    func postDeliveryType(_ type: DeliveryReturn)
}

class DeliveryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet { tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            tableView.register(TextFieldTableViewCell.self, forCellReuseIdentifier: "cellTF")
        }
    }
    @IBOutlet weak var pickeView: UIPickerView!
    var toolbar: UIToolbar!
    private var barButtonItem: UIBarButtonItem!
    var delegate: DeliveryTypeDelegate?
    
    
    private var sections = [HeaderSection(name: "Самовивіз".L, items: []),
                            HeaderSection(name: "Кур'єр".L, items: ["Область", "Місто".L, "Вулиця".L, "Будинок".L, "Квартира".L, "Коментар для кур'єра".L]),
                            HeaderSection(name: "Нова Пошта".L, items: ["Область", "Місто".L, "Оберіть відділення".L])]
    
    private var selectedIndex = 0 { didSet { selectedIndexChanged() } }
    private var selectedAtSecondSection: Int?
    
    private var loading = 0 {
        didSet {
            if loading != 0 { self.showLoader() }
            else { self.removeLoader() }
        }
    }
    
    private var pickeDataSource: [String] = [] {
        didSet {
            pickeView.reloadAllComponents()
            pickeView.selectRow(0, inComponent: 0, animated: true)
        }
    }
    
    private var pickerIsHidden = true {
        didSet{
            pickeView.isHidden = pickerIsHidden
            toolbar.isHidden = pickerIsHidden
            view.layoutIfNeeded()
        }
    }
    private var deliveryTypes: [DeliveryPaymentType] = []
    private var regionsList: [Region] = []
    private var cityList: [City] = [] {
        didSet {
            cachCityList[pickerRegion?.zone_id ?? ""] =  cityList
        }
    }
    private var cachCityList: [String: [City]] = [:]
    private var newPostDepartment: [NewPost] = [] {
        didSet {
            cachPostDepartment[pickerCity?.city_id ?? ""] = newPostDepartment
        }
    }
    private var cachPostDepartment: [String: [NewPost]] = [:]
    
    private var pickerRegion: Region? {
        didSet {
            sections[1].items[0] = pickerRegion?.name ?? "Область"
            sections[2].items[0] = pickerRegion?.name ?? "Область"

            if pickerRegion?.zone_id != oldValue?.zone_id {
                pickerCity = nil
                pickerDepartment = nil
            }
            tableView.reloadData()
        }
    }
    private var pickerCity: City? {
        didSet {
            
            sections[1].items[1] = pickerCity?.name ?? "Місто".L
            sections[2].items[1] = pickerCity?.name ?? "Місто".L
            if pickerCity?.city_id != oldValue?.city_id {
                pickerDepartment = nil
            }
            if selectedIndex == 1 {
                checkAddress()
            }
            tableView.reloadData()
        }
    }
    private var pickerDepartment: NewPost? {
        didSet {
            sections[2].items[2] = pickerDepartment?.name ?? "Оберіть відділення".L
            tableView.reloadData()
            barButtonItem.isEnabled = pickerDepartment != nil
        }
    }
    private var pickUpAddress: String? {
        didSet {
            sections[0].name = "\(sections[0].name) (\(pickUpAddress ?? ""))"
            tableView.reloadData()
        }
    }
    
    private var address = Address() { didSet { checkAddress() } }
    
    private let viewModel = OrderViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        barButtonItem = UIBarButtonItem(title: "Застосувати".L, style: .plain, target: self, action: #selector(applyDelivery))
        navigationItem.rightBarButtonItem = barButtonItem
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.deleteSeparatorOnEmptyCell()
        
        configureRx()
        loading += 3
        viewModel.getDeliveryType()
        viewModel.getRegiosList()
        viewModel.getPickUpAddress()
        
        pickeView.backgroundColor = .white
        pickeView.frame = CGRect(x: 0, y: view.bounds.height - 180 - view.layoutMargins.bottom, width: view.bounds.width, height: 180)
        pickeView.isHidden = true
        toolbar = UIToolbar(frame: CGRect(x: 0, y: pickeView.frame.origin.y - 40, width: view.bounds.width, height: 40));
        toolbar.sizeToFit ()
        let doneButton = UIBarButtonItem (title: "Done", style: .plain, target: nil, action: nil)
        doneButton.rx.tap
            .subscribe { [unowned self] _ in
                self.self.pickerSelected()
        }.disposed(by: self.disposeBag)
        
        toolbar.setItems([.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                          doneButton], animated: false)
        view.addSubview(toolbar)
        toolbar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)),
                                               name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
   private func keyboardWasShown (notification: NSNotification) {
        let info = notification.userInfo
        let keyboardSize = (info?[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
    }

    @objc
    private func keyboardWillBeHidden (notification: NSNotification) {
        tableView.contentInset = UIEdgeInsets.zero
    }
    
    private func configureRx() {
        
        viewModel.onDelivery
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(let list):
                    self?.deliveryTypes = list
                }
            }).disposed(by: disposeBag)
        
        viewModel.onRegionList
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(let list):
                    self?.regionsList = list
                }
            }).disposed(by: disposeBag)
        
        viewModel.onPickUp
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(let address):
                    self?.pickUpAddress = address
                }
            }).disposed(by: disposeBag)
        
        viewModel.onCityList
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(let list):
                    self?.cityList = list
                }
            }).disposed(by: disposeBag)
        
        viewModel.onNewPostList
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(let posts):
                    self?.newPostDepartment = posts
                }
            }).disposed(by: disposeBag)
    }
    
    private func pickerSelected() {
        self.pickerIsHidden = true
        if self.selectedAtSecondSection == 0{
            if pickerRegion == nil { pickerRegion = regionsList[0] }
            let region = self.pickerRegion!
            guard self.cachCityList[region.zone_id] == nil else {return}
            self.viewModel.getCityList(key: region.area)
            self.loading += 1
        }
        if self.selectedAtSecondSection == 1 {
            if pickerCity == nil { pickerCity = cityList[0] }
            let city = self.pickerCity!
            guard self.cachPostDepartment[city.city_id] == nil else {return}
            self.viewModel.getNewPostDepartment(key: city.Ref)
            self.loading += 1
        }
        if self.selectedAtSecondSection == 2 && pickerDepartment == nil{
            if newPostDepartment.count > 0 {
                pickerDepartment = newPostDepartment[0]
            }
        }
    }

    private func checkAddress() {
        if address.street != "" && address.street != nil &&
            address.house != "" && address.house != nil && pickerCity != nil {
            barButtonItem.isEnabled = true
        } else { barButtonItem.isEnabled = false }
    }
    
    private func selectedIndexChanged() {
        tableView.beginUpdates()
        if tableView.numberOfSections > 1 {
            tableView.deleteSections([1], with: .automatic)
        }
        if selectedIndex != 0 {
            tableView.insertSections([1], with: .automatic)
        }
        tableView.endUpdates()
    }
    
    @objc private func applyDelivery() {
        switch selectedIndex {
        case 0:
            delegate?.postDeliveryType(.pickUp(address: pickUpAddress ?? ""))
        case 1:
            delegate?.postDeliveryType(.courier(region: pickerRegion!, city: pickerCity!, address: address))
        case 2:
            delegate?.postDeliveryType(.newPost(region: pickerRegion!, city: pickerCity!, department: pickerDepartment!))
        default: return
        }
        navigationController?.popViewController(animated: true)
    }
    
    private func setupFirstSection(_ tableView: UITableView, for row: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.config(nil, text: sections[row].name)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.accessoryType = row == selectedIndex ? .checkmark : .none
        if row == selectedIndex { cell.isSelected = true }
        return cell
    }
    
    private func setupSecondSection(_ tableView: UITableView, for row: Int) -> UITableViewCell {
        if selectedIndex == 1 && (row == 5 || row == 2 || row == 3 || row == 4) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTF") as! TextFieldTableViewCell
            cell.config(sections[selectedIndex].items[row], text: "", keyboardType: .default)
            cell.textField.rx.controlEvent(.editingDidBegin)
            .subscribe(onNext: self.pickerSelected).disposed(by: cell.disposeBag)
            switch row {
            case 2:
                cell.textField.text = address.street ?? ""
                cell.textField.rx.text.orEmpty.subscribe(onNext: { [weak self] (text) in
                    self?.address.street = text
                }).disposed(by: cell.disposeBag)
            case 3:
                cell.textField.text = address.house ?? ""
                cell.textField.rx.text.orEmpty.subscribe(onNext: { [weak self] (text) in
                    self?.address.house = text
                }).disposed(by: cell.disposeBag)
            case 4:
                cell.textField.text = address.flat ?? ""
                cell.textField.rx.text.orEmpty.subscribe(onNext: { [weak self] (text) in
                    self?.address.flat = text
                }).disposed(by: cell.disposeBag)
            case 5:
                cell.textField.text = address.comment ?? ""
                cell.textField.rx.text.orEmpty.subscribe(onNext: { [weak self] (text) in
                    self?.address.comment = text
                }).disposed(by: cell.disposeBag)
            default:break
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
            cell.config(nil, text: sections[selectedIndex].items[row])
            if (pickerRegion == nil && (row == 1 || row == 2)) || (pickerCity == nil && row == 2) {
                cell.textLabel?.textColor = #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1)
            }  else {
                cell.textLabel?.textColor = .black
            }
            return cell
        }
    }
}

extension DeliveryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 16, y: 20, width: tableView.bounds.width - 30, height: 50))
        label.font = FontHelper.font(type: .regular, size: .medium)
        label.textColor = #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1)
        label.text = section == 0 ? "1. Спосіб доставки".L : "2. Адреса доставки".L
        let view = UIView()
        view.backgroundColor = .white
        view.addSubview(label)
        return view
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return selectedIndex == 0 ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? sections.count : sections[selectedIndex].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        return indexPath.section == 0 ? setupFirstSection(tableView, for: indexPath.row) :
        setupSecondSection(tableView, for: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if tableView.cellForRow(at: indexPath) as? TextFieldTableViewCell == nil {
                if selectedAtSecondSection != indexPath.row {
                    selectedAtSecondSection = indexPath.row
                    switch indexPath.row {
                    case 0:
                        pickeDataSource = regionsList.map{ $0.name }
                    case 1:
                        pickeDataSource = cityList.map{ $0.name }
                    case 2:
                        pickeDataSource = newPostDepartment.map{ $0.name }
                    default: break
                    }
                }
                pickerIsHidden = false
                pickeView.layoutIfNeeded()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {

        if pickeView.isHidden == false {
            self.pickerSelected()
            return nil
        }
        if indexPath.section == 0 {
            for i in 0...2 {
                tableView.deselectRow(at: IndexPath(row: i, section: 0), animated: true)
                tableView.delegate?.tableView?(tableView,
                                                didDeselectRowAt: IndexPath(row: i, section: 0))
            }
            selectedIndex = indexPath.row
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            barButtonItem.isEnabled = indexPath.row == 0 ? true : false
            pickerIsHidden = true
            pickerRegion = nil
        } else {
            switch indexPath.row {
            case 1 :
                if pickerRegion == nil { return nil }
            case 2 where selectedIndex == 2:
                if pickerCity == nil { return nil }
            default:
                break
            }
        }
        return indexPath
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
    }
}

extension DeliveryViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickeDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickeDataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch selectedAtSecondSection {
        case 0:
            pickerRegion = regionsList[row]
        case 1:
            pickerCity = cityList[row]
        case 2:
            pickerDepartment = newPostDepartment[row]
        default: break
        }
    }
}


struct Address {
    var street: String?
    var house: String?
    var flat: String?
    var comment: String?
}
