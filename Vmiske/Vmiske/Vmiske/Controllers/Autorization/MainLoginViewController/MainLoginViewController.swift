//
//  MainLoginViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn
import RxSwift
import RxCocoa

class MainLoginViewController: BaseAuthorizationViewController {
    
    static var isFast = false
    
    @IBOutlet weak var toShopButton: UIButton!
    
    private let disposeBag = DisposeBag()
    private let viewModel = AuthorizedViewModel()
    private let basketViewModel = BasketViewModel()
    
    private var addedFromTempBasket = 0
    
    private var socialData: RegistrationViewController.SocialSignUpModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance()?.delegate = self
        
        toShopButton.addTarget(self, action: #selector(toShopAction), for: .touchUpInside)
        
        configureRx()
    }
    
    @IBAction func facebookSignIn(_ sender: Any) {
        
        let manager = LoginManager()
        manager.logIn(permissions: [.publicProfile, .email], viewController: self) { [weak self] (result) in
            guard let `self` = self else { return }
            switch result {
            case .cancelled:
                self.showError(message: "Ви скасували реєстрацію.".L)
            case .failed(let error):
                self.showError(message: error.localizedDescription)
            case .success:
                
                let request = GraphRequest.init(graphPath: "me", parameters: ["fields": "id, email, first_name, last_name"], tokenString: AccessToken.current?.tokenString, version: nil, httpMethod: .get)
                request.start(completionHandler: { [weak self](_, result, error) in
                    
                    guard let result = result as? [String: String?],
                        let firstName = result["first_name"] ?? "",
                        let lastName = result["last_name"] ?? "",
                        let email = result["email"] ?? "",
                        let id = result["id"] ?? "-1" else {
                            
                        self?.showError(message: "Ошибка получения данных.".L)
                        return
                    }
                    manager.logOut()
                    self?.checkSignIn(id: id, isGoogle: false, firstName: firstName, lastName: lastName, email: email == "" ? nil : email)
                })

                
            }
        }
    }
    
    @IBAction func googleSignIn(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @objc private func toShopAction() {
        if !MainLoginViewController.isFast {
            (UIApplication.shared.delegate as? AppDelegate)?.setupSideMenu()
        } else {
            MainLoginViewController.isFast = false
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func configureRx() {
        viewModel.onSocialSignIn
            .subscribe(onNext: { [weak self] (res) in
                
                switch res {
                case .success(let res):
                    if res {
                        self?.viewModel.getUserData()
                    } else {
                        
                        self?.removeLoader()
                        guard let vc = UIStoryboard(name: "Autorization", bundle: nil)
                            .instantiateViewController(withIdentifier: "regVC") as? RegistrationViewController else { return }
                        vc.socialSignUp = self?.socialData
                        self?.navigationController?.pushViewController(vc, animated: true)
                    }
                case .failure(let error):
                    self?.removeLoader()
                    self?.showError(message: error.localizedDescription)
                    
                }}).disposed(by: disposeBag)
        
        viewModel.onUserDataRecieved
            .subscribe(onNext: { [weak self] (result) in
                
                self?.removeLoader()
                switch result {
                case .success:
                    if MainLoginViewController.isFast {
                        self?.dismiss(animated: true, completion: nil)
                        MainLoginViewController.isFast = false
                    } else {
                        if UserDataManager.shared.TempProductInBasket == nil || UserDataManager.shared.TempProductInBasket?.count == 0 {
                            (UIApplication.shared.delegate as? AppDelegate)?.setupSideMenu()
                        } else {
                            self?.showLoader()
                            self?.addTempProduct()
                        }
                    }
                    
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                }
                
            }).disposed(by: disposeBag)
        
        basketViewModel.onAddedProduct
            .subscribe(onNext: { [unowned self](result) in
                self.addedFromTempBasket += 1
                if self.addedFromTempBasket == UserDataManager.shared.TempProductInBasket?.count {
                    self.basketViewModel.getBasketList()
                }
            }).disposed(by: disposeBag)
        
        basketViewModel.onBasketList
            .subscribe(onNext: { (result) in
                (UIApplication.shared.delegate as? AppDelegate)?.setupSideMenu()
            }).disposed(by: disposeBag)
    }
    
    
    private func addTempProduct() {
        UserDataManager.shared.TempProductInBasket?.forEach { [weak self] product in
            if product.currentType == nil {
                self?.basketViewModel.addProduct(productId: product.info.id,
                                                 quantity: product.info.quantity,
                                                 povId: nil, poId: nil,
                                                 subName: nil,
                                                 subDescription: nil,
                                                 subPrice: nil)
            } else {
                guard let currentT = product.currentType else { return }
                self?.basketViewModel.addProduct(productId: product.info.id,
                                                 quantity: product.info.quantity,
                                                 povId: currentT.povId,
                                                 poId: currentT.poId,
                                                 subName: currentT.name,
                                                 subDescription: currentT.value,
                                                 subPrice: Int(currentT.price))
            }
        }
    }
    
    func checkSignIn(id: String, isGoogle: Bool, firstName: String?,
                     lastName: String?, email: String?) {
        
        self.showLoader()
        viewModel.socialSignIn(isGoogle: isGoogle, id: id, email: email)
        self.socialData = RegistrationViewController.SocialSignUpModel(isGoogle: isGoogle, id: id, firstName: firstName, lastName: lastName, email: email)
    }
}

//MARK: Google SignIn Delegate
extension MainLoginViewController: GIDSignInDelegate {
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if error.localizedDescription.contains("The operation couldn’t be completed") {
                self.showError(message: "Ви скасували реєстрацію.".L)
            } else {
                self.showError(message: error.localizedDescription)
            }
        } else {
            
            checkSignIn(id: user.userID, isGoogle: true, firstName: user.profile.givenName, lastName: user.profile.familyName, email: user.profile.email)
            
            GIDSignIn.sharedInstance()?.disconnect()
        }
    }
}


//MARK: Codable struct for Facebook response
extension MainLoginViewController {
    
    struct FacebookUser: Codable {
        let first_name: String
        let last_name: String
        let email: String
        let id: String
    }
}
