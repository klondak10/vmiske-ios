//
//  ForgotPasswordEmailViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ForgotPasswordEmailViewController: BaseAuthorizationViewController {

    @IBOutlet weak var emailTF: VmiskeTextField!
    @IBOutlet weak var sendEmailButton: RoundedButton!
    
    private let viewModel = AuthorizedViewModel()
    private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()

       emailTF.validText.bind(to: sendEmailButton.rx.isEnabled).disposed(by: disposeBag)
        
        sendEmailButton.rx.tap
            .subscribe(onNext: { [unowned self] (_) in
                self.viewModel.forgottenPassword(self.emailTF.text!)
            }).disposed(by: disposeBag)
        
        viewModel.onForgottenPassword
            .subscribe(onNext: { [unowned self](result) in
                
                switch result {
                    
                case .success:
                    
                    let messVC = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
                    messVC.settings(image: #imageLiteral(resourceName: "letter-sended"), message: "Лист з інструкцією для відновлення паролю було надіслано Вам на пошту.Будь ласка, перевірте свою пошту та дотримуйтесь отриманої інструкції.", controllerTitle: "Відновлення паролю".L, buttonTitle: "Повернутися".L, action: {
                        messVC.navigationController?.popBackViewControllers(3)
                    })
                    self.navigationController?.pushViewController(messVC, animated: true)
                 
                case .failure(let error):
                    self.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
}
