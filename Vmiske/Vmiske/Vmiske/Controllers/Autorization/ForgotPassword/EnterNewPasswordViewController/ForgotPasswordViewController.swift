//
//  ForgotPasswordViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ForgotPasswordViewController: BaseAuthorizationViewController {
    
    @IBOutlet weak var passwordTF: SecureVmiskeTextField!
    @IBOutlet weak var repitPasswordTF: SecureVmiskeTextField!
    @IBOutlet weak var saveButton: RoundedButton!
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let equalPassword = Observable.combineLatest(repitPasswordTF.rx.text.orEmpty, passwordTF.rx.text.orEmpty) { $0 == $1 }
        let allValid = Observable.combineLatest(passwordTF.validText, repitPasswordTF.validText, equalPassword) { $0 && $1 && $2}
        allValid.bind(to: saveButton.rx.isEnabled).disposed(by: disposeBag)
        
        saveButton.rx.tap
            .subscribe(onNext: { [unowned self] (_) in
                let messVC = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
                messVC.settings(image: #imageLiteral(resourceName: "passwordChanged"), message: "Новий пароль успішно застосовано. Тепер ви можете увійти у свій особистий кабінет.".L, controllerTitle: "Пароль змінено".L, buttonTitle: "Повернутися".L, action: {
                    let needRemove = self.navigationController!.viewControllers.count - 2
                    self.navigationController?.popBackViewControllers(needRemove)
                    self.navigationController?.pushViewController(messVC, animated: true)
                })
            }).disposed(by: disposeBag)
    }
}
