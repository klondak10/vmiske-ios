//
//  RegistrationViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

class RegistrationViewController: BaseAuthorizationViewController {
    
    @IBOutlet weak var nameTF: VmiskeTextField!
    @IBOutlet weak var lastNameTF: VmiskeTextField!
    @IBOutlet weak var emailTF: VmiskeTextField!
    @IBOutlet weak var phoneTF: PhoneNumberTextField!
    @IBOutlet weak var vozhakNumberTF: PhoneNumberTextField!
    @IBOutlet weak var passwordTF: SecureVmiskeTextField!
    @IBOutlet weak var repitPasswordTF: SecureVmiskeTextField!
    @IBOutlet weak var accessButton: UIButton!
    @IBOutlet weak var regButton: RoundedButton!
    @IBOutlet weak var privacyLabel: UILabel!
    
    
    private let viewModel = AuthorizedViewModel()
    private let basketViewModel = BasketViewModel()
    private let disposeBag = DisposeBag()
    
    private var addedFromTempBasket = 0
    
    private let isAccess = BehaviorRelay<Bool>(value: false)
    private var accessObservable: Observable<Bool>  {
        return isAccess.asObservable()
    }
    private var accessVariable = false {
        didSet {
            isAccess.accept(accessVariable)
            accessButton.setImage(accessVariable ? #imageLiteral(resourceName: "rect-check") : #imageLiteral(resourceName: "edit-rect"), for: .normal)
        }
    }
    var socialSignUp: SocialSignUpModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        congifureRx()
        responseHandler()
        userDataRecieved()
        configUI()
    }
    
    private func configUI() {
        let textRange = NSMakeRange(0, privacyLabel.text!.count)
        let attributedText = NSMutableAttributedString(string: privacyLabel.text!)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        privacyLabel.attributedText = attributedText
        
        vozhakNumberTF.silenceValidation = true
        
        guard let social = socialSignUp else { return }
        nameTF.text = social.firstName
        lastNameTF.text = social.lastName
        emailTF.text = social.email
        
        nameTF.checkTextValidation()
        lastNameTF.checkTextValidation()
        emailTF.checkTextValidation()
    }
    
    
    private func congifureRx() {
        
        let equalPassword = Observable
            .combineLatest(repitPasswordTF.rx.text.orEmpty, passwordTF.rx.text.orEmpty)
            { $0 == $1 }
        
        let leaderExist = Observable
            .combineLatest( vozhakNumberTF.rx.text.orEmpty,vozhakNumberTF.isValidText)
            { ($0 == self.vozhakNumberTF.startMask) || $1 }
        
        let leaderAndUserNumberNotEqual = Observable
            .combineLatest(vozhakNumberTF.rx.text.orEmpty, phoneTF.rx.text.orEmpty)
            { $0 != $1 }
        
        let allTextValid = Observable
            .combineLatest(nameTF.validText, lastNameTF.validText, emailTF.validText, phoneTF.validText, passwordTF.validText, accessObservable)
            { $0 && $1 && $2 && $3 && $4 && $5 }
        
        let allValid = Observable
            .combineLatest(allTextValid, equalPassword, leaderExist, leaderAndUserNumberNotEqual)
            { $0 && $1 && $2 && $3 }
        
        allValid.bind(to: regButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        accessButton.rx.tap
            .subscribe(onNext: { [unowned self] (_) in
                self.accessVariable = !self.accessVariable
            }).disposed(by: disposeBag)
        
        regButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                
                self.showLoader()
                
                let leaderPhone = self.vozhakNumberTF.text == self.vozhakNumberTF.startMask ? nil : self.vozhakNumberTF.text!
                self.viewModel.signUp(firstName: self.nameTF.text!, lastName: self.lastNameTF.text!, email: self.emailTF.text!, phone: self.phoneTF.text!, password: self.passwordTF.text!, leaderPhone: leaderPhone, isGoogle: self.socialSignUp?.isGoogle, sID: self.socialSignUp?.id)
                
            }).disposed(by: disposeBag)
        
        view.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self] (_) in
                self?.view.endEditing(true)
            }).disposed(by: disposeBag)
        
        basketViewModel.onAddedProduct
            .subscribe(onNext: { [unowned self](result) in
                self.addedFromTempBasket += 1
                if self.addedFromTempBasket == UserDataManager.shared.TempProductInBasket?.count {
                    self.basketViewModel.getBasketList()
                }
            }).disposed(by: disposeBag)
        
        basketViewModel.onBasketList
            .subscribe(onNext: { (result) in
                let messVC = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
                messVC.settings(image: #imageLiteral(resourceName: "success-picture"),
                                message: "Вітаємо! Ви зареєстровані!".L,
                                controllerTitle: "",
                                buttonTitle: "Почати шопінг".L, action: {
                                    (UIApplication.shared.delegate as? AppDelegate)?.setupSideMenu()
                })
                self.present(messVC, animated: true, completion: nil)
            }).disposed(by: disposeBag)
        
        privacyLabel.rx.tapGesture().when(.recognized)
            .subscribe(onNext: { (_) in
                if UserDataManager.shared.Language == ConstantProject.Language.RU {
                    UIApplication.shared.open(URL(string: "https://vmiske.ua/terms")!,
                                              options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.open(URL(string: "https://vmiske.ua/terms_ua")!,
                                              options: [:], completionHandler: nil)
                }
            }).disposed(by: disposeBag)
    }
    
    private func responseHandler() {
        viewModel.onSignUpResult
            .subscribe(onNext: { [weak self] (result) in
                
                switch result {
                case .success:
                    self?.viewModel.getUserData()
                case .failure(let error):
                    self?.removeLoader()
                    self?.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    private func userDataRecieved() {
        viewModel.onUserDataRecieved
            
            .subscribe(onNext: { [unowned self] (result) in
                self.removeLoader()
                
                switch result {
                case .success:
                    if MainLoginViewController.isFast {
                        self.dismiss(animated: true, completion: nil)
                        MainLoginViewController.isFast = false
                    } else {
                        if UserDataManager.shared.TempProductInBasket == nil ||
                            UserDataManager.shared.TempProductInBasket?.count == 0 {
                            let messVC = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
                            messVC.settings(image: #imageLiteral(resourceName: "success-picture"),
                                            message: "Вітаємо! Ви зареєстровані!".L,
                                            controllerTitle: "",
                                            buttonTitle: "Почати шопінг".L, action: {
                                                (UIApplication.shared.delegate as? AppDelegate)?.setupSideMenu()
                            })
                            self.present(messVC, animated: true, completion: nil)
                        } else {
                            self.showLoader()
                            self.addTempProduct()
                        }
                    }
                    
                case .failure(let error):
                    self.showError(message: error.localizedDescription)
                } 
            }).disposed(by: disposeBag)
    }
    private func addTempProduct() {
        UserDataManager.shared.TempProductInBasket?.forEach { [weak self] product in
            if product.currentType == nil {
                self?.basketViewModel.addProduct(productId: product.info.id,
                                                 quantity: product.info.quantity,
                                                 povId: nil, poId: nil,
                                                 subName: nil,
                                                 subDescription: nil,
                                                 subPrice: nil)
            } else {
                guard let currentT = product.currentType else { return }
                self?.basketViewModel.addProduct(productId: product.info.id,
                                                 quantity: product.info.quantity,
                                                 povId: currentT.povId,
                                                 poId: currentT.poId,
                                                 subName: currentT.name,
                                                 subDescription: currentT.value,
                                                 subPrice: Int(currentT.price))
            }
        }
    }
}


extension RegistrationViewController {
    
    struct SocialSignUpModel {
        let isGoogle: Bool
        let id: String
        let firstName: String?
        let lastName: String?
        let email: String?
    }
}
