//
//  LoginViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: BaseAuthorizationViewController {
    
    @IBOutlet weak var emailTF: VmiskeTextField!
    @IBOutlet weak var passwordTF: SecureVmiskeTextField!
    @IBOutlet weak var forgetPassword: UIButton!
    
    @IBOutlet weak var enterButton: RoundedButton!
    
    private let viewModel = AuthorizedViewModel()
    private let disposeBag = DisposeBag()
    private let basketViewModel = BasketViewModel()

    private var addedFromTempBasket = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureRx()
        responseHandler()
        userDataRecieved()
    }
    
    private func configureRx() {
        
        let allValid = Observable.combineLatest(emailTF.validText, passwordTF.validText) { $0 && $1 }
        allValid.bind(to: enterButton.rx.isEnabled).disposed(by: disposeBag)
        
        enterButton.rx.tap
            .subscribe(onNext: { [unowned self] (_) in
                self.showLoader()
                self.viewModel.signIn(email: self.emailTF.text!, password: self.passwordTF.text!)
            }).disposed(by: disposeBag)
        
        basketViewModel.onAddedProduct
            .subscribe(onNext: { [unowned self](result) in
                self.addedFromTempBasket += 1
                if self.addedFromTempBasket == UserDataManager.shared.TempProductInBasket?.count {
                    self.basketViewModel.getBasketList()
                }
            }).disposed(by: disposeBag)
        
        basketViewModel.onBasketList
            .subscribe(onNext: { (result) in
                (UIApplication.shared.delegate as? AppDelegate)?.setupSideMenu()
            }).disposed(by: disposeBag)
    }
    
    private func responseHandler() {
        viewModel.onSignInResult
            .subscribe(onNext: { [weak self] (result) in
                switch result {
                    
                case .success:
                    self?.viewModel.getUserData()
                case .failure(let error):
                    self?.removeLoader()
                    self?.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    private func userDataRecieved() {
        viewModel.onUserDataRecieved
            .subscribe(onNext: { [weak self] (result) in
                self?.removeLoader()
                switch result {
                case .success:
                    if MainLoginViewController.isFast {
                        self?.dismiss(animated: true, completion: nil)
                        MainLoginViewController.isFast = false
                    } else {
                        if UserDataManager.shared.TempProductInBasket == nil ||
                            UserDataManager.shared.TempProductInBasket?.count == 0 {
                            (UIApplication.shared.delegate as? AppDelegate)?.setupSideMenu()
                        } else {
                            self?.showLoader()
                            self?.addTempProduct()
                        }
                    }
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                }    
            }).disposed(by: disposeBag)
    }
    
    private func addTempProduct() {
        UserDataManager.shared.TempProductInBasket?.forEach { [weak self] product in
            if product.currentType == nil {
                self?.basketViewModel.addProduct(productId: product.info.id,
                                                 quantity: product.info.quantity,
                                                 povId: nil, poId: nil,
                                                 subName: nil,
                                                 subDescription: nil,
                                                 subPrice: nil)
            } else {
                guard let currentT = product.currentType else { return }
                self?.basketViewModel.addProduct(productId: product.info.id,
                                                 quantity: product.info.quantity,
                                                 povId: currentT.povId,
                                                 poId: currentT.poId,
                                                 subName: currentT.name,
                                                 subDescription: currentT.value,
                                                 subPrice: Int(currentT.price))
            }
        }
    }
}
