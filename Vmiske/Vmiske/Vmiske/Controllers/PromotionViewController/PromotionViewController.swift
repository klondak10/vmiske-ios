//
//  PromotionViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class PromotionViewController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func detailAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
