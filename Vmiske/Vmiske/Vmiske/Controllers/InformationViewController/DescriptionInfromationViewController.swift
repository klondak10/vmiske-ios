//
//  DescriptionInfromationViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit


class DescriptionInfromationViewController: BaseViewController {
    
    @IBOutlet weak var textView: DescriptionView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }
}
