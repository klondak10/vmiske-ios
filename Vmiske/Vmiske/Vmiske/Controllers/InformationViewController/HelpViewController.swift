//
//  HelpViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet { collectionView.register(UINib(nibName: "HelpCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: HelpCollectionViewCell.identifier)}
    }
    
    private let itemsList = UserDataManager.shared.Language == ConstantProject.Language.UA ? ConstantProject.Information.helpUA : ConstantProject.Information.helpRU
    

    override func viewDidLoad() {
        super.viewDidLoad()

        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout)
            .estimatedItemSize = CGSize(width: 1, height: 1)
    }
    
}

extension HelpViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HelpCollectionViewCell.identifier, for: indexPath) as! HelpCollectionViewCell
        
        cell.configure(title: itemsList[indexPath.row].0, description: itemsList[indexPath.row].1)
        cell.action = {
            collectionView.performBatchUpdates(nil) { _ in
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
            }
        }
       return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ConstantProject.ReviewCollection.lineSpacing
    }
}
