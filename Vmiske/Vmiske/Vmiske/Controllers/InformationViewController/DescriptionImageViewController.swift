//
//  DescriptionImageViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 27.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class DescriptionImageViewController: BaseViewController {

    @IBOutlet weak var textView: DescriptionView!
    
    
  override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }
}
