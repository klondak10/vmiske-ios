//
//  InfromationViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class InfromationViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet { tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")}
    }
    
    
    private let itemList = UserDataManager.shared.Language == ConstantProject.Language.UA ? ConstantProject.Information.informtaionsUATitles : ConstantProject.Information.informtaionsRUTitles
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
       tableView.deleteSeparatorOnEmptyCell()
    }
}

extension InfromationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.config(nil, text: itemList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
            navigationController?.pushViewController(UIViewController
                .getFromMainStoryboard(id: "helpVC"), animated: true)
        } else if indexPath.row == 1 {
            let descrVC = UIViewController.getFromMainStoryboard(id: "descrImageVC") as! DescriptionImageViewController
            descrVC.title = itemList[indexPath.row]
            descrVC.loadViewIfNeeded()
            descrVC.textView.textView.text = UserDataManager.shared.Language == ConstantProject.Language.UA ? ConstantProject.Information.informtaionsUA[indexPath.row - 1] : ConstantProject.Information.informtaionsRU[indexPath.row - 1]
            navigationController?.pushViewController(descrVC, animated: true)
        } else {
            let descrVC = UIViewController.getFromMainStoryboard(id: "descriptionVC") as! DescriptionInfromationViewController
            descrVC.title = itemList[indexPath.row]
            descrVC.loadViewIfNeeded()
            descrVC.textView.textView.text = UserDataManager.shared.Language == ConstantProject.Language.UA ? ConstantProject.Information.informtaionsUA[indexPath.row - 1] : ConstantProject.Information.informtaionsRU[indexPath.row - 1]
            navigationController?.pushViewController(descrVC, animated: true)
        }
    }
}

