//
//  MessageViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class MessageViewController: BaseViewController {
    
    typealias Action = () -> Void
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var messageLabel: UILabel!
    
    @IBOutlet private weak var button: RoundedButton!
    
    private var removeBackArrow = false
    private var action: Action?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
        if removeBackArrow {
            self.navigationItem.setHidesBackButton(removeBackArrow, animated: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func settings(image: UIImage, message: String, controllerTitle: String, buttonTitle: String,
                  withLogo: Bool = false, removeBackArrow: Bool = false, action: @escaping Action) {
        self.removeBackArrow = removeBackArrow
        loadViewIfNeeded()
        if withLogo { setupLogo() }
        imageView.image = image
        title = controllerTitle
        messageLabel.text = message
        button.setTitle(buttonTitle, for: .normal)
        self.action = action
    }
    
    
    @IBAction func buttonAction(_ sender: Any) {
        action?()
    }
    
}
