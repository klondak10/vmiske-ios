//
//  BasketControllerSwitcher.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

struct BasketControllerSwitcher {
    
    func getBasketViewController() -> UIViewController {
        if (UserDataManager.shared.IsAuthorized && UserDataManager.shared.ProductInBasket?.count ?? 0 != 0 ) ||
        (!UserDataManager.shared.IsAuthorized && UserDataManager.shared.TempProductInBasket?.count ?? 0 != 0 ) {
            return UIViewController.getFromMainStoryboard(id: "basketVC")
        } else {
            let vc = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
            vc.settings(image: #imageLiteral(resourceName: "empty-basket "), message: "Тут нічого немає".L, controllerTitle: "Кошик".L, buttonTitle: "Повернутися до шопінгу".L) { [weak vc] in
                vc?.sideMenuController?.setContentViewController(to: UIViewController
                    .getFromMainStoryboard(id: "baseNavVC"))
            }
            return vc
        }
    }
}
