//
//  UseBonusesViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxGesture

protocol BonusesDelegate {
    func addBonuses(_ count: Int)
}

class UseBonusesViewController: UIViewController {

    @IBOutlet weak var bonusesLabel: UILabel!
    @IBOutlet weak var useBonusesTF: VmiskeTextField!
    @IBOutlet weak var priceLabel: UILabel!
    
    var currentPrice: Int = 0
    
    var delegate: BonusesDelegate?
    
    private let disposeBag = DisposeBag()
    private let haveBonuses = UserDataManager.shared.Bonuses ?? "0"
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let sub = view.subviews[0]
        sub.layer.cornerRadius = sub.bounds.height / 5
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bonusesLabel.text = "\(haveBonuses)"
        useBonusesTF.infoLabel.numberOfLines = 2
        useBonusesTF.infoLabel.adjustsFontSizeToFitWidth = true
        useBonusesTF.infoTextColor = #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1)
        useBonusesTF.showInfo("Введіть необхідну к-сть бонусів для застосування".L, animated: false)
        
        priceLabel.text = String(currentPrice)
        view.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self](_) in
                self?.view.endEditing(true)
            }).disposed(by: disposeBag)
    }
    
    @IBAction func acceptAction(_ sender: Any) {
        if let use = Int(useBonusesTF.text!), let haveBonuses = Int(haveBonuses) {
            if use > haveBonuses || use <= 0 || currentPrice < use {
                useBonusesTF.infoTextColor = #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1)
                useBonusesTF.showInfo("Введена кількість бонусів перевищує кількість доступних бонусів або поточну суму".L, animated: true)
                useBonusesTF.lineColor = #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1)
            } else {
                delegate?.addBonuses(use)
                navigationController?.popViewController(animated: true)
            }
        } else {
            useBonusesTF.infoTextColor = #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1)
            useBonusesTF.showInfo("Ошибка значения", animated: true)
            useBonusesTF.lineColor = #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1)
        }
    }
}
