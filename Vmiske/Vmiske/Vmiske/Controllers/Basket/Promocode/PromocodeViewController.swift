//
//  PromocodeViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol PromocodeDelegate {
    func addPromocode(_ promo: Promocode, _ text: String)
}

class PromocodeViewController: UIViewController {
    
    @IBOutlet weak var promocodeTF: VmiskeTextField!
    @IBOutlet weak var confirmButton: RoundedButton!
    
    var delegate: PromocodeDelegate?

    private let viewModel = BasketViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.onPromocodeInfo
            .subscribe(onNext: { [weak self] (result) in
                self?.removeLoader()
                switch result {
                case .failure(let error):
                    self?.promocodeTF.lineColor = #colorLiteral(red: 0.9185729623, green: 0.1846595407, blue: 0.1107173935, alpha: 1)
                    self?.promocodeTF.showInfo("", animated: true)
                    let alert = UIAlertController(title: "Помилка".L, message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                case .success(let promo):
                    self?.delegate?.addPromocode(promo, self?.promocodeTF.text ?? "")
                    self?.navigationController?.popViewController(animated: true)
                }
            }).disposed(by: disposeBag)
        
            promocodeTF.rx.text.orEmpty.map { $0 != "" }
                              .bind(to: confirmButton.rx.isEnabled)
                              .disposed(by: disposeBag)
        confirmButton.rx.tap.subscribe(onNext: { [weak self] _ in
            self?.confirmPromocodeAction()
            }).disposed(by: disposeBag)
    }
    
    func confirmPromocodeAction() {
        self.showLoader()
        viewModel.checkPromocode(promocodeTF.text ?? "")
    }
}
