//
//  BasketViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SideMenuSwift

class BasketViewController: BaseViewController {
    
    let data = UserDataManager.shared
    @IBOutlet weak var basketCollectionView: UICollectionView! {
        didSet {basketCollectionView.register(UINib(nibName: "BasketCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: BasketCollectionViewCell.identifier) }
    }
    @IBOutlet weak var basketBottomView: BasketBottomDefaultView!
    @IBOutlet weak var widthBottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightBottomViewConstraint: NSLayoutConstraint!
    
    weak var emptyController: UIViewController?
    
    let basketMinPrice = 500.0
    var bottomViewisReduce = false
    
    private var currentPrice = 0
    private var promocode: Promocode? {
        didSet {
            setPrice()
            if promocode == nil { promocodeText = nil}
        }
    }
    private var promocodeText: String?
    private var bonuses: Int? {
        didSet {
            setPrice()
            UserDataManager.shared.UsingBonuses = bonuses == nil ? nil : "\(bonuses!)"
        }
    }
    
    private let basketViewModel = BasketViewModel()
    private let disposeBag = DisposeBag()
    
    private var loading = 0 {
        didSet {
            if loading != 0 { self.showLoader() }
            else { self.removeLoader() }
        }
    }
    private var addedFromTempBasket = 0
    private var justUpdate = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if data.IsAuthorized {
            loading += 1
            basketViewModel.getBasketList()
        }
        navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getBasketList),
                                               name: .UpdateBasketCountItems, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(goToDelivery), name: .UpdatedUserName, object: nil)
        configureBasketView()
        setPrice()
        configureRx()
        if (UserDataManager.shared.Bonuses == "0") {
            basketBottomView.bonusesButton.isHidden = true
        }
        bonuses = UserDataManager.shared.UsingBonuses == nil ? nil : Int(UserDataManager.shared.UsingBonuses!)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    private func goToDelivery() {
        data.TempProductInBasket?.forEach { [weak self] product in
            self?.loading += 1
            if product.currentType == nil {
                self?.basketViewModel.addProduct(productId: product.info.id,
                                                 quantity: product.info.quantity,
                                                povId: nil, poId: nil,
                                                subName: nil,
                                                subDescription: nil,
                                                subPrice: nil)
            } else {
                guard let currentT = product.currentType else { return }
                self?.basketViewModel.addProduct(productId: product.info.id,
                                                 quantity: product.info.quantity,
                                                povId: currentT.povId,
                                                poId: currentT.poId,
                                                subName: currentT.name,
                                                subDescription: currentT.value,
                                                subPrice: Int(currentT.price))
            }
        }
    }
    
    @objc
    private func getBasketList() {
        let count = data.IsAuthorized ? data.ProductInBasket?.count ?? 0 :
                        data.TempProductInBasket?.count ?? 0
        if count == 0 {
            self.setEmptyController()
        } else {
            emptyController?.willMove(toParent: nil)
            emptyController?.view.removeFromSuperview()
            emptyController?.removeFromParent()
        }
        self.basketCollectionView.reloadData()
        self.setPrice()
    }
    
    private func configureRx() {
        
        basketViewModel.onAddedProduct
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                self?.addedFromTempBasket += 1
                if self?.addedFromTempBasket == self?.data.TempProductInBasket?.count {
                    self?.loading += 1
                    self?.justUpdate = false
                    self?.basketViewModel.getBasketList()
                }
            }).disposed(by: disposeBag)
        
        basketViewModel.onBasketList
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (_) in
                self?.loading -= 1
                self?.data.TempProductInBasket = nil
                if self?.addedFromTempBasket != 0 && !self!.justUpdate {
                    self?.justUpdate = true
                    self?.sideMenuController?.menuViewController = UIStoryboard(name: "LeftMenu", bundle: nil)
                        .instantiateViewController(withIdentifier: "leftRegVC")
                    self?.navigationController?
                        .pushViewController(UIViewController.getFromMainStoryboard(id: "delAndPayVC"), animated: true)
                }
            }).disposed(by: disposeBag)
        
        basketViewModel.onDeletedItem
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .success(let res):
                    if res { self?.loading += 1; self?.basketViewModel.getBasketList() }
                    else { self?.showError(message: "Ошибка получения данных. (Delete item).")}
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
        
        basketViewModel.onUpdatedProduct
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .success(let res):
                    if res { self?.loading += 1; self?.basketViewModel.getBasketList() }
                    else { self?.showError(message: "Ошибка получения данных. (Update count).")}
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
        
        basketBottomView.havePromocodeButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                if UserDataManager.shared.IsAuthorized {
                    let promoVC = UIViewController
                        .getFromMainStoryboard(id: "promocodeVC") as! PromocodeViewController
                    promoVC.delegate = self
                    self?.navigationController?.pushViewController(promoVC, animated: true)
                } else {
                    self?.showError(message: "Хотите проверить промокод? Войдите в личный кабинет.")
                }
            }).disposed(by: disposeBag)
        
        basketBottomView.bonusesButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                let bVC = UIViewController
                .getFromMainStoryboard(id: "useBonusesVC") as! UseBonusesViewController
                bVC.delegate = self
                bVC.currentPrice = ((self?.currentPrice ?? 0) + (self?.bonuses ?? 0))
                self?.navigationController?.pushViewController(bVC, animated: true)
            }).disposed(by: disposeBag)
        
        basketBottomView.returnToShopping.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                if let menu = self?.sideMenuController?.menuViewController as? RegisteredLeftMenuViewController {
                    menu.currentMenuIndex = 0
                } else if let menu = self?.navigationController?.sideMenuController?.menuViewController as? UnregisteredLeftMenuViewController {
                    menu.currentMenuIndex = 0
                }
                self?.sideMenuController?.setContentViewController(to: UIViewController
                    .getFromMainStoryboard(id: "baseNavVC"))
            }).disposed(by: disposeBag)
        
        basketBottomView.createOrderButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                if self?.data.IsAuthorized == true {
                    let delVC = UIViewController.getFromMainStoryboard(id: "delAndPayVC") as! DelivetyAndPaymentViewController
                    delVC.promocode = self?.promocode
                    delVC.promocodeText = self?.promocodeText
                    delVC.bonuses = self?.bonuses
                    self?.navigationController?
                        .pushViewController(delVC, animated: true)
                } else {
                    
                    MainLoginViewController.isFast = true
                    self?.present(UIViewController.getFromAuthStoryboard(id: "authNavVC"), animated: true, completion: nil)
                }
            }).disposed(by: disposeBag)
    }
    
    private func configureBasketView() {
        let auth = data.IsAuthorized
        basketBottomView.layer.masksToBounds = false
        basketBottomView.layer.shadowColor = UIColor.black.cgColor
        basketBottomView.layer.shadowOpacity = 0.4
        basketBottomView.layer.shadowRadius = 5
        basketBottomView.layer.shadowOffset = CGSize(width: 0, height: -1)
        widthBottomViewConstraint.constant =  ConstantProject.BasketPart.BasketBottomView.Default.width
        heightBottomViewConstraint.constant = ConstantProject.BasketPart.BasketBottomView.Default.height
        if !auth { heightBottomViewConstraint.constant -= 40 }
        if !auth { basketBottomView.bonusesViews.forEach { $0.isHidden = true }}
        if !auth { bottomViewisReduce = true } else { bottomViewisReduce = false }
        if auth && !(data.Bonuses?.isEmpty ?? false) {
            basketBottomView.haveBonusesLabel.text = data.Bonuses
        } else { basketBottomView.haveBonusesLabel.text = "0" }
    }
    
    private func setPrice() {
        var price = 0.0
        if data.IsAuthorized {
            price = data.ProductInBasket?
                .map { Double($0.price + $0.opt.price) * Double($0.count) }
                .reduce(0,+) ?? 0
        } else {
            price = data.TempProductInBasket?
                .map {Double($0.info.price + ($0.currentType?.price ?? 0.0)) * Double($0.info.quantity) }
                .reduce(0,+) ?? 0
        }
        
        basketBottomView.bonusesViews.forEach { $0.isHidden = (price < basketMinPrice || !data.IsAuthorized) }
        if basketBottomView.bonusesViews[0].isHidden && !bottomViewisReduce { heightBottomViewConstraint.constant -= 40
            bottomViewisReduce = true
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut, animations: {
                self.basketBottomView.layoutIfNeeded()
            }, completion: nil)
        } else if !basketBottomView.bonusesViews[0].isHidden && bottomViewisReduce {
            heightBottomViewConstraint.constant += 40
            bottomViewisReduce = false
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut, animations: {
                self.basketBottomView.layoutIfNeeded()
            }, completion: nil)
        }
       
        var newPrice = price
        if let promo = promocode {
            if price >= promo.minimumSummary {
                if promo.Type == .Percantage {
                    newPrice = price - (Double(price) * promo.summary / 100)
                } else { newPrice = price - (promo.summary)}
            } else {
                showError(message: String(format: "Сума повинна бути більше: %d".L, Int(promo.minimumSummary)))
                promocode = nil
            }
        }
        if let bonuse = bonuses {
            if Int(newPrice) < bonuse {
                bonuses = Int(price)
                return
            }
            newPrice -= Double(bonuse)
        }
        newPrice.round()
        price.round()
        if  price != newPrice {
            currentPrice = Int(newPrice)
            basketBottomView.priceLabel.togetherPrice(Int(newPrice), with: Int(price))
        } else {
            currentPrice = Int(price)
            basketBottomView.priceLabel.togetherPrice(Int(price), with: nil)
        }
    }

    
    private func setEmptyController() {
        let vc = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
        vc.settings(image: #imageLiteral(resourceName: "empty-basket "), message: "Тут нічого немає".L, controllerTitle: "Кошик".L,
                    buttonTitle: "Повернутися до шопінгу".L) { [weak vc] in
            vc?.sideMenuController?.setContentViewController(to: UIViewController
                .getFromMainStoryboard(id: "baseNavVC"))
        }
        
        self.emptyController = vc
        self.addChild(vc)
        vc.view.frame = view.bounds
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    override func restoreFromOffline() {
        loading += 1
        basketViewModel.getBasketList()
    }
}

extension BasketViewController: UICollectionViewDataSource, UICollectionViewDelegate,
                                                UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.IsAuthorized ? data.ProductInBasket?.count ?? 0 :
            data.TempProductInBasket?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BasketCollectionViewCell.identifier, for: indexPath) as! BasketCollectionViewCell
       
        if data.IsAuthorized, let product = data.ProductInBasket?[indexPath.row] {
            cell.config(product)
        }else if let product = data.TempProductInBasket?[indexPath.row] {
            cell.config(product)
        }
        cell.updateCount = { [unowned self] count in
            if self.data.IsAuthorized {
                self.loading += 1
                self.basketViewModel.updateInBasket(basketId: self.data.ProductInBasket![indexPath.row].baskId, quantity: count)
            } else {
                let basket = self.data.TempProductInBasket
                basket?[indexPath.row].info.quantity = count
                self.data.TempProductInBasket = basket
            }
        }
        cell.deleteButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                if self.data.IsAuthorized {
                    self.loading += 1
                    self.basketViewModel.deleteFromBasket(basketId: self.data.ProductInBasket![indexPath.row].baskId)
                } else {
                    var temp = self.data.TempProductInBasket
                    temp?.remove(at: indexPath.row)
                    self.data.TempProductInBasket = temp
                    collectionView.reloadData()
                }
            }).disposed(by: cell.basketDisposeBag)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ConstantProject.BasketCollectionViewCell.width,
                      height: ConstantProject.BasketCollectionViewCell.height)
    }
}

extension BasketViewController: PromocodeDelegate, BonusesDelegate {
    func addPromocode(_ promo: Promocode, _ text: String) {
        promocode = promo
        promocodeText = text
    }
    
    func addBonuses(_ count: Int) {
        self.bonuses = count
    }
}
