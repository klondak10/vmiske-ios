//
//  BonusViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BonusViewController: BaseViewController {

    @IBOutlet weak var bonusesLabel: UILabel!
    
    @IBOutlet weak var statusLAbel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let sub = view.subviews[0]
        sub.layer.cornerRadius = sub.bounds.height / 5
    }
    
    override func viewDidLoad() {
        bonusesLabel.text = UserDataManager.shared.Bonuses ?? "0"
        statusLAbel.text = UserDataManager.shared.Status
    }
}
