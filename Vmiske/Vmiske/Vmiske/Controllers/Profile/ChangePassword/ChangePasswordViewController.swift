//
//  ChangePasswordViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ChangePasswordViewController: BaseViewController {
    
    @IBOutlet weak var oldPasswordTF: SecureVmiskeTextField!
    @IBOutlet weak var newPasswordTF: SecureVmiskeTextField!
    @IBOutlet weak var repitPasswordTF: SecureVmiskeTextField!
    @IBOutlet weak var saveButton: RoundedButton!
    
    private let viewModel = UserViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureRx()
    }
    
    private func configureRx() {
        
        let equalPassword = Observable
            .combineLatest(repitPasswordTF.rx.text.orEmpty, newPasswordTF.rx.text.orEmpty,
                           newPasswordTF.isValidText, oldPasswordTF.isValidText)
            { ($0 == $1) && $2 && $3 }
        equalPassword.bind(to: saveButton.rx.isEnabled).disposed(by: disposeBag)
        
        viewModel.onPasswordChanged
            .subscribe(onNext: { [weak self] (result) in
                
                switch result {
                case .success(_):
                    let messVC = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
                    messVC.settings(image: #imageLiteral(resourceName: "passwordChanged"), message: "Ваш пароль змінено".L, controllerTitle: "Пароль змінено".L, buttonTitle: "Повернутися".L) {
                        messVC.navigationController?.popBackViewControllers(3)
                    }
                    self?.navigationController?.pushViewController(messVC, animated: true)
                    
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    @IBAction func changeAction(_ sender: Any) {
        viewModel.changePassword(oldPassword: oldPasswordTF.text!,
                                 newPassword: newPasswordTF.text!)
    }
}
