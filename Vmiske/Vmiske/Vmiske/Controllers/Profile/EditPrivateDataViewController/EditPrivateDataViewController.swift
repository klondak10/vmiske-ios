//
//  EditPrivateDataViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

class EditPrivateDataViewController: UIViewController {

    @IBOutlet weak var nameTF: VmiskeTextField!
    @IBOutlet weak var lastNameTF: VmiskeTextField!
    @IBOutlet weak var emailTF: VmiskeTextField!
    @IBOutlet weak var phoneTF: PhoneNumberTextField!
    @IBOutlet weak var saveButton: RoundedButton!
    
    private let viewModel = UserViewModel()
    private let getUserDataViewModel = AuthorizedViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        configureUI()
        userDataReceivedHandler()
        
        viewModel.onEditedProfile
            .subscribe(onNext: { [weak self] (result) in
                
                switch result {
                case .success:
                    self?.getUserDataViewModel.getUserData()
                case .failure(let error):
                    self?.removeLoader()
                    self?.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
        
        view.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { (_) in
                self.view.endEditing(true)
            }).disposed(by: disposeBag)
    }
    
    
    private func configureUI() {
        
        phoneTF.isHidden = true
        emailTF.isHidden = true
        
        nameTF.text = UserDataManager.shared.FirstName
        lastNameTF.text = UserDataManager.shared.LastName
        emailTF.text = UserDataManager.shared.Email
        phoneTF.text = UserDataManager.shared.Phone
        
        nameTF.checkTextValidation()
        lastNameTF.checkTextValidation()
        emailTF.checkTextValidation()
        phoneTF.checkTextValidation()
        
        let allValid = Observable
                   .combineLatest(nameTF.isValidText, lastNameTF.isValidText, emailTF.isValidText, phoneTF.isValidText)
                   { $0 && $1 && $2 && $3 }
               
        allValid.bind(to: saveButton.rx.isEnabled)
                   .disposed(by: disposeBag)
    }
    @IBAction func saveAction(_ sender: Any) {
        
        showLoader()
        viewModel.editProfile(firstName: nameTF.text!, lastName: lastNameTF.text!,
                              phone: phoneTF.text!, email: emailTF.text!)
    }
    
    private func userDataReceivedHandler() {
        getUserDataViewModel.onUserDataRecieved
            .subscribe(onNext: { [weak self] (result) in
      
                self?.removeLoader()
                switch result {
                case .success:
                    self?.navigationController?.popViewController(animated: true)
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                }
                }).disposed(by: disposeBag)
    }
}
