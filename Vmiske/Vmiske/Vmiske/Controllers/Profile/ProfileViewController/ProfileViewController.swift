//
//  ProfileViewControllerViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/20/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import TweeTextField

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var name: TweeAttributedTextField!
    
    @IBOutlet weak var lastNameTF: TweeAttributedTextField!
    
    @IBOutlet weak var emailTF: TweeAttributedTextField!
    
    @IBOutlet weak var phoneTF: TweeAttributedTextField!
    
    @IBOutlet weak var vozhakTF: TweeAttributedTextField!
    
    @IBOutlet weak var userTypeTF: TweeAttributedTextField!
    
    @IBOutlet weak var changePasswordButton: UIButton!
    
    @IBOutlet weak var logOutButton: RoundedButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Ред.", style: .plain,
                                                            target: self,
                                                            action: #selector(editProfileViewController))
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
    }
    
    private func configUI() {
        configureTextField(name)
        configureTextField(lastNameTF)
        configureTextField(emailTF)
        configureTextField(phoneTF)
        configureTextField(vozhakTF)
        configureTextField(userTypeTF)
        
        name.text = UserDataManager.shared.FirstName
        lastNameTF.text = UserDataManager.shared.LastName
        emailTF.text = UserDataManager.shared.Email
        phoneTF.text = UserDataManager.shared.Phone
        vozhakTF.text = UserDataManager.shared.LeaderName
        vozhakTF.isHidden = UserDataManager.shared.LeaderName == nil
        userTypeTF.text = UserDataManager.shared.Status
        
        logOutButton.addTarget(self, action: #selector(logOutAction), for: .touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(getNewUserData(_:)), name: .UpdateUserData, object: nil)
    }

    @objc
    private func editProfileViewController() {
        navigationController?.pushViewController(UIViewController.getFromMainStoryboard(id: "editPrivateVC"), animated: true)
    }
    
    @objc
    private func getNewUserData(_ notification: Notification) {
        
        if let user = notification.userInfo?["user"] as? User {
            
            name.text = user.firstName
            lastNameTF.text = user.lastName
            emailTF.text = user.email
            phoneTF.text = user.phone
        }
    }
    
    @objc
    private func logOutAction() {
        
        let alert = UIAlertController(title: "Ой!", message: "Ви дійсно хочете вийти?".L, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Скасувати".L, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Вийти".L, style: .default, handler: { (_) in
            (UIApplication.shared.delegate as? AppDelegate)?.logOut()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func configureTextField(_ textfield: TweeAttributedTextField) {

        textfield.activeLineColor = .clear

        textfield.borderStyle = .none
        
        textfield.lineColor = .clear
        textfield.font = FontHelper.font(type: .regular, size: .medium)
        textfield.placeholderColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        textfield.minimumPlaceholderFontSize = 12
        textfield.placeholderDuration = 0
    }
}
