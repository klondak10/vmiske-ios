//
//  BaseAuthorizationViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BaseAuthorizationViewController: UIViewController {
    
     private var noInternetView: NoInternetConnectionView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func showError(message: String) {
        if message.contains("offline") || message.contains("подключение") || message.contains("підключення") || message.contains("соединение") {
            setupNoInternetView()
        } else {
            super.showError(message: message)
        }
    }
    
    @objc
    private func setupNoInternetView() {
        noInternetView = NoInternetConnectionView(frame: self.view.bounds)
        view.addSubview(noInternetView!)
        view.bringSubviewToFront(noInternetView!)
        noInternetView?.restoreAction = restoreFromOffline
    }
    
    func restoreFromOffline() {
    }
}

