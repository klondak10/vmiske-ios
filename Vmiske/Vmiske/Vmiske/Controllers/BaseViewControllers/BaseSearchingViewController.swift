//
//  BaseSearchingViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BaseSearchingViewController: BaseViewController, UISearchBarDelegate {
    
    var _searchController: MySearchVC!
//    private let _disposeBag = DisposeBag()
//    private let _viewModel = MainPageViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _searchController = MySearchVC(searchResultsController: nil)
        let search = _searchController!

        definesPresentationContext = true
        search.mainVC = self
        self.navigationItem.searchController = search
        search.dimsBackgroundDuringPresentation = false
        search.searchBar.delegate = self
        
        let searchBarTextAttributes = [
            NSAttributedString.Key.font: FontHelper.font(type: .regular, size: .small),
            NSAttributedString.Key.foregroundColor: UIColor.darkGray
        ]
        // Default attributes for search text
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
        
        // Attributed placeholder string
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Я шукаю...".L, attributes: searchBarTextAttributes)
        
        // Search bar cancel button
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(searchBarTextAttributes, for: .normal)
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Скасувати".L
        
       // addStartViewToSearchViewController(searchVC: search)
        navigationItem.hidesSearchBarWhenScrolling = false
    }
}
