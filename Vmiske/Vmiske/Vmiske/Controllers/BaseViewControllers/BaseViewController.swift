//
//  BaseViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    private var noInternetView: NoInternetConnectionView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupRightBarForBasket()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if navigationController?.viewControllers.count == 1 {
            addHamburgerButton()
        }
    }

    override func showError(message: String) {
        if message.contains("offline") || message.contains("подключение") || message.contains("підключення") || message.contains("соединение") {
            setupNoInternetView()
        } else {
            super.showError(message: message)
        }
    }
    
    func restoreFromOffline() {
    }
    
    @objc
    private func setupNoInternetView() {
        noInternetView = NoInternetConnectionView(frame: self.view.bounds)
        view.addSubview(noInternetView!)
        view.bringSubviewToFront(noInternetView!)
        noInternetView?.restoreAction = restoreFromOffline
    }

    func setupRightBarForBasket() {
        let basket = BarButtonBadge(frame: .zero)
        basket.sizeToFit()
        basket.button.addTarget(self, action: #selector(goToBasket), for: .touchUpInside)
        let basketItem = UIBarButtonItem(customView: basket)
        navigationItem.rightBarButtonItem = basketItem
    }
    
    private func addHamburgerButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "hamburger"), style: .plain, target: self, action: #selector(openSideMenu))
    }
    
    @objc private func goToBasket() {
        navigationController?.pushViewController(BasketControllerSwitcher()
            .getBasketViewController(), animated: true) 
    }
    
    @objc
    private func openSideMenu() {
        sideMenuController?.revealMenu()
    }
    
    func setupLogo() {
        let imageView = UIImageView(image:#imageLiteral(resourceName: "Vmiske-logo-orange"))
        navigationItem.titleView = imageView
    }
}
