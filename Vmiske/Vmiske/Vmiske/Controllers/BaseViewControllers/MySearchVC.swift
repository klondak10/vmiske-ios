//
//  MySearchVC.swift
//  Vmiske
//
//  Created by Roman Haiduk on 11.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

protocol MySearchVCDelegate {
    func getNextPage(_ current: Int)
}

class MySearchVC: UISearchController{
    
    var startSearchView: StartSearchingView!
    var collectionView: UICollectionView!

    var pageDelegate: MySearchVCDelegate?
    weak var mainVC: UIViewController?
    
    private var changedTypeIndex: [Int: Int] = [:]
    private var loading = false
    var searched: CategoryProductList? {
        didSet {
            if searched == nil {
                collectionView.isHidden = true
                startSearchView.isHidden = false
                startSearchView.configView(for: true)
            } else if searched?.products.count == 0 {
                collectionView.isHidden = true
                startSearchView.isHidden = false
                startSearchView.configView(for: false)
            } else {
                collectionView.isHidden = false
                startSearchView.isHidden = true
                collectionView.reloadData()
                if loading {
                    searched?.pagination.offset += 1
                    loading = false
                }
                if searched?.pagination.offset == 0 {
                    collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredVertically, animated: true)
                }
            }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.transition(with: self.startSearchView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                   self.startSearchView.isHidden = false
        }, completion: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
       
        UIView.transition(with: self.startSearchView, duration: 2, options: .transitionCrossDissolve, animations: {
            self.startSearchView.isHidden = true
            self.collectionView.isHidden = true
        }, completion: nil)
        removeLoader()
        super.viewWillDisappear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        addCollectionView()
        addStartViewToSearchViewController()
    }
    
    private func setupViewFrame(for view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            view.topAnchor.constraint(equalTo: self.view.topAnchor),
            view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }

    private func addStartViewToSearchViewController() {
        
        startSearchView = StartSearchingView()
        startSearchView.isHidden = true
        view.addSubview(startSearchView)
        setupViewFrame(for: startSearchView)
        startSearchView.configView(for: true)
    }
    
    private func addCollectionView() {
        let flow = UICollectionViewFlowLayout()
        flow.itemSize = CGSize(width: ConstantProject.ProductCollection.GridCollectionCell.width,
        height: ConstantProject.ProductCollection.GridCollectionCell.height)
        flow.minimumLineSpacing = ConstantProject.ProductCollection.GridCollectionCell.lineSpace
        flow.minimumInteritemSpacing = 4
        flow.scrollDirection = .vertical
        flow.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 24)
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: flow)
        collectionView.contentInset = UIEdgeInsets(top: 6, left: 10, bottom: 10, right: 6)
        collectionView.backgroundColor = .white
        collectionView.register(UINib(nibName: "ProductGridCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ProductGridCollectionViewCell.identifier)
        collectionView.register(CollectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        collectionView.isHidden = true
        view.addSubview(collectionView)
        setupViewFrame(for: collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.keyboardDismissMode = .onDrag
    }
}

extension MySearchVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            
            if let headerView = collectionView
                .dequeueReusableSupplementaryView(ofKind: kind,
                                                  withReuseIdentifier: "header",
                                                  for: indexPath) as? CollectionHeader {
                headerView.setCount(searched?.pagination.count ?? 0)
                return headerView
            }
        default: break
        }
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searched?.products.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let product = searched?.products[indexPath.item] else { return UICollectionViewCell() }
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: ProductGridCollectionViewCell.identifier, for: indexPath) as! ProductGridCollectionViewCell
        cell.basketButton.setImage(nil, for: .normal)
        cell.configure(product: product, currentTypeIndex: changedTypeIndex[product.id])
        cell.typeChangeView.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self] (_) in
                let vc = UIViewController.getFromMainStoryboard(id: "changeTypeVC") as! TypeChangeViewController
                vc.delegate = cell
                vc.typeChanged = { [weak self] index in
                    self?.changedTypeIndex[product.id] = index
                }
                vc.configureView(cell.productImageView.image, product.name, type: product.type, currentTypeIndex: cell.getCurrentTypeIndex() ?? 0)
                self?.present(vc, animated: true, completion: nil)
            }).disposed(by: cell.disposeBag)
        cell.showLoader = {
            collectionView.showLoader()
        }
        cell.removeLoader = {
            collectionView.removeLoader()
        }
        cell.showError = { mess in
            self.showError(message: mess)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        guard let product = searched?.products[indexPath.row] else { return }
        let detailProduct = UIViewController.getFromMainStoryboard(id: "tabDetailVC") as! TabDetailViewController
        detailProduct.title = UserDataManager.shared.Language == ConstantProject.Language.UA ? "Пошук" : "Поиск"
        detailProduct.productIndex = product.id
        
        let indexType = (collectionView.cellForItem(at: indexPath) as! ProductGridCollectionViewCell).getCurrentTypeIndex()
        
        detailProduct.productCurrentTypeIndex = indexType ?? -1

        mainVC?.navigationController?.pushViewController(detailProduct, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let pagination = searched?.pagination ?? .zero
        if indexPath.row == (searched?.products.count ?? 0) - 1 &&
                (pagination.count > pagination.limit * (pagination.offset + 1)) &&
            !loading {
            pageDelegate?.getNextPage(pagination.offset)
            loading = true
        }
    }
}


class CollectionHeader: UICollectionReusableView {
    
    private var titleLabel: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configLabel()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configLabel()
    }
    
    private func configLabel() {
        self.backgroundColor = .white
        titleLabel = UILabel()
        self.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        titleLabel.font = FontHelper.font(type: .regular, size: .small)
        titleLabel.textColor = #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1)
    }
    
    func setCount(_ count: Int) {
        titleLabel.text = String(format: UserDataManager.shared.Language == ConstantProject.Language.UA ? "Знайдено %d товарів" : "Найдено %d товаров", count)
    }
}
