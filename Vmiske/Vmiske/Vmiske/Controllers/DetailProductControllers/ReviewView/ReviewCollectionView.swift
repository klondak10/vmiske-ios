//
//  ReviewCollectionView.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit


struct ReviewSwitcher {
    
    func getReviewView(frame: CGRect, reviewCount: Int) -> UIView {
        if reviewCount != 0 {
            return ReviewCollectionView(frame: frame)
        } else {
            return ReviewEmptyView(frame: frame)
        }
    }
    
}


class ReviewCollectionView: UIView {
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let _ = loadViewFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let _ = loadViewFromNib()
    }
    
    private func loadViewFromNib() -> UIView {
        
        let view = UINib(nibName: "ReviewCollectionView", bundle: nil)
            .instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        return view
    }
}

class ReviewEmptyView: UIView {
    
    var action: () -> Void = {}
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let _ = loadViewFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let _ = loadViewFromNib()
    }
    
    private func loadViewFromNib() -> UIView {
        
        let view = UINib(nibName: "ReviewEmptyView", bundle: nil)
            .instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        return view
    }
    
    @IBAction func stepToCreateReviewAction(_ sender: Any) {
        action()
    }
}
