//
//  FastOrderViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 23.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class FastOrderViewController: UIViewController {
    
    @IBOutlet weak var phoneTF: PhoneNumberTextField!
    @IBOutlet weak var abortButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    
    var delegate: TabDetailControllerProtocol?
    
    var product: Product!
    
    private let viewModel = OrderViewModel()
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.removeObserver(phoneTF!)
        
        phoneTF.isValidText
            .bind(to: confirmButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        abortButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                
                self?.willMove(toParent: nil)
                self?.view.removeFromSuperview()
                self?.removeFromParent()
            }).disposed(by: disposeBag)
        
        confirmButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                guard let self = self, let product = self.product else { return }
                
                if self.product.currentType == nil {
                    self.viewModel.createFastOrder(phone: self.phoneTF.text!, prodId: product.info.id)
                } else {
                    self.viewModel.createFastOrder(phone: self.phoneTF.text!, prodId: product.info.id,
                                                   optName: product.currentType!.name, optDesc: product.currentType!.value, optPrice: Int(product.currentType!.price),
                                                   povId: product.currentType?.poId, poId: product.currentType?.povId)
                }
            }).disposed(by: disposeBag)
        
        viewModel.onFastOrder
            .subscribe(onNext: { [weak self] (result) in
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(let res) where res == true:
                    self?.showConfirmOrderAlert()
                default:
                    self?.showError(message: "Сталася невідома помилка 😢".L)
                }
            }).disposed(by: disposeBag)
    }
    
    func showConfirmOrderAlert() {
        delegate?.succesOrder()
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
}
