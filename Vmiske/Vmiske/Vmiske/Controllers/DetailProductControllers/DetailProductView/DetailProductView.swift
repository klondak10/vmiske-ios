//
//  DetailProductView.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture
import FSPagerView

class DetailProductView: UIView, TypeChangeDelegate {

    @IBOutlet weak var bannerView: FSPagerView!
    @IBOutlet weak var pageControl: FSPageControl!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var salesView: UIView!
    @IBOutlet weak var salesLabel: UILabel!
    
    @IBOutlet weak var barcodeLabel: UILabel!
    @IBOutlet weak var articleLabel: UILabel!
    @IBOutlet weak var changeTypeView: UIView!
    @IBOutlet weak var changeTypeLabel: UILabel!
    @IBOutlet weak var reviewCountLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var oldPriceLabel: UILabel!
    @IBOutlet weak var buy1ClickButton: RoundedButton!
    @IBOutlet weak var addInBasketButton: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var notAvailableLabel: UILabel!
    
    @IBOutlet var starRateImageCollection: [UIImageView]!
      
    var delegate: TabDetailControllerProtocol?
    
    private var indexBannerControlWillDisplay = 0
    var currentType = 0 {
        didSet { product?.currentType = product?.info.types[currentType] }
    }
    private let viewModel = BasketViewModel()
    private let orderViewModel = OrderViewModel()
    private let disposeBag = DisposeBag()
    
    private var product: Product? {
        didSet { bannerView.reloadData()}
    }
   
    
    override func layoutSubviews() {
        super.layoutSubviews()
        changeTypeView.layer.cornerRadius = changeTypeView.bounds.height / 2
        changeTypeView.layer.borderWidth = 1
        changeTypeView.layer.borderColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1).cgColor
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let _ = loadViewFromNib()
        configurePager()
        configureRx()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
      //  let _ = loadViewFromNib()
    }
    
    private func loadViewFromNib() -> UIView {
        
        let view = UINib(nibName: "DetailProductView", bundle: nil)
            .instantiate(withOwner: self, options: nil).first as! UIView
        
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        NotificationCenter.default.addObserver(self, selector: #selector(basketCountChanged),
                                               name: .UpdateBasketCountItems, object: nil)
        return view
    }
    
    @IBAction func shareAction(_ sender: UIView) {
        delegate?.shareAction(sender: sender, product!.Link)
    }
    
    @objc
    private func basketCountChanged() {
        if UserDataManager.shared.hasInBasket(product!) {
            addInBasketButton.isSelected = true
        } else {
            addInBasketButton.isSelected = false
        }
        layoutIfNeeded()
    }
    
    //MARK: Configure RX
    private func configureRx() {
        
        changeTypeLabel.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                guard let product = self.product?.info else { return }
                
                let vc = UIViewController.getFromMainStoryboard(id: "changeTypeVC") as! TypeChangeViewController
                vc.delegate = self
                let image = self.bannerView.cellForItem(at: 0)?.imageView?.image
                
                vc.configureView(image, product.name, type: product.types, currentTypeIndex: self.currentType)
                self.delegate?.opentView(vc)
            }).disposed(by: disposeBag)
        
        addInBasketButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                if UserDataManager.shared.IsAuthorized {
                    self.addProductToBasket()
                } else {
                    self.unAuthorizedAddToBasket()
                }
            }).disposed(by: disposeBag)
        
        buy1ClickButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                guard let parentVC = self.delegate as? UIViewController else { return }
                
                let fastVC = UIViewController.getFromMainStoryboard(id: "fastOrderVC") as! FastOrderViewController
                fastVC.product = self.product!
                fastVC.delegate = self.delegate
                parentVC.addChild(fastVC)
                fastVC.view.frame = parentVC.view.frame
                parentVC.view.addSubview(fastVC.view)
                fastVC.didMove(toParent: parentVC)
            }).disposed(by: disposeBag)
        
        viewModel.onAddedProduct
            .subscribe(onNext: { [unowned self] (result) in
                self.removeLoader()
                switch result {
                case .success(let res):
                    if res {
                        self.showLoader()
                        self.viewModel.getBasketList()
                    } else {
                        self.showError("Ошибка добавления товара.".L)
                    }
                default:
                    self.showError("Ошибка добавления товара.".L)
                }
            }).disposed(by: disposeBag)
        
        viewModel.onBasketList
            .subscribe(onNext: {[unowned self] (res) in
                self.removeLoader()
                switch res {
                case .success(let list):
                    UserDataManager.shared.ProductInBasket = list
                case .failure(let error):
                    self.showError(error.localizedDescription)
                }
            }).disposed(by: disposeBag)
        
        viewModel.onDeletedItem
            .subscribe(onNext: { [unowned self] (result) in
                self.removeLoader()
                switch result {
                case .success(let res):
                    if res {
                        self.showLoader()
                        self.viewModel.getBasketList()
                    } else {
                        self.showError("Ошибка удалении товара.".L)
                    }
                default:
                    self.showError("Ошибка удалении товара.".L)
                }
            }).disposed(by: disposeBag)
    }
    
    
    //MARK: Configure UI
    private func configurePager() {
        bannerView.transformer = FSPagerViewTransformer(type: .zoomOut)
        bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        bannerView.delegate = self
        bannerView.dataSource = self
        
        pageControl.contentHorizontalAlignment = .center
        pageControl.itemSpacing = 8
        let control = #colorLiteral(red: 0.5924921036, green: 0.5925064087, blue: 0.5924987197, alpha: 1)
        pageControl.setFillColor(control, for: .selected)
        pageControl.setFillColor(control.withAlphaComponent(0.2), for: .normal)
    }
    
    func configureView(for product: Product) {
        self.product = product
        if currentType != -1 {
            self.product?.currentType = product.info.types[currentType]
        }
        nameLabel.attributedText = product.info.name
            .htmlAttributed(family: nameLabel.font.familyName, size: 14,
                            color: nameLabel.textColor, onLeft: true)
       
        if product.info.types.count == 0 {
            changeTypeView.isHidden = true
            articleLabel.text = product.info.artikul
            barcodeLabel.text = product.info.barcode
            //            if let discount = product.info.discount {
            //                oldPriceLabel.isHidden = false
            //                oldPriceLabel.setOldPrice(Int(product.info.price))
            //                priceLabel.setPrice(Int(discount.price))
            //            } else {
            oldPriceLabel.isHidden = true
            priceLabel.setPrice(Int(product.info.price))
            // }
        } else {
            configureSubprice()
        }
        starRateImageCollection.setupReviewEstimate(Int(product.info.reviewCount.rating.rounded()))
        reviewCountLabel.text = String(format: "%d відгуків".L, product.info.reviewCount.count)
        descriptionTextView.attributedText = product.info.description
            .htmlAttributed(family: descriptionTextView.font?.familyName,
                            size: 12,
                            color: descriptionTextView.textColor ?? .black,
                            onLeft: true)
        if UserDataManager.shared.hasInBasket(self.product!) {
            addInBasketButton.isSelected = true
        } else {
            addInBasketButton.isSelected = false
        }
        layoutIfNeeded()
    }
    
    
    private func configureSubprice() {
        
        guard let currentT = product?.info.types[currentType] else { return }
        changeTypeView.isHidden = false
        changeTypeLabel.text = currentT.name
        articleLabel.text = currentT.artikul
        barcodeLabel.text = currentT.barcode
//        if let discount = product?.info.discount {
//            oldPriceLabel.isHidden = false
//            oldPriceLabel.setOldPrice(Int(currentT.price + product!.info.price))
//            priceLabel.setPrice(Int(currentT.price + discount.price))
//        } else {
            oldPriceLabel.isHidden = true
            priceLabel.setPrice(Int(currentT.price + product!.info.price))
//        }
    }
    
    private func showError(_ message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.delegate?.opentView(alert)
    }

    //MARK: Type change Delegate
    func typeChanged(on index: Int) {
        currentType = index
        product?.currentType = product?.info.types[index]
        if UserDataManager.shared.hasInBasket(self.product!) {
            addInBasketButton.isSelected = true
        } else {
            addInBasketButton.isSelected = false
        }
        configureSubprice()
    }
    
    //MARK: Methods
    
    private func addProductToBasket() {
        guard let product = self.product else { return }
        self.showLoader()
        if !addInBasketButton.isSelected {
            if product.info.types.count == 0 {
                viewModel.addProduct(productId: product.info.id, quantity: 1,
                                          povId: nil, poId: nil, subName: nil,
                                          subDescription: nil, subPrice: nil)
            } else {
                let currentT = product.info.types[self.currentType]
                viewModel.addProduct(productId: product.info.id, quantity: 1,
                                          povId: currentT.povId, poId: currentT.poId,
                                          subName: currentT.name,
                                          subDescription: currentT.value,
                                          subPrice: Int(currentT.price))
            }
        } else {
            if let id = UserDataManager.shared.ProductInBasket?
                .first(where: { $0 == product })?.baskId {
                
                viewModel.deleteFromBasket(basketId: id)
            }
        }
    }
    
    private func unAuthorizedAddToBasket() {

        var saveIt = self.product!
        if self.product!.info.types.count > 0 {
            saveIt.currentType = saveIt.info.types[currentType]
        }
        saveIt.info.quantity = 1
    
        if var basket = UserDataManager.shared.TempProductInBasket {
            if let _ = basket.first(where: { $0 == saveIt }) {
                UserDataManager.shared.TempProductInBasket?.removeAll(where: { $0 == saveIt })
            } else {
                basket.append(saveIt)
                UserDataManager.shared.TempProductInBasket = basket
            }
        } else {
            UserDataManager.shared.TempProductInBasket = [saveIt]
        }
    }
}

//MARK: FSPager delegate, datasource
extension DetailProductView: FSPagerViewDelegate, FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        pageControl.numberOfPages = product?.ImagesPath.count ?? 0
        pageControl.currentPage = 0
        return product?.ImagesPath.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.contentView.layer.shadowColor = UIColor.clear.cgColor
        cell.imageView?.sd_setImage(with: URL(string: product!.ImagesPath[index]), placeholderImage: #imageLiteral(resourceName: "placeholder_photo"))
        cell.imageView?.contentMode = .scaleAspectFit
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, shouldHighlightItemAt index: Int) -> Bool {
        return false
    }
    
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
        indexBannerControlWillDisplay = index
    }
    func pagerView(_ pagerView: FSPagerView, didEndDisplaying cell: FSPagerViewCell, forItemAt index: Int) {
        if pageControl.currentPage == index {
            pageControl.currentPage = indexBannerControlWillDisplay
        }
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        indexBannerControlWillDisplay = targetIndex
    }
}
