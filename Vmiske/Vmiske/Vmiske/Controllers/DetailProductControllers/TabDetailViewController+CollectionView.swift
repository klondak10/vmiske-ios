//
//  TabDetailViewController+CollectionView.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension TabDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reviews.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReviewCollectionViewCell.identifier, for: indexPath) as! ReviewCollectionViewCell
        
        cell.configureCell(reviews[indexPath.row])
        cell.action = {
            collectionView.performBatchUpdates(nil, completion: nil)
            self.reviews[indexPath.row].IsExpandable.toggle()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return ConstantProject.ReviewCollection.lineSpacing
    }
}

extension TabDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productDetailModel?.specification.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CharacteristicTableViewCell.identifier) as? CharacteristicTableViewCell {
            cell.configure(productDetailModel?.specification[indexPath.row].name ?? "", productDetailModel?.specification[indexPath.row].text ?? "") 
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 85
    }
}
