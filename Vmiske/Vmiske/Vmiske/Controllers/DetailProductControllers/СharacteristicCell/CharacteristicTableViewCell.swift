//
//  TableViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class CharacteristicTableViewCell: UITableViewCell {

    static let identifier = "CharacteristicTableViewCell"
    
    @IBOutlet weak var specName: UILabel!
    @IBOutlet weak var specDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func configure(_ name: String, _ text: String) {
        specName.text = name
        specDetail.text = text
        
        layoutIfNeeded()
    }
}
