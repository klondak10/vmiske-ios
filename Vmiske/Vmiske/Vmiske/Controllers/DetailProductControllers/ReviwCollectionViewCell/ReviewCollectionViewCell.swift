//
//  ReviewCollectionViewCell.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class ReviewCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "ReviewCollectionViewCell"
    
    //MARK: Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var readMoreButton: UIButton!
    @IBOutlet weak var readMoreArrowImageView: UIImageView!
    @IBOutlet var rateStarCollectionImageView: [UIImageView]!

    @IBOutlet weak var nameAnswLabel: UILabel!
    @IBOutlet weak var dateAnswLabel: UILabel!
    @IBOutlet weak var answerTextView: UITextView!
    
    @IBOutlet weak var heightOfCellConstraint: NSLayoutConstraint!
    @IBOutlet private weak var maxWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var heightTextViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var answerViewHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: Properties
    var maxWidth: CGFloat? = nil {
        didSet {
            guard let maxWidth = maxWidth else {
                return
            }
            maxWidthConstraint.isActive = true
            maxWidthConstraint.constant = maxWidth
        }
    }
    
    private static let defaultViewHeight: CGFloat = 93.5
    private static let defaultTextHeight: CGFloat = 77
    private static let defaultAnswerViewHeight: CGFloat = 44
    private var textHeight: CGFloat? = nil
    
    var reviewTextHeight: CGFloat  {
       if textHeight == nil {
            textHeight = checkReviewTextHeight()
        }
        return textHeight!
    }
    private var answerViewHeight: CGFloat = 0 {
        didSet {
            answerViewHeightConstraint.constant = answerViewHeight
        }
    }
    
    private var isExpanded = false
    var action:  () -> Void = {}
    
   //MARK: Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 10).cgPath
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
         contentView.backgroundColor = .clear
        
        maxWidth = ConstantProject.ReviewCollection.width
        
        heightOfCellConstraint.constant = 0
        heightTextViewConstraint.constant = 0
        readMoreButton.addTarget(self, action: #selector(readMoreAction), for: .touchUpInside)
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.15
        layer.cornerRadius = 10
        contentView.layer.cornerRadius = 10
        (maxWidthConstraint.firstItem as? UIView)?.layer.cornerRadius = 10
        layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10).cgPath
        layer.shadowOffset = CGSize(width: 0, height: 0.5)
    }
    
    override func prepareForReuse() {
        textHeight = nil
    }
    
    
    //MARK: Methods
    
    func configureCell(_ review: ReviewCollectionCellModel) {
        self.isExpanded = review.IsExpandable
        
        nameLabel.text = review.Review.author
        dateLabel.text = review.Review.CreateAt
        reviewTextView.text = review.Review.text
        rateStarCollectionImageView.setupReviewEstimate(Int(review.Review.rating) ?? 0)
        
        if let answer = review.Review.answer, let _ = review.Review.answer?.answerName{
            nameAnswLabel.text = answer.answerName
            dateAnswLabel.text = answer.AnswerDate
            answerTextView.text = answer.answerText
            answerViewHeight = ReviewCollectionViewCell.defaultAnswerViewHeight + checkAnswerTextHeight()
            nameAnswLabel.superview?.isHidden = false
            nameAnswLabel.superview?.makeDashedBorderLine()
        } else {
            answerViewHeight = 0
        }
        firstSetupCellHeight()
    }
    
    @objc private func readMoreAction() {
        
        isExpanded = !isExpanded
        readMoreArrowImageView.rotate(isExpanded ? .pi : 0)
        let different = self.reviewTextHeight - ReviewCollectionViewCell.defaultTextHeight
        
        UIView.animate(withDuration: 0.3 ) {
            self.readMoreButton.setTitle(self.isExpanded ? "Показати меньше".L : "Читати далі".L, for: .normal)
             self.heightTextViewConstraint.constant = self.isExpanded ? self.reviewTextHeight : ReviewCollectionViewCell.defaultTextHeight
            self.heightOfCellConstraint.constant += self.isExpanded ? different : -different
        }
        action()
    }
    
    
    private func checkAnswerTextHeight() -> CGFloat {
        let size = CGSize(width: ConstantProject.ReviewCollection.width - 40, height: .infinity)
        return answerTextView.sizeThatFits(size).height
    }
    private func checkReviewTextHeight() -> CGFloat {
        let size = CGSize(width: ConstantProject.ReviewCollection.width, height: .infinity)
        return reviewTextView.sizeThatFits(size).height
    }
    
    private func firstSetupCellHeight() {
        
        if reviewTextHeight <= ReviewCollectionViewCell.defaultTextHeight {
            readMoreButton.isHidden = true
            readMoreArrowImageView.isHidden = true
            heightTextViewConstraint.constant = reviewTextHeight
            self.heightOfCellConstraint.constant = ReviewCollectionViewCell.defaultViewHeight + reviewTextHeight + answerViewHeight
        } else {
            readMoreButton.isHidden = false
            readMoreArrowImageView.isHidden = false
            
            heightTextViewConstraint.constant = isExpanded ? reviewTextHeight : ReviewCollectionViewCell.defaultTextHeight
             heightOfCellConstraint.constant = ReviewCollectionViewCell.defaultViewHeight + heightTextViewConstraint.constant + answerViewHeight
            self.readMoreButton.setTitle(self.isExpanded ? "Показати меньше".L : "Читати далі".L, for: .normal)
            
            layoutIfNeeded()
        }
    }
}
