//
//  TabDetailViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


protocol TabDetailControllerProtocol {
    func opentView(_ vc: UIViewController)
    func succesOrder()
    func shareAction(sender: UIView, _ link: String)
}

class TabDetailViewController: BaseViewController {
    
    private var viewPager: WormTabStrip!
    @IBOutlet weak var callUsButton: UIButton!
    private weak var reviewCollectionView: UICollectionView?
    
    
    var reviews: [ReviewCollectionCellModel] = []
    private let viewModel = MainPageViewModel()
    private let disposeBag = DisposeBag()
    var productDetailModel: Product? {
        didSet {
            reviews = productDetailModel?.reviews
            .map { ReviewCollectionCellModel($0) } ?? []
            reviewCollectionView?.reloadData()
        }
    }
    
    
    var productIndex = -1
    var productCurrentTypeIndex = -1
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if viewPager != nil && viewPager.currentTabIndex == 1 {
            setupCreateNewReviewButton()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: 0, y: view.safeAreaInsets.top, width: self.view.frame.size.width, height: self.view.frame.size.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom)
        
        if viewPager != nil { viewPager.frame = frame }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callUsButton.addTarget(self, action: #selector(callButtonPressed), for: .touchUpInside)
        showLoader()
        configureRx()
    }
    
    @objc
    private func callButtonPressed() {
        let alert = UIAlertController(title: "Подзвонити нам".L, message: nil, preferredStyle: .actionSheet)
        let act1 = UIAlertAction(title: "+38 (044) 390 41 00", style: .default) { [unowned self] (_) in
            self.toPhone("380443904100")
        }
        let act2 = UIAlertAction(title: "+38 (067) 006 55 11", style: .default) { [unowned self] (_) in
            self.toPhone("380670065511")
        }
        let act3 = UIAlertAction(title: "Скасувати".L, style: .cancel, handler: nil)
        alert.addAction(act1)
        alert.addAction(act2)
        alert.addAction(act3)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func toPhone(_ number: String) {
       guard let numberURL = URL(string: "telprompt://" + number) else { return }
        UIApplication.shared.open(numberURL, options: [:], completionHandler: nil)
    }
    
    //MARK: Rx configure
    
    private func configureRx() {
        
        viewModel.onProductDetail.subscribe(onNext: { [weak self] (product) in
            
            self?.removeLoader()
            switch product {
                
            case .success(let product):
                self?.productDetailModel = product
                self?.tabPagerConfigure()
                
            case .failure(let error):
                self?.showError(message: error.localizedDescription)
            }
        }).disposed(by: disposeBag)
        
        viewModel.getProductDetail(for: productIndex)
        
    }
    
    //MARK: Configure TAB pager
    private func tabPagerConfigure() {
        
        let frame = CGRect(x: 0, y: view.safeAreaInsets.top, width: self.view.frame.size.width, height: self.view.frame.size.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom)
        if viewPager != nil { viewPager.removeFromSuperview() }
        viewPager = WormTabStrip(frame: frame)
        self.view.addSubview(viewPager) //IMPORTANT!
        viewPager.translatesAutoresizingMaskIntoConstraints = false

        viewPager.delegate = self
        viewPager.eyStyle.wormStyel = .BUBBLE
        viewPager.eyStyle.isWormEnable = true
        viewPager.eyStyle.spacingBetweenTabs = 0
        viewPager.eyStyle.dividerBackgroundColor = .clear
        viewPager.eyStyle.kHeightOfWorm = 35
        viewPager.eyStyle.kHeightOfWormForBubble = 25
        viewPager.eyStyle.tabItemDefaultColor = #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1)
        viewPager.eyStyle.tabItemSelectedColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        viewPager.eyStyle.tabItemSelectedFont = UIFont(name: "Rubik-Medium", size: 17)!
        viewPager.eyStyle.tabItemDefaultFont = UIFont(name: "Rubik-Regular", size: 16)!
        viewPager.eyStyle.WormColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        viewPager.eyStyle.topScrollViewBackgroundColor = .white
        viewPager.eyStyle.contentScrollViewBackgroundColor = .white
    
        //default selected tab
        viewPager.currentTabIndex = 0
        //center the selected tab
        viewPager.shouldCenterSelectedWorm = false
        viewPager.buildUI()
        view.bringSubviewToFront(callUsButton)
    }
    
    // Property shows lastIndex
    internal var lastTabIndexView = 0
    
    //MARK: Add right bar button for Review
    internal func setupCreateNewReviewButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "create-review"), style: .plain, target: self,
                                                            action: #selector(openCreateReviewController))
    }
    @objc private func openCreateReviewController() {
        
        let vc = UIViewController
        .getFromMainStoryboard(id: "createReviewVC") as! CreateReviewViewController
        vc.productId = productIndex
        vc.viewModel = viewModel
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func restoreFromOffline() {
        viewModel.getProductDetail(for: productIndex)
    }
}

//MARK: TabDetailControllerProtocol FastOrderDelegate
extension TabDetailViewController: TabDetailControllerProtocol {
    
    func opentView(_ vc: UIViewController) {
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func succesOrder() {
        DispatchQueue.main.async {
            let messTrue = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
            messTrue.settings(image: #imageLiteral(resourceName: "Group 2"),
                              message: "Дякуємо за покупку! \nНаш менеджер скоро зв’яжеться з вами".L,
                              controllerTitle: "", buttonTitle: "Продовжити покупки".L, withLogo: true, removeBackArrow: true) {
                                messTrue.navigationController?.popViewController(animated: true)
            }
            self.navigationController?.pushViewController(messTrue, animated: true)
        }
    }
    
    func shareAction(sender: UIView, _ link: String) {
        if let website = URL(string: link) {
            let objectsToShare = [website] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

            activityVC.excludedActivityTypes = [UIActivity.ActivityType.addToReadingList]
            
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

//MARK: WORMTabStrip ext
extension TabDetailViewController: WormTabStripDelegate {
    func WTSNumberOfTabs() -> Int {
        return 4
    }
    
    func WTSViewOfTab(index: Int) -> UIView {
        let v = UIView(frame: view.bounds)
        let frame = CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.size.width, height: self.view.frame.size.height - 30))
        
        switch index {
        case 0:
            
            let vc =  DetailProductView(frame: CGRect(origin: .zero, size: CGSize(width: UIScreen.main.bounds.width, height: view.bounds.height)))
            vc.delegate = self
            vc.currentType = productCurrentTypeIndex
            if let product = productDetailModel {
                vc.configureView(for: product)
            }
            return vc
        case 1:
            
            let review = ReviewSwitcher().getReviewView(frame: frame,
                                                        reviewCount: reviews.count)
            if let review = review as? ReviewCollectionView {
                review.collectionView.register(UINib(nibName: ReviewCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: ReviewCollectionViewCell.identifier)
                
                self.reviewCollectionView = review.collectionView
                review.collectionView.delegate = self
                review.collectionView.dataSource = self
                if let layout = review.collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                    layout.estimatedItemSize = CGSize(width:1, height: 1)
                    layout.sectionInset = UIEdgeInsets(top: 20, left: 6, bottom: 0, right: 10)
                }
            } else {
                (review as! ReviewEmptyView).action = { [weak self] in
                    self?.openCreateReviewController()
                }
            }
            return review
            
        case 2:
            return DescriptionView(frame: frame, text: productDetailModel?.info.description ?? "")
        case 3 :
            let table = UITableView(frame: frame)
            table.backgroundColor = .white
            table.separatorStyle = .none
            table.bounces = false
            table.estimatedRowHeight = 80
            table.rowHeight = UITableView.automaticDimension
            table.register(UINib(nibName: "CharacteristicTableViewCell", bundle: nil), forCellReuseIdentifier: CharacteristicTableViewCell.identifier)
            table.delegate = self
            table.dataSource = self
            return table
        default:
            v.backgroundColor = .black
        }
        return v
    }
    
    func WTSTitleForTab(index: Int) -> String {
        switch index {
        case 0:
            return "Товар".L
        case 1:
            return "Відгуки".L
        case 2:
            return "Опис".L
        case 3:
            return "Характеристики".L
        default:
            return "title"
        }
    }
    
    func WTSCurrentViewChanged(index: Int) {
        if index == 1 {
            setupCreateNewReviewButton()
        } else if lastTabIndexView != 2 {
            setupRightBarForBasket()
        }
    }
    
    func WTSReachedLeftEdge(panParam: UIPanGestureRecognizer) {
        
    }
    
    func WTSReachedRightEdge(panParam: UIPanGestureRecognizer) {
        
    }
}


