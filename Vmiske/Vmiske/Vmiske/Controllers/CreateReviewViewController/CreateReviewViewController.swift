//
//  CreateReviewViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

class CreateReviewViewController: BaseViewController {
    
    @IBOutlet weak var nameTF: VmiskeTextField!
    @IBOutlet weak var emailTF: VmiskeTextField!
    
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var createButton: RoundedButton!
    
    @IBOutlet var starButtonCollection: [UIButton]!
    
    private var rate: Int = 5
    var productId: Int?
    
    var viewModel: MainPageViewModel?
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        reviewTextView.layer.cornerRadius = 10
        reviewTextView.layer.borderWidth = 1
        reviewTextView.layer.borderColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1).cgColor
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureRx()
        setStarsFor(index: rate)
    }
    
    private func configureRx() {
        
        
        viewModel?.onAddedReview
            .subscribe(onNext: { [weak self] (review) in
                self?.viewModel?.getProductDetail(for: self?.productId ?? -1)
                
                let mesVC = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
                mesVC.settings(image: #imageLiteral(resourceName: "review-sended"), message: "Дякуемо за відгук!".L, controllerTitle: "Відгук відправлено".L, buttonTitle: "Повернутися".L, action: {
                    self?.navigationController?.popBackViewControllers(3)
                })
                self?.navigationController?.pushViewController(mesVC, animated: true)
                
            }, onError: { [weak self] (error) in
                self?.showError(message: (error as? ResponseError)?.localizedDescription ?? "Error")
            }).disposed(by: disposeBag)
        
        starButtonCollection.forEach { but in
            but.rx.tap
                .subscribe(onNext: { [weak self] (_) in
                    self?.setStarsFor(index: but.tag)
                }).disposed(by: disposeBag)
        }
        
        configureValidation()
        createButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                guard let productId = self?.productId, let `self` = self else { return }
                
                self.viewModel?.createReview(on: productId, author: self.nameTF.text!, email: self.emailTF.text!, text: self.reviewTextView.text, rate: self.rate)

            }).disposed(by: disposeBag)
        
        view.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self] (_) in
                self?.view.endEditing(true)
            }).disposed(by: disposeBag)
    }
    
    private func configureValidation() {
        let allValid = Observable.combineLatest(nameTF.validText, emailTF.validText) { $0 && $1}
        allValid.bind(to: createButton.rx.isEnabled).disposed(by: disposeBag)
    }
    
    
    private func setStarsFor(index: Int) {
        
        rate = index
        
        for (_index, element) in starButtonCollection.enumerated() {
            UIView.transition(with: element, duration: 0.2, options: .transitionCrossDissolve, animations: {
                if _index < index {
                    element.setImage(#imageLiteral(resourceName: "star-orange"), for: .normal)
                } else { element.setImage(#imageLiteral(resourceName: "star-gray"), for: .normal)}
            }, completion: nil)
        }
    }
}
