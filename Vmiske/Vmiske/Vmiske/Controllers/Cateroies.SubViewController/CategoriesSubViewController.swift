//
//  CategoriesSubViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CategoriesSubViewController: BaseSearchingViewController {
    
    @IBOutlet weak var tableView: UITableView! {
        didSet { tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")}
    }
    
    @IBOutlet weak var emptyListView: UIView!
    
    
    private let viewModel = MainPageViewModel()
    private let disposeBag = DisposeBag()
    
    private var catalog: [Category] = [] {
        didSet { tableView.reloadData() }
    }
    var isSubcategories: Int? {
        willSet {
            if let subIndex = newValue {
                viewModel.getSubcategories(for: subIndex)
            } else {
                viewModel.getMainCategories()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.deleteSeparatorOnEmptyCell()

        configureRx()
    }
    
    private func configureRx() {
        
        _searchController.searchBar.rx.text.orEmpty.throttle(1, scheduler: ConcurrentMainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] (searchText) in
                let _search = searchText.trimmingCharacters(in: CharacterSet.whitespaces)
                if _search == "" {
                    if self._searchController.searched == nil {
                        return
                    } else {
                        self._searchController.startSearchView.isHidden = false
                        self._searchController.startSearchView.configView(for: true)
                        self._searchController.collectionView.isHidden = true
                    }
                } else {
                    self._searchController.showLoader()
                    self.viewModel.searchProducts(_search, 0)
                }
        }).disposed(by: disposeBag)
        
        viewModel.onSearchedProducts
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (result) in
                self._searchController.removeLoader()
                switch result {
                case .success(let list):
                    if list.pagination.offset == 0 {
                        self._searchController.searched = list
                    } else {
                        self._searchController.searched?.products.append(contentsOf: list.products)
                    }
                case .failure(let error):
                    self.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
        
        viewModel.onCategories
            .subscribe(onNext: { [weak self] (categories) in
                
                switch categories {                    
                case .success(let categories):
                    if categories.count == 0 {
                        self?.emptyListView.isHidden = false
                    } else {  self?.emptyListView.isHidden = true }
                    self?.catalog = categories
                    
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    override func restoreFromOffline() {
        if let subIndex = isSubcategories {
            viewModel.getSubcategories(for: subIndex)
        } else {
            viewModel.getMainCategories()
        }
    }
}

extension CategoriesSubViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catalog.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.config(nil, text: catalog[indexPath.row].name)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        
        if catalog[indexPath.row].subCategoriesCount != 0 {
            let vc = UIViewController.getFromMainStoryboard(id: "categoriesVC") as! CategoriesSubViewController
            vc.title = cell.textLabel?.text
            vc.isSubcategories = catalog[indexPath.row].id
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let cell = tableView.cellForRow(at: indexPath)!
            let vc = UIViewController.getFromMainStoryboard(id: "productListVC") as! ProductListViewController
            vc.title = cell.textLabel?.text
            vc.listId = catalog[indexPath.row].id
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK: MYSearchDelegate
extension CategoriesSubViewController: MySearchVCDelegate {
    func getNextPage(_ current: Int) {
        if let searchText = _searchController.searchBar.text?
            .trimmingCharacters(in: CharacterSet.whitespaces) {
            
            viewModel.searchProducts( searchText, current + 1)
        }
    }
}
