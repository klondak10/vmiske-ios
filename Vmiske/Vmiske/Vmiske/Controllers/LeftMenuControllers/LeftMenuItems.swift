//
//  LeftMenuItems.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

enum LeftMenuItems: String, CaseIterable {
    
    case Головна
    case Каталог
    case Кошик
    case Покупки
    case Бонуси
    case Налаштування
    case Інформація
}


extension LeftMenuItems {
    
    func returnViewController() -> UIViewController {
        
        switch self {
        case .Головна:
            return UIViewController.getFromMainStoryboard(id: "baseNavVC")
        case .Каталог:
            let vc = UIViewController.getFromMainStoryboard(id: "categoriesVC") as? CategoriesSubViewController
            vc?.isSubcategories = nil
            return  BaseNavigationControllerViewController(rootViewController: vc ?? UIViewController())
        case .Кошик:
            return BaseNavigationControllerViewController(rootViewController: BasketControllerSwitcher().getBasketViewController())
        case .Покупки:
            return BaseNavigationControllerViewController(rootViewController:
                UIViewController.getFromMainStoryboard(id: "purchaseVC"))
        case .Бонуси:
            return BaseNavigationControllerViewController(rootViewController: UIViewController.getFromMainStoryboard(id: "bonusVC"))
        case .Налаштування:
            return BaseNavigationControllerViewController(rootViewController:
                UIViewController.getFromMainStoryboard(id: "settingsVC"))
        case .Інформація:
            return BaseNavigationControllerViewController(rootViewController: UIViewController.getFromMainStoryboard(id: "informVC"))
        }
    }
}

