//
//  UnregisteredLeftMenuViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class UnregisteredLeftMenuViewController: UIViewController {
    
    @IBOutlet weak var enterToStore: UIButton!
    @IBOutlet weak var callUsButton: UIButton!
    private var firstNumberButton: UIButton? {
        didSet {
            if firstNumberButton != nil {
                firstNumberButton?.setTitle("+38 (044) 390 41 00", for: .normal)
                firstNumberButton?.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
                firstNumberButton?.setTitleColor(#colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1), for: .normal)
                firstNumberButton?.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .highlighted)
                firstNumberButton?.layer.cornerRadius = 10
            }
        }
    }
    private var secondNumberButton: UIButton? {
        didSet {
            if secondNumberButton != nil {
                secondNumberButton?.setTitle("+38 (067) 006 55 11", for: .normal)
                secondNumberButton?.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
                secondNumberButton?.setTitleColor(#colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1), for: .normal)
                secondNumberButton?.setTitleColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), for: .highlighted)
                secondNumberButton?.layer.cornerRadius = 10
            }
        }
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet {tableView.register(UINib(nibName: "LeftMenuTableViewCell", bundle: nil),
                                   forCellReuseIdentifier: LeftMenuTableViewCell.identifier) }
    }
    
    private var basketCountItems = 0
    var currentMenuIndex = 0
    private let items: [LeftMenuItems] = [.Головна,.Каталог,.Кошик,.Налаштування,.Інформація]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        basketCountItems = UserDataManager.shared.TempProductInBasket?.count ?? 0
        callUsButton.addTarget(self, action: #selector(callButtonPressed), for: .touchUpInside)
        NotificationCenter.default.addObserver(self, selector: #selector(updateBasketCountItems(_:)), name: .UpdateBasketCountItems, object: nil)
        enterToStore.addTarget(self, action: #selector(goToAuth), for: .touchUpInside)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc
    private func callButtonPressed() {
        let alert = UIAlertController(title: "Подзвонити нам".L, message: nil, preferredStyle: .actionSheet)
        let act1 = UIAlertAction(title: "+38 (044) 390 41 00", style: .default) { [unowned self] (_) in
            self.toPhone("380443904100")
        }
        let act2 = UIAlertAction(title: "+38 (067) 006 55 11", style: .default) { [unowned self] (_) in
            self.toPhone("380670065511")
        }
        let act3 = UIAlertAction(title: "Скасувати".L, style: .cancel, handler: nil)
        alert.addAction(act1)
        alert.addAction(act2)
        alert.addAction(act3)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func toPhone(_ number: String) {
        guard let numberURL = URL(string: "telprompt://" + number) else { return }
        UIApplication.shared.open(numberURL, options: [:], completionHandler: nil)
    }
    
    @objc
    private func updateBasketCountItems(_ notification: Notification) {
        basketCountItems = notification.userInfo?["items"] as? Int ?? 0
        tableView.reloadData()
    }
    
    @objc private func goToAuth() {
        (UIApplication.shared.delegate as? AppDelegate)?.logOut(false)
    }
}

extension UnregisteredLeftMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LeftMenuTableViewCell.identifier) as! LeftMenuTableViewCell
        cell.nameLabel?.text = items[indexPath.row].rawValue.L
        if items[indexPath.row].rawValue == "Кошик".L {
            cell.setBadge(basketCountItems)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height / CGFloat(LeftMenuItems.allCases.count)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currentMenuIndex != indexPath.row {
            sideMenuController?.setContentViewController(to: items[indexPath.row].returnViewController())
            currentMenuIndex = indexPath.row
        }
        sideMenuController?.hideMenu()
    }
}
