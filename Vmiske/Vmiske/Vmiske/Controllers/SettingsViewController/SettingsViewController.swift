//
//  SettingsViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var signOutButton: RoundedButton!
    
    
    @IBOutlet weak var settingsTable: UITableView! { didSet { settingsTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")}}
    
    private let languages = ["Українська", "Русский"]
    private var selectedIndex = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.rightBarButtonItem = nil
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsTable.deleteSeparatorOnEmptyCell()
        if !UserDataManager.shared.IsAuthorized {
            signOutButton.isHidden = true
        }
        selectedIndex = UserDataManager.shared.Language == ConstantProject.Language.UA ? 0 : 1
    }
    
    //Go to autorication view controller
    @IBAction func signOutAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Ой!", message: "Ви дійсно хочете вийти?".L, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Скасувати".L, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Вийти".L, style: .default, handler: { (_) in
            (UIApplication.shared.delegate as? AppDelegate)?.logOut()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = languages[indexPath.row]
        cell.selectionStyle = .none
        cell.layoutMargins = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
        cell.textLabel?.font = FontHelper.font(type: .regular, size: .medium)
        cell.textLabel?.textColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        if selectedIndex == indexPath.row {
            cell.accessoryType = .checkmark
        } else { cell.accessoryType = .none}
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        UserDataManager.shared.Language = selectedIndex == 0 ? ConstantProject.Language.UA :
            ConstantProject.Language.RU
        Bundle.setLanguage(UserDataManager.shared.Language)
        (UIApplication.shared.delegate as? AppDelegate)?.reloadControllersOnLanguage()
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "  Мова інтерфейсу".L
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = .white
        (view as! UITableViewHeaderFooterView).textLabel?.font = FontHelper.font(type: .medium, size: .medium)
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = .white
    }
}
