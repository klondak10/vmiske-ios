//
//  ViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import FSPagerView
import RxSwift
import RxCocoa


class MainScreenViewController: BaseSearchingViewController {
    
    //MARK: OUTLETS
    @IBOutlet weak var bannerView: FSPagerView!
    @IBOutlet weak var bannerControl: FSPageControl!
    @IBOutlet weak var tableView: UITableView! {
        didSet { tableView.register(UITableViewCell.self, forCellReuseIdentifier: "catCell")}
    }
    
    //MARK: Properties
    private var indexBannerControlWillDisplay = 0
    private var banners: [Banner] = [] {
        didSet {  bannerView.reloadData()  }
    }
    private var catalog: [Category] = [] {
        didSet { tableView.reloadData() }
    }
    
    private var loading = 0 {
        didSet {
            if loading != 0 {
                self.showLoader()
            } else { self.removeLoader()}
        }
    }
    
    private let getUserDataViewModel = AuthorizedViewModel()
    private let basketViewModel = BasketViewModel()
    private let viewModel = MainPageViewModel()
    private let disposeBag = DisposeBag()
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureRx()
        userDataReceivedHandler()
        if UserDataManager.shared.IsAuthorized {
            getUserDataViewModel.getUserData()
            basketViewModel.getBasketList()
            loading += 2
        }
        _searchController.searchBar.rx.text.orEmpty.throttle(1, scheduler: ConcurrentMainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] (searchText) in
                let _search = searchText.trimmingCharacters(in: CharacterSet.whitespaces)
                if _search == "" {
                    if self._searchController.searched == nil {
                        return
                    } else {
                        self._searchController.startSearchView.isHidden = false
                        self._searchController.startSearchView.configView(for: true)
                        self._searchController.collectionView.isHidden = true
                    }
                } else {
                    self._searchController.showLoader()
                    self.viewModel.searchProducts(_search, 0)
                }
        }).disposed(by: disposeBag)
        
        viewModel.onSearchedProducts
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (result) in
                self._searchController.removeLoader()
                switch result {
                case .success(let list):
                    if list.pagination.offset == 0 {
                        self._searchController.searched = list
                    } else {
                        self._searchController.searched?.products.append(contentsOf: list.products)
                    }
                case .failure(let error):
                    self.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }

    //MARK: Methods
    private func configureUI() {
        configurePagerView()
        setupLogo()
        tableView.deleteSeparatorOnEmptyCell()
        _searchController.pageDelegate = self
    }
    
    func configureRx() {
        viewModel.onBannerList.subscribe(onNext: { [weak self] (banners) in
            self?.loading -= 1
            switch banners {
            case .success(let banners):
                self?.banners = banners
                
            case .failure(let error):
                 self?.showError(message: error.localizedDescription)
            }
        }).disposed(by: disposeBag)
        
        viewModel.onCategories.subscribe(onNext: { [weak self] (categories) in
            self?.loading -= 1
            switch categories {
            case .success(let categories):
                self?.catalog = categories
                
            case .failure(let error):
                self?.showError(message: error.localizedDescription)
            }
        }).disposed(by: disposeBag)
   
        viewModel.getBanner()
        viewModel.getMainCategories()
        self.loading += 2
        
        basketViewModel.onBasketList
            .subscribe(onNext: { [unowned self] (result) in
                self.loading -= 1
                switch result {
                case .success(let list):
                    UserDataManager.shared.ProductInBasket = list
                case .failure(let error):
                    self.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
    }
    
    private func userDataReceivedHandler() {
           getUserDataViewModel.onUserDataRecieved
               .subscribe(onNext: { [weak self] (result) in
         
                self?.loading -= 1
                   switch result {
                   case .success:
                       self?.navigationController?.popViewController(animated: true)
                   case .failure(let error):
                       self?.showError(message: error.localizedDescription)
                   }
                   }).disposed(by: disposeBag)
       }
    
    
    private func configurePagerView() {
        
        bannerView.transformer = FSPagerViewTransformer(type: .zoomOut)
        bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        bannerView.delegate = self
        bannerView.dataSource = self
        
        bannerControl.contentHorizontalAlignment = .center
        bannerControl.itemSpacing = 8
        bannerControl.setFillColor(#colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1), for: .selected)
        bannerControl.setFillColor(UIColor(white: 0, alpha: 0.3), for: .normal)
    }
    
    override func restoreFromOffline() {
        viewModel.getBanner()
        viewModel.getMainCategories()
        self.loading += 2
        if UserDataManager.shared.IsAuthorized {
            getUserDataViewModel.getUserData()
            basketViewModel.getBasketList()
            loading += 2
        }
    }
}

//MARK: TableView Delegate
extension MainScreenViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return catalog.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "catCell")!
        cell.config(nil, text: catalog[indexPath.row].name)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        
        let vc = UIViewController.getFromMainStoryboard(id: "categoriesVC") as! CategoriesSubViewController
        vc.title = cell.textLabel?.text
        vc.isSubcategories = catalog[indexPath.row].id
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK: FSPagerViewDelegate
extension MainScreenViewController: FSPagerViewDelegate, FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        bannerControl.numberOfPages = banners.count
        bannerControl.currentPage = 0
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
//        let promoVC = UIViewController.getFromMainStoryboard(id: "promoVC") as! PromotionViewController
//        promoVC.loadViewIfNeeded()
//        promoVC.imageView.sd_setImage(with: URL(string: banners[index].imagePath))
//        navigationController?.pushViewController(promoVC, animated: true)
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.contentView.layer.shadowColor = UIColor.clear.cgColor
        cell.imageView?.sd_setImage(with: URL(string: banners[index].imagePath), placeholderImage: #imageLiteral(resourceName: "placeholder_photo"))
        cell.imageView?.contentMode = .scaleAspectFit
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
        return false
    }
    
    func pagerView(_ pagerView: FSPagerView, didHighlightItemAt index: Int) {
        pagerView.cellForItem(at: index)?.isHighlighted = false
    }
    
    func pagerView(_ pagerView: FSPagerView, willDisplay cell: FSPagerViewCell, forItemAt index: Int) {
        indexBannerControlWillDisplay = index
        pagerView.cellForItem(at: index)?.isHighlighted = false
    }
    func pagerView(_ pagerView: FSPagerView, didEndDisplaying cell: FSPagerViewCell, forItemAt index: Int) {
        if bannerControl.currentPage == index {
            bannerControl.currentPage = indexBannerControlWillDisplay
        }
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        indexBannerControlWillDisplay = targetIndex
    }
}

//MARK: MYSearchDelegate
extension MainScreenViewController: MySearchVCDelegate {
    func getNextPage(_ current: Int) {
        if let searchText = _searchController.searchBar.text?
            .trimmingCharacters(in: CharacterSet.whitespaces) {
            
            viewModel.searchProducts( searchText, current + 1)
        }
    }
}
