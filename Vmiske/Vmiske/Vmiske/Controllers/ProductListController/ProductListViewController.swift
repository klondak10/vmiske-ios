//
//  ProductListViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxGesture
import RxCocoa

class ProductListViewController: BaseSearchingViewController {

    //MARK: Outlets
    @IBOutlet weak var emptyListView: UIView!
    @IBOutlet weak var filteView: UIStackView!
    @IBOutlet weak var sortView: UIStackView!
    @IBOutlet weak var changeLayoutButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib(nibName: "ProductListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ProductListCollectionViewCell.identifier)
            collectionView.register(UINib(nibName: "ProductGridCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ProductGridCollectionViewCell.identifier)
        }
    }
    
    //MARK: Properties
    private var productList: CategoryProductList? {
        willSet{
            collectionView.reloadData()
            
            emptyListView.isHidden = newValue?.products.count == 0 ? false : true
        }
    }
    private var changedTypeProducts: [Int: Int] = [:]
    
    private var allFilters: [Filters]?
    private var filters: [Filters] = []
    private var filterRequstItems: [Filters.FilterItem] = [] {
        didSet{
            pagination = .zero
            viewModel.getProductList(id: listId, pagination: pagination,
                                     sort: sorting, filters: filterRequstItems)
        }
    }
    private var pagination = PaginationModel(count: 0, limit: 20, offset: 0)
    private var sorting: SortingState? {
        didSet {
            pagination = .zero
            viewModel.getProductList(id: listId, pagination: pagination,
                                     sort: sorting, filters: filterRequstItems)
        }
    }
    
    
    var listId: Int = -1 {
        didSet {
            if listId > 0 {
                viewModel.getProductList(id: listId, pagination: pagination,
                                         sort: sorting, filters: filterRequstItems)
            }
        }
    }
    private let viewModel = MainPageViewModel()
    private let basketViewModel = BasketViewModel()
    private let disposeBag = DisposeBag()
    
    private var isListCell = true {
        didSet {
            collectionView.reloadData()
        }
    }
    //MARK: Lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureRx()
    }
    
    //MARK: Rx Config
    private func configureRx() {
        
        _searchController.searchBar.rx.text.orEmpty.throttle(1, scheduler: ConcurrentMainScheduler.instance)
            .distinctUntilChanged()
            .subscribe(onNext: { [unowned self] (searchText) in
                let _search = searchText.trimmingCharacters(in: CharacterSet.whitespaces)
                if _search == "" {
                    if self._searchController.searched == nil {
                        return
                    } else {
                        self._searchController.startSearchView.isHidden = false
                        self._searchController.startSearchView.configView(for: true)
                        self._searchController.collectionView.isHidden = true
                    }
                } else {
                    self.showLoader()
                    self.viewModel.searchProducts(_search, 0)
                }
        }).disposed(by: disposeBag)
        
        viewModel.onSearchedProducts
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (result) in
                self._searchController.removeLoader()
                switch result {
                case .success(let list):
                    if list.pagination.offset == 0 {
                        self._searchController.searched = list
                    } else {
                        self._searchController.searched?.products.append(contentsOf: list.products)
                    }
                case .failure(let error):
                    self.showError(message: error.localizedDescription)
                }
            }).disposed(by: disposeBag)
        
        viewModel.onProductList
            .subscribe(onNext: { [weak self] (products) in
                
                switch products {
                    
                case .success(let products):
                    
                    if products.pagination.offset == 0 {
                        self?.productList = products
                        self?.collectionView.setContentOffset(.zero, animated: true)
                    } else {
                        self?.productList?.products.append(contentsOf: products.products)
                    }
                    self?.pagination = products.pagination
                    self?.filters = products.filters
                    if self?.allFilters == nil { self?.allFilters = products.filters }
                    
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                    
                }
            }).disposed(by: disposeBag)
        
        changeLayoutButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                guard let `self` = self else { return }
                self.isListCell = !self.isListCell
                if self.isListCell{
                    self.collectionView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
                } else {
                    self.collectionView.contentInset = UIEdgeInsets(top: 8, left: 7, bottom: 8, right: 7)
                }
                self.changeLayoutButton.setImage( self.isListCell ? #imageLiteral(resourceName: "grid-pic") : #imageLiteral(resourceName: "list-pic"), for: .normal)
            }).disposed(by: disposeBag)
        
        sortView.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [weak self] (_) in
                self?.showSortAlert()
            }).disposed(by: disposeBag)
        
        filteView.rx.tapGesture()
            .when(.recognized)
            .subscribe(onNext: { [unowned self] (_) in
                let vc = UIViewController.getFromMainStoryboard(id: "filterVC") as! FilterViewController
                vc.configViewController(productId: self.listId, choisenFilters: self.filterRequstItems, allFilters: self.allFilters ?? [])
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }).disposed(by: disposeBag)
    }
    
    //MARK: ALERT Method
    private func showSortAlert() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let def = UIAlertAction(title: "за замовчуванням".L, style: .default) { [weak self] _ in
            self?.sorting = nil
        }
        let fromChip = UIAlertAction(title: "від дешевих до дорогих".L,
                                     style: .default) { [weak self] _ in
            self?.sorting = SortingState(sortType: .price, direction: .ASC)
        }
        let fromExpensive = UIAlertAction(title: "від дорогих до дешевих".L,
                                          style: .default) { [weak self] _ in
            self?.sorting = SortingState(sortType: .price, direction: .DESC)
        }
        let fromA = UIAlertAction(title: "А-Я".L, style: .default) { [weak self] _ in
            self?.sorting = SortingState(sortType: .name, direction: .ASC)
        }
        let fromZ = UIAlertAction(title: "Я-А".L, style: .default) { [weak self] _ in
            self?.sorting = SortingState(sortType: .name, direction: .DESC)
        }
        let cancel = UIAlertAction(title: "Скасувати".L, style: .cancel, handler: nil)
        
        alert.addAction(def); alert.addAction(fromChip); alert.addAction(fromExpensive)
        alert.addAction(fromA); alert .addAction(fromZ); alert.addAction(cancel)
    
        present(alert, animated: true, completion: nil)
    }
    
    override func restoreFromOffline() {
        viewModel.getProductList(id: listId, pagination: pagination,
        sort: sorting, filters: filterRequstItems)
    }
}

//MARK: Filer VC delegate
extension ProductListViewController: FiltersViewControllerDelegate {
    func abortFiltes() {
        filterRequstItems = []
    }
    
    func filtersChanged(_ filters: [Filters.FilterItem]) {
        filterRequstItems = filters
    }
}

//MARK: Extension CollectionView
extension ProductListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList?.products.count ?? 0
    }
 
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let product = productList?.products[indexPath.row] else { return UICollectionViewCell()}
        
        if isListCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductListCollectionViewCell.identifier, for: indexPath) as! ProductListCollectionViewCell
            cell.basketButton.setImage(nil, for: .normal)
            cell.configure(product: product, currentTypeIndex: changedTypeProducts[product.id])
            cell.typeChangeView.rx.tapGesture()
                .when(.recognized)
                .subscribe(onNext: { [weak self] (_) in
                    
                    let vc = UIViewController.getFromMainStoryboard(id: "changeTypeVC") as! TypeChangeViewController
                    vc.delegate = cell
                    vc.configureView(cell.productImageView.image, product.name, type: product.type, currentTypeIndex: cell.getCurrentTypeIndex() ?? 0)
                    vc.typeChanged = { [weak self] index in
                        self?.changedTypeProducts[product.id] = index
                    }
                    self?.navigationController?.present(vc, animated: true, completion: nil)
                }).disposed(by: cell.disposeBag)
            cell.showLoader = {
                collectionView.showLoader()
            }
            cell.removeLoader = {
                collectionView.removeLoader()
            }
            cell.showError = { mess in
                self.showError(message: mess)
            }
            
            if UserDataManager.shared.hasInBasket(product) {
                cell.basketButton.isSelected = true
                cell.basketButton.setImage(UIImage(named: "in-basket"), for: .normal)
            } else {
                cell.basketButton.isSelected = false
                cell.basketButton.setImage(UIImage(named: "add-to-basket"), for: .normal)
            }
            
            return cell
        } else {
            
            let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: ProductGridCollectionViewCell.identifier, for: indexPath) as! ProductGridCollectionViewCell
            cell.basketButton.setImage(nil, for: .normal)
            cell.configure(product: product, currentTypeIndex: changedTypeProducts[product.id])
            cell.typeChangeView.rx.tapGesture()
                .when(.recognized)
                .subscribe(onNext: { [weak self] (_) in
                    
                    let vc = UIViewController.getFromMainStoryboard(id: "changeTypeVC") as! TypeChangeViewController
                    vc.delegate = cell
                    vc.configureView(cell.productImageView.image, product.name, type: product.type, currentTypeIndex: cell.getCurrentTypeIndex() ?? 0)
                    vc.typeChanged = { [weak self] index in
                        self?.changedTypeProducts[product.id] = index
                    }
                    self?.navigationController?.present(vc, animated: true, completion: nil)
                }).disposed(by: cell.disposeBag)            
            cell.showLoader = {
                collectionView.showLoader()
            }
            cell.removeLoader = {
                collectionView.removeLoader()
            }
            cell.showError = { mess in
                self.showError(message: mess)
            }
            if UserDataManager.shared.hasInBasket(product) {
                cell.basketButton.isSelected = true
                cell.basketButton.setImage(UIImage(named: "in-basket"), for: .normal)
            } else {
                cell.basketButton.isSelected = false
                cell.basketButton.setImage(UIImage(named: "add-to-basket"), for: .normal)
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        guard let product = productList?.products[indexPath.row] else { return }
        let detailProduct = UIViewController.getFromMainStoryboard(id: "tabDetailVC") as! TabDetailViewController
        detailProduct.title = title
        detailProduct.productIndex = product.id
        
        let indexType = isListCell ? (collectionView.cellForItem(at: indexPath) as! ProductListCollectionViewCell).getCurrentTypeIndex() : (collectionView.cellForItem(at: indexPath) as! ProductGridCollectionViewCell).getCurrentTypeIndex()
        
        detailProduct.productCurrentTypeIndex = indexType ?? -1

        navigationController?.pushViewController(detailProduct, animated: true)
    }
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return isListCell ? CGSize(width: ConstantProject.ProductCollection.ListCollectionCell.width,
                                   height: ConstantProject.ProductCollection.ListCollectionCell.height) :
        CGSize(width: ConstantProject.ProductCollection.GridCollectionCell.width,
               height: ConstantProject.ProductCollection.GridCollectionCell.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return isListCell ? 1 : ConstantProject.ProductCollection.GridCollectionCell.lineSpace
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == (productList?.products.count ?? 0) - 1 &&
            (pagination.count > pagination.limit * (pagination.offset + 1)) {
            viewModel.getProductList(id: listId,
                                     pagination: pagination.getInrementItem(),
                                     sort: sorting, filters: filterRequstItems)
        }
        if let cell = cell as? ProductListCollectionViewCell {
            cell.basketCountChanged()
        } else if let cell = cell as? ProductGridCollectionViewCell {
            cell.basketCountChanged()
        }
    }
}

//MARK: MYSearchDelegate
extension ProductListViewController: MySearchVCDelegate {
    func getNextPage(_ current: Int) {
        if let searchText = _searchController.searchBar.text?
            .trimmingCharacters(in: CharacterSet.whitespaces) {
            
            viewModel.searchProducts( searchText, current + 1)
        }
    }
}
