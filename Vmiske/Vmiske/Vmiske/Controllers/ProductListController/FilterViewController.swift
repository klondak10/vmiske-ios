//
//  FilterViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol FiltersViewControllerDelegate {
    
    func abortFiltes()
    
    func filtersChanged(_ filters: [Filters.FilterItem])
}

class FilterViewController: BaseViewController {

    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView! {
        didSet { tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")}
    }
    @IBOutlet weak var saveButton: UIButton!
    
    //MARK: Properties
    var delegate: FiltersViewControllerDelegate?
    
    private var productId = -1
    private var allFilters: [FilterTableModel] = [] {
        didSet { tableView.reloadData() }
    }
    private var choisenFilters: [Filters.FilterItem] = []
    
    private var viewModel = MainPageViewModel()
    private let disposeBag = DisposeBag()
    
    
    //MARK: Lyfecycles
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Скинути".L, style: .plain, target: self, action: #selector(abortFilter))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.deleteSeparatorOnEmptyCell()
        saveButton.addTarget(self, action: #selector(saveFilter), for: .touchUpInside)
       // configureRx()
    }
    
    //MARK: @Objc Methods
    @objc private func abortFilter() {
        delegate?.abortFiltes()
        navigationController?.popViewController(animated: true)
    }
    @objc private func saveFilter() {
        delegate?.filtersChanged(choisenFilters)
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: Methods
//    private func configureRx() {
//        viewModel.onProductList
//            .subscribe(onNext: { [weak self] (productList) in
//                self?.allFilters = productList.filters
//                            .map{ FilterTableModel(filter: $0, isCollapsed: false)}
//            }, onError: { [weak self] (error) in
//                self?.showError(message: (error as? ResponseError)?.localizedDescription ?? "")
//            }).disposed(by: disposeBag)
//    }
    
    func configViewController(productId: Int, choisenFilters: [Filters.FilterItem], allFilters: [Filters]) {
        loadViewIfNeeded()
        self.productId = productId
        self.choisenFilters = choisenFilters
        self.allFilters = allFilters
                                   .map{ FilterTableModel(filter: $0, isCollapsed: false)}
        
    }
}

extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allFilters[section].isCollapsed ? 0 : allFilters[section].filter.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let fItem = allFilters[indexPath.section].filter.value[indexPath.row]
        cell.layoutMargins = UIEdgeInsets(top: 8, left: 50, bottom: 8, right: 8)
        cell.textLabel?.text = fItem.subname
        cell.accessoryType = choisenFilters.contains { fItem.id == $0.id }  ? .checkmark : .none
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let choisenItem = allFilters[indexPath.section].filter.value[indexPath.row]
        
        if choisenFilters.contains(where: { $0.id == choisenItem.id}) {
            
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
            choisenFilters.removeAll { choisenItem.id == $0.id}
            
        } else {
            
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            choisenFilters.append(choisenItem)
        }
//        viewModel.getProductList(id: productId, pagination: .zero,
//        sort: nil, filters: choisenFilters)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return allFilters.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = allFilters[section].filter.name
        header.setCollapsed(allFilters[section].isCollapsed)
        header.section = section
        header.delegate = self
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension FilterViewController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ section: Int, _ header: CollapsibleTableViewHeader) {

        // Toggle collapse
        allFilters[section].isCollapsed.toggle()
        header.setCollapsed(allFilters[section].isCollapsed)
        
        // Reload the whole section
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
}
