//
//  TypeChangeViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

protocol TypeChangeDelegate {
    func typeChanged(on index: Int)
}

class TypeChangeViewController: UIViewController {
    
    @IBOutlet weak var _contentView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView! {
        didSet { tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")}
    }
    @IBOutlet weak var visualView: UIVisualEffectView!
    
    var delegate: TypeChangeDelegate?
    private var currentIndex = 0
    
    private var type: [Product.ProductType] = [] {
        didSet {
            guard tableView != nil else { return }
            tableView.reloadData()
        }
    }
    var typeChanged: ((Int) -> Void)?
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        _contentView.layer.cornerRadius = 10
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.deleteSeparatorOnEmptyCell()
    }
    
    func configureView(_ img: UIImage?, _ name: String, type: [Product.ProductType], currentTypeIndex: Int) {
        loadViewIfNeeded()
        
        self.imageView.image = img
        self.descriptionLabel.text = name
        self.type = type
        self.currentIndex = currentTypeIndex
    }
}

extension TypeChangeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return type.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = type[indexPath.row].name
        if indexPath.row == currentIndex {
            cell.accessoryType = .checkmark
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.accessoryType = .checkmark
        self.delegate?.typeChanged(on: indexPath.row)
        self.typeChanged?(indexPath.row)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if let indexP = tableView.indexPathForSelectedRow {
            tableView.cellForRow(at: indexP)?.accessoryType = .none
        }
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = .white
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if type.count >= 1 {
            return type[0].value
        } else { return "" }
    }
  
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}
