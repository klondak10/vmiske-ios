//
//  FastRegistrationViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FastRegistrationViewController: BaseViewController {

    @IBOutlet weak var nameTF: VmiskeTextField!
    @IBOutlet weak var lastNameTF: VmiskeTextField!
    @IBOutlet weak var emailTF: VmiskeTextField!
    @IBOutlet weak var phoneTF: PhoneNumberTextField!
    @IBOutlet weak var enterButton: RoundedButton!
    
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureRx()
    }
    
    private func configureRx() {
        
        let allValid = Observable.combineLatest(nameTF.validText, lastNameTF.validText,
                                                emailTF.validText, phoneTF.validText) {
                                                    $0 && $1 && $2 && $3 }
        allValid.bind(to: enterButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        enterButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                self?.navigationController?
                    .pushViewController(UIViewController
                        .getFromMainStoryboard(id: "delAndPayVC"), animated: true)
            }).disposed(by: disposeBag)
    }
}
