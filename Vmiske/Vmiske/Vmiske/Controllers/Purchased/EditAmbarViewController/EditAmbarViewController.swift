//
//  EditAmbarViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import TweeTextField
import RxSwift
import RxCocoa

class EditAmbarViewController: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var productCollectionView: UICollectionView! {
        didSet { productCollectionView.register(UINib(nibName: "EditAmbarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: EditAmbarCollectionViewCell.identifier)} }
    @IBOutlet weak var datePickTF: TweeActiveTextField!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var abortButton: RoundedButton!
    @IBOutlet weak var saveButton: RoundedButton!
    
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    
    var orderInfo: OrderInfo? {
        didSet {
            collectionHeightConstraint.constant = CGFloat(orderInfo?.products.count ?? 0) * ConstantProject.PurchasedPart.EditAmbarCollectionViewCell.height + 80
            if let ambarId = selectedAmbarId, let currentAmbar = orderInfo?.ambars
                .first(where: { $0.info.ambarId == ambarId})  {
                commentTextView.text = currentAmbar.info.customerText
                datePickTF.text = currentAmbar.info.date
                currentAmbar.products
                    .forEach { self.selectedEditProduct["\($0.order_product_id)"] = "\($0.quantity)"}
            }
            view.layoutIfNeeded()
        }
    }
    var selectedAmbarId: Int?
    
    private let viewModel = OrderViewModel()
    private let disposeBag = DisposeBag()
  //  private let hasSelectedProducts = BehaviorRelay<Bool>(value: false)
    
    var defaultSelected: [String: String] = [:] {
        didSet{ selectedEditProduct.merge(defaultSelected, uniquingKeysWith: { $0 + $1 })}
    }
    var selectedEditProduct: [String: String] = [:] {
        didSet {
//            hasSelectedProducts.accept(selectedEditProduct.count != 0 )
            productCollectionView.reloadData()
        }
    }
    var textViewPlaceholder: String {
        return "Вкажіть точний час і адресу доставки".L
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
    }

    
    fileprivate func addToolbarToTextView(_ textview: UITextView) {
        let toolbar = UIToolbar ();
        toolbar.sizeToFit ()
        //кнопки для Готово и Отмена для тулбара
        let doneButton = UIBarButtonItem (title: "Готово", style: .plain,
                                          target: nil, action: nil)
        doneButton.rx.tap
            .subscribe { [unowned self] _ in
                self.view.endEditing(true)
        }.disposed(by: self.disposeBag)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        toolbar.setItems([spaceButton, doneButton], animated: false)
        textview.inputAccessoryView = toolbar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)),
                                               name: UIResponder.keyboardDidShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        showDatePicker(datePickTF)
        configureRx()
        collectionHeightConstraint.constant = ConstantProject.PurchasedPart.EditAmbarCollectionViewCell.height + 80
        configureTextView()
        if datePickTF.text?.isEmpty ?? true {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            datePickTF.text = formatter.string(from: Date())
        }
    }
    
    private func configureTextView() {
        commentTextView.clipsToBounds = true
        commentTextView.layer.cornerRadius = 10
        commentTextView.layer.borderColor = #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1)
        commentTextView.layer.borderWidth = 1
        commentTextView.contentInset = UIEdgeInsets(top: 4, left: 16, bottom: 4, right: 4)
        commentTextView.text = textViewPlaceholder
        addToolbarToTextView(commentTextView)
        commentTextView.delegate = self
    }
    
    private func configureRx() {
        abortButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                self?.selectedEditProduct = self?.defaultSelected ?? [:]
                self?.datePickTF.text = ""
            }).disposed(by: disposeBag)
        
        saveButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                guard let self = self, let order = self.orderInfo, self.checkValidation() else { return }
                
                self.viewModel.addToAmbar(id: order.ordId, date: self.datePickTF.text ?? "",
                                          comment: self.commentTextView.text,
                                          products: self.selectedEditProduct,
                                          currentAmbarId: self.selectedAmbarId)
            }).disposed(by: disposeBag)
        
        viewModel.onAddToAmbar
            .subscribe(onNext: { (result) in
                switch result {
                case .failure(let error):
                    self.showError(message: error.localizedDescription)
                case .success(_):
                    self.navigationController?.popViewController(animated: true)
                }
            }).disposed(by: disposeBag)
        
        
//
//        let emptyDateObservable = Observable<Bool>
//            .combineLatest( datePickTF.rx.text.orEmpty.map { $0 != ""},
//                            hasSelectedProducts,
//                            commentTextView.rx.text.orEmpty.map { $0
//                                .trimmingCharacters(in: .whitespacesAndNewlines) != "" && $0 != self.textViewPlaceholder })
//            { $0 && $1 && $2}
//
//        emptyDateObservable
//            .bind(to: saveButton.rx.isEnabled)
//            .disposed(by: disposeBag)
    }
    
    @objc
    private func keyboardWasShown (notification: NSNotification) {
            let info = notification.userInfo
            let keyboardSize = (info?[UIResponder.keyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
         if !commentTextView.isFirstResponder {
            scrollView.contentOffset = CGPoint(x: 0, y: keyboardSize.height)
        }
    }
    
    @objc
    private func keyboardWillBeHidden (notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
     }
    
    private func checkValidation() -> Bool {
        
        if selectedEditProduct.count > 0 && (commentTextView.text
            .trimmingCharacters(in: .whitespacesAndNewlines) != "" && commentTextView.text != self.textViewPlaceholder) {
            return true
        } else {
            showError(message: "Заповніть всі поля для збереження амбару".L)
            return false
        }
    }
    
    private func showDatePicker(_ textfield: UITextField) {
        
        let datepicker = UIDatePicker()
        datepicker.datePickerMode = .date
        datepicker.minimumDate = Date()
        //Добавить тубар над дата пикером
        let toolbar = UIToolbar ();
        toolbar.sizeToFit ()
        //кнопки для Готово и Отмена для тулбара
        let doneButton = UIBarButtonItem (title: "Готово", style: .plain,
                                          target: nil, action: nil)
        doneButton.rx.tap
            .subscribe { [unowned self] _ in
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                textfield.text = formatter.string(from: datepicker.date)
                self.view.endEditing(true)
            }.disposed(by: self.disposeBag)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Отмена", style: .plain,
                                           target: nil, action: nil)
        cancelButton.rx.tap
            .subscribe { [unowned self] _ in
                self.view.endEditing(true)
            }.disposed(by: self.disposeBag)
        
        toolbar.setItems([cancelButton,spaceButton, doneButton], animated: false)
        
        //Привязка пикера и тулбара к текстфилду
        textfield.inputView = datepicker
        textfield.inputAccessoryView = toolbar
    }
}

extension EditAmbarViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return orderInfo?.products.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EditAmbarCollectionViewCell.identifier, for: indexPath) as!  EditAmbarCollectionViewCell
        cell.configure(orderInfo!.products[indexPath.row], orderInfo!, selectedAmbarId,
                       Int(selectedEditProduct["\(orderInfo!.products[indexPath.row].orderProductId)"] ?? ""))
        cell.checkmarkButton.isSelected = selectedEditProduct["\(orderInfo!.products[indexPath.row].orderProductId)"] != nil
        cell.selectedProd = { [weak self] id, count in
            self?.selectedEditProduct["\(id)"] = "\(count)"
        }
        cell.deselectedProd = {[weak self] id in
            self?.selectedEditProduct.removeValue(forKey: "\(id)")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ConstantProject.PurchasedPart.EditAmbarCollectionViewCell.width,
                      height: ConstantProject.PurchasedPart.EditAmbarCollectionViewCell.height)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == textViewPlaceholder {
            textView.text = ""
        }
    }
}
