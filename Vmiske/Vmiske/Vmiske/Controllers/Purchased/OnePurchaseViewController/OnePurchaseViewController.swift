//
//  OnePurchaseViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class OnePurchaseViewController: BaseViewController {
    
    @IBOutlet weak var statiscticView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var inAmbarImageView: UIImageView!
    @IBOutlet weak var courierCommentLabel: UILabel!
    @IBOutlet weak var seeInAmbarButton: RoundedBorderedButton!
    @IBOutlet weak var togetherPriceLabel: UILabel!
    @IBOutlet weak var buyItAgainButton: RoundedButton!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var productTable: UITableView! {
        didSet {productTable.register(UINib(nibName: "OnePurchaseTableViewCell", bundle: nil), forCellReuseIdentifier: OnePurchaseTableViewCell.identifier) }
    }
    
    private let dateToFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()
    
    private let dateFromFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    var orderId: Int?
    private var orderProducts: [OrderInfo.Product] = [] {
        didSet {
            productTable.reloadData()
            productTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
    }
    private var orderInfo: OrderInfo? {
        didSet { configInnerData(order: orderInfo!)}
    }
    
    var commentViewConstraintMaxHeight: CGFloat = 100
    
    private var loading = 0 {
        didSet {
            if loading != 0 { self.showLoader() }
            else { self.removeLoader() }
        }
    }
    private let viewModel = OrderViewModel()
    private let disposeBag = DisposeBag()
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        statiscticView.addCornerAndShadow()
        courierCommentLabel.addCornerAndShadow()
        courierCommentLabel.layer.backgroundColor = UIColor.white.cgColor
        commentViewConstraintMaxHeight = courierCommentLabel.bounds.width * 67 / 180
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
        loading += 1
        viewModel.getOrderInfo(id: orderId ?? 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buyItAgainButton.isHidden = true
        statusLabel.text = ""
        dateLabel.text = ""
        inAmbarImageView.isHidden = true
        seeInAmbarButton.isHidden = true
        arrowImageView.isHidden = seeInAmbarButton.isHidden
        togetherPriceLabel.togetherPrice(0, with: nil)
        productTable.deleteSeparatorOnEmptyCell()
        configureRx()
    }
    
    private func configureRx() {
        
        viewModel.onOrderInfo
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(let info):
                    self?.orderProducts = info.products
                    self?.orderInfo = info
                }
            }).disposed(by: disposeBag)
        
        seeInAmbarButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                
                guard let orderInfo = self?.orderInfo else { return}
               
                let ambarVC = UIViewController.getFromMainStoryboard(id: "ambarVC") as! AmbarViewController
                ambarVC.orderInfo = orderInfo
                self?.navigationController?.pushViewController(ambarVC, animated: true)
            }).disposed(by: disposeBag)
    }
    
    
    private func configInnerData(order: OrderInfo) {
        
        togetherPriceLabel.togetherPrice(Int(Double(order.total) ?? 0.0), with: nil)
        if let dateFrom = dateFromFormatter.date(from: order.date) {
            dateLabel.text = dateToFormatter.string(from: dateFrom)
        }
        inAmbarImageView.image = UserDataManager.shared.Language == ConstantProject.Language.RU ?
        UIImage(named: "inambrRU") : UIImage(named: "in-ambar")
        inAmbarImageView.isHidden = order.Ambar == .no
        seeInAmbarButton.isHidden = order.Ambar == .no
        arrowImageView.isHidden = seeInAmbarButton.isHidden
        courierCommentLabel.text = "\(order.address)\n\(order.paytype)\n\(order.delivery)"
        courierCommentLabel.sizeToFit()
        setStatus(order.status)
        view.layoutIfNeeded()
    }
    
    private func setStatus(_ delivered: String) {
        statusLabel.text = delivered
        statusLabel.textColor = delivered == "Доставлено" ? #colorLiteral(red: 0.5557940602, green: 0.8436309695, blue: 0.1671362221, alpha: 1) : #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1)
    }
    
    override func restoreFromOffline() {
        loading += 1
        viewModel.getOrderInfo(id: orderId ?? 0)
    }
}

extension OnePurchaseViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OnePurchaseTableViewCell.identifier) as! OnePurchaseTableViewCell
        let prod = orderProducts[indexPath.row]
        cell.config(img: prod.Image, description: prod.name, quantity: prod.quantity,
                    price: Int(Double(prod.price) ?? 0.0))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ConstantProject.PurchasedPart.OnePurchaseTableViewCell.height
    }
}
