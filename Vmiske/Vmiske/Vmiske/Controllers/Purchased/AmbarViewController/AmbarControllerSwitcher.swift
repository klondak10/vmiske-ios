//
//  AmbarControllerSwitcher.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

struct AmbarControllerSwitcher {
    
    func getAmbarViewController(_ order: OrderInfo) -> UIViewController {
        if order.ambars.count != 0  {
            let ambarVC = UIViewController.getFromMainStoryboard(id: "ambarVC") as! AmbarViewController
            ambarVC.orderInfo = order
            return ambarVC
        } else {
            let vc = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
            vc.settings(image: #imageLiteral(resourceName: "clock-ambar"), message: "Немає інформації по датах доставки амбара", controllerTitle: "Дати доставки амбара", buttonTitle: "Додати дату") { [weak vc] in
                let editVC = UIViewController.getFromMainStoryboard(id: "editAmbarVC") as! EditAmbarViewController
                editVC.loadViewIfNeeded()
                editVC.orderInfo = order
                vc?.navigationController?.pushViewController(editVC, animated: true)
            }
            return vc
        }
    }
}
