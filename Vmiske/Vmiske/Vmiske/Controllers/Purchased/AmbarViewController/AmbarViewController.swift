//
//  AmbarViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AmbarViewController: BaseViewController {
    
    
    @IBOutlet weak var addDateButton: RoundedButton!
    @IBOutlet weak var ambarCollectionView: UICollectionView! {
        didSet { ambarCollectionView.register(UINib(nibName: "AmbarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: AmbarCollectionViewCell.identifier)}
    }
    weak var emptyViewController: UIViewController?
    
    private var isLoaded = false
    var orderInfo: OrderInfo? {
        didSet {
            loadViewIfNeeded()
            if orderInfo?.ambars.count == 0 {
                self.setEmptyController()
            } else {
                removeEmptyController()
                let info = orderInfo
                
                addDateButton.isHidden = info?.products
                    .map { $0.quantity == $0.inAmarId }
                    .reduce(true, {$0 && $1}) ?? false
            }
            ambarCollectionView.reloadData()
        }
    }
    private var loading = 0 {
        didSet {
            if loading != 0 { self.showLoader() }
            else { self.removeLoader() }
        }
    }
    private let viewModel = OrderViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
        if isLoaded {
            loading += 1
            viewModel.getOrderInfo(id: orderInfo?.ordId ?? 0)
            isLoaded = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (ambarCollectionView.collectionViewLayout as! UICollectionViewFlowLayout)
            .estimatedItemSize = CGSize(width: 1, height: 1)
        viewModel.onOrderInfo
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (result) in
                       self?.loading -= 1
                       switch result {
                       case .failure(let error):
                           self?.showError(message: error.localizedDescription)
                       case .success(let info):
                           self?.orderInfo = info
                       }
            }).disposed(by: disposeBag)
               
    }

    @IBAction func addDateAction(_ sender: Any) {
        let editVC = UIViewController.getFromMainStoryboard(id: "editAmbarVC") as! EditAmbarViewController
        self.isLoaded = true
        editVC.loadViewIfNeeded()
        editVC.orderInfo = self.orderInfo
        self.removeEmptyController()
        self.navigationController?.pushViewController(editVC, animated: true)
    }
    
    private func setEmptyController() {
        let vc = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
        vc.settings(image: #imageLiteral(resourceName: "clock-ambar"), message: "Немає інформації по датах доставки амбара".L, controllerTitle: "Дати доставки амбара", buttonTitle: "Додати дату".L) { [weak self] in
            let editVC = UIViewController.getFromMainStoryboard(id: "editAmbarVC") as! EditAmbarViewController
            editVC.loadViewIfNeeded()
            editVC.orderInfo = self?.orderInfo
            self?.isLoaded = true
            self?.removeEmptyController()
            self?.navigationController?.pushViewController(editVC, animated: true)
        }
        self.emptyViewController = vc
        self.addChild(vc)
        vc.view.frame = view.bounds
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    private func removeEmptyController() {
        emptyViewController?.willMove(toParent: nil)
        emptyViewController?.view.removeFromSuperview()
        emptyViewController?.removeFromParent()
    }
    
    override func restoreFromOffline() {
        loading += 1
        viewModel.getOrderInfo(id: orderInfo?.ordId ?? 0)
    }
}

extension AmbarViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return orderInfo!.ambars.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AmbarCollectionViewCell.identifier, for: indexPath) as! AmbarCollectionViewCell
        
        cell.configure(orderInfo!.ambars[indexPath.row])
        cell.action = {
            collectionView.performBatchUpdates(nil,completion: nil)
            cell.layoutIfNeeded()
        }
        cell.editAction = { [weak self] in
            let editVC = UIViewController.getFromMainStoryboard(id: "editAmbarVC") as! EditAmbarViewController
            editVC.selectedAmbarId = self?.orderInfo?.ambars[indexPath.row].info.ambarId
            editVC.loadViewIfNeeded()
            editVC.orderInfo = self?.orderInfo
            self?.navigationController?.pushViewController(editVC, animated: true)
            self?.isLoaded = true
            self?.removeEmptyController()
        }
        return cell
    }
}
