//
//  PurchasedViewController.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PurchasedViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet { collectionView.register(UINib(nibName: "PurchaseCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: PurchaseCollectionViewCell.identifier)}
    }
    
    private var orderList: [Order] = [] {
        didSet{
            collectionView.reloadData()
        }
    }
    private var loading = 0 {
        didSet {
            if loading != 0 { self.showLoader() }
            else { self.removeLoader() }
        }
    }
    
    private let viewModel = OrderViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem = nil
        loading += 1
        viewModel.getOrderList()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureRx()
    }
    
    private func configureRx() {
        
        viewModel.onOrderList
            .subscribe(onNext: { [weak self] (result) in
                self?.loading -= 1
                switch result {
                case .failure(let error):
                    self?.showError(message: error.localizedDescription)
                case .success(let list):
                    self?.orderList = list.sorted(by: {$0.ordId > $1.ordId })
                    if self?.orderList.count == 0 {
                        self?.setEmptyController()
                    }
                }
            }).disposed(by: disposeBag)
    }
    
    private func setEmptyController() {
        let vc = UIViewController.getFromMainStoryboard(id: "messageVC") as! MessageViewController
        vc.settings(image: #imageLiteral(resourceName: "empty-basket "),
                    message: "Тут нічого немає".L, controllerTitle: "Покупки".L,
                    buttonTitle: "Перейти до каталог".L) { [weak vc] in
                        vc?.sideMenuController?.setContentViewController(to:  BaseNavigationControllerViewController(rootViewController: UIViewController.getFromMainStoryboard(id: "categoriesVC")))
        }
        
        self.addChild(vc)
        vc.view.frame = view.bounds
        self.view.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
    
    override func restoreFromOffline() {
        loading += 1
        viewModel.getOrderList()
    }
}

extension PurchasedViewController: UICollectionViewDelegate, UICollectionViewDataSource,
                                                    UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return orderList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PurchaseCollectionViewCell.identifier, for: indexPath) as! PurchaseCollectionViewCell
        let ord = orderList[indexPath.row]
        cell.config(numberPurchased: ord.ordId, date: ord.date, price: ord.total, status: ord.status ?? "", inAmbar: ord.Ambar)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ConstantProject.PurchasedPart.PurchasedCollectionViewCell.width,
                      height: ConstantProject.PurchasedPart.PurchasedCollectionViewCell.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! PurchaseCollectionViewCell
        
        let onePurchaseVC = UIViewController.getFromMainStoryboard(id: "onePurchaseVC") as! OnePurchaseViewController
        onePurchaseVC.orderId = orderList[indexPath.row].ordId
        onePurchaseVC.title = "Замовлення ".L + cell.purchasedNumberLabel.text!
        navigationController?.pushViewController(onePurchaseVC, animated: true)
    }
}
