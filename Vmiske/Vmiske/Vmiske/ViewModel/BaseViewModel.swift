//
//  BaseViewModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import Moya
import RxSwift


enum ResponseError: Error {
    
    case messagableError(message: String)
    case nonMessageble
    
    var localizedDescription: String {
        switch self {
        case .messagableError(let message):
            return message
        case .nonMessageble:
            return "Unexpected error (ResponseError)"
        }
    }
}


protocol BaseViewModel {
    
}

extension  BaseViewModel{
    
    func getObject<T: Codable>(_ type: T.Type, from response: SingleEvent<Response>) -> Result<T, ResponseError> {
        
        switch response {
        case .success(let result):
            
            do {
                let user = try JSONDecoder().decode(BaseResponse<T>.self, from: result.data)
                if user.error {
                    return .failure(.messagableError(message: user.message ?? "Unexpected Response Error (Method: baseResponseHandler)"))
                } else {
                    return .success(user.answer)
                }
            } catch (let error) {
                print(error)
                return .failure(.messagableError(message: "Сталася помилка 🌚\n".L + String(describing: type)))
            }
            
        case .error(let error):
            return .failure(.messagableError(message: error.localizedDescription))
        }
    }
}
