//
//  OrderViewModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 09.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa

struct Empty: Codable {
}

class OrderViewModel: BaseViewModel {
    
    typealias Response<T> = PublishSubject<Result<T, ResponseError>>
    typealias _Response<T> = Observable<Result<T, ResponseError>>
    
    //MARK: Fields
    
    private let provider: MoyaProvider<OrderRequests>
    private let disposeBag: DisposeBag
    
    private let orderList = Response<[Order]>()
    private let regionList = Response<[Region]>()
    private let cityList = Response<[City]>()
    private let newPostList = Response<[NewPost]>()
    private let pickUp = Response<String>()
    private let createOrder = Response<CreateOrderResponse>()
    private let delivery = Response<[DeliveryPaymentType]>()
    private let payment = Response<[DeliveryPaymentType]>()
    private let confirmOrder = Response<Empty>()
    private let addToAmbar = Response<Bool>()
    private let orderInfo = Response<OrderInfo>()
    private let fastOrder = Response<Bool>()
    
    
    //MARK: Properties
    
    var onOrderList: _Response<[Order]> {
        return orderList.asObservable()
    }
    var onRegionList: _Response<[Region]> {
        return regionList.asObservable()
    }
    var onCityList: _Response<[City]> {
        return cityList.asObservable()
    }
    var onNewPostList: _Response<[NewPost]> {
        return newPostList.asObservable()
    }
    var onPickUp: _Response<String> {
        return pickUp.asObservable()
    }
    var onCreateOrder: _Response<CreateOrderResponse> {
        return createOrder.asObservable()
    }
    var onDelivery: _Response<[DeliveryPaymentType]> {
        return delivery.asObservable()
    }
    var onPayment: _Response<[DeliveryPaymentType]> {
        return payment.asObservable()
    }
    var onConfirmOrder: _Response<Empty> {
        return confirmOrder.asObservable()
    }
    var onAddToAmbar: _Response<Bool> {
        return addToAmbar.asObservable()
    }
    var onOrderInfo: _Response<OrderInfo> {
        return orderInfo.asObservable()
    }
    var onFastOrder: _Response<Bool> {
        return fastOrder.asObservable()
    }
    
    init() {
        provider = MoyaProvider<OrderRequests>(plugins: [NetworkLoggerPlugin(verbose: true)])
        disposeBag = DisposeBag()
    }
    
    //MARK: Methods
    
    func getOrderList() {
        provider.rx.request(.list)
            .subscribe {[unowned self] (response) in
                switch self.getObject([Order].self, from: response) {
                case .failure(let error):
                    self.orderList.onNext(.failure(error))
                case .success(let list):
                    self.orderList.onNext(.success(list
                        .filter { !($0.order_status == 1 && $0.payment_code == "liqpay") }))
                }
        }.disposed(by: disposeBag)
    }
    
    func getRegiosList() {
        provider.rx.request(.region)
            .subscribe { [unowned self] (response) in
                switch self.getObject([Region].self, from: response) {
                case .failure(let error):
                    self.regionList.onNext(.failure(error))
                case .success(let list):
                    self.regionList.onNext(.success(list))
                }
        }.disposed(by: disposeBag)
    }
    
    func getCityList(key: String) {
        provider.rx.request(.city(key: key))
            .subscribe { [unowned self] (response) in
                switch self.getObject([City].self, from: response) {
                case .failure(let error):
                    self.cityList.onNext(.failure(error))
                case .success(let list):
                    self.cityList.onNext(.success(list))
                }
        }.disposed(by: disposeBag)
    }
    
    func getNewPostDepartment(key: String) {
        provider.rx.request(.newPost(key: key))
            .subscribe { [unowned self] (response) in
                switch self.getObject([NewPost].self, from: response) {
                case .failure(let error):
                    self.newPostList.onNext(.failure(error))
                case .success(let list):
                    self.newPostList.onNext(.success(list))
                }
        }.disposed(by: disposeBag)
    }
    
    func getDeliveryType() {
        provider.rx.request(.delivery)
            .subscribe { [unowned self] (response) in
                switch self.getObject([DeliveryPaymentType].self, from: response) {
                case .failure(let error):
                    self.delivery.onNext(.failure(error))
                case .success(let list):
                    self.delivery.onNext(.success(list))
                }
        }.disposed(by: disposeBag)
    }
    
    func getPaymentType() {
        provider.rx.request(.payment)
            .subscribe { [unowned self] (response) in
                switch self.getObject([DeliveryPaymentType].self, from: response) {
                case .failure(let error):
                    self.payment.onNext(.failure(error))
                case .success(let list):
                    self.payment.onNext(.success(list))
                }
        }.disposed(by: disposeBag)
    }
    
    func getPickUpAddress() {
        provider.rx.request(.pickUp)
            .subscribe { [unowned self] (response) in
                switch self.getObject([String: String].self, from: response) {
                case .failure(let error):
                    self.pickUp.onNext(.failure(error))
                case .success(let dict):
                    self.pickUp.onNext(.success(dict["address"] ?? "Nothing"))
                }
        }.disposed(by: disposeBag)
    }
    
    func createOrder(ambar: Ambar, address: String, cityId: String, regionId: String,
                     comment: String, paymant: String, shippingCode: String, promocode: String?,
                     bonuses: Int?) {
        provider.rx.request(.createOrder(ambar: ambar, address: address, cityId: cityId,
                                         regionId: regionId, comment: comment,
                                         paymant: paymant, shippingCode: shippingCode,
                                         promocode: promocode, bonuses: bonuses))
            .subscribe { [unowned self] (response) in
                switch self.getObject(CreateOrderResponse?.self, from: response) {
                case .failure(let error):
                    self.createOrder.onNext(.failure(error))
                case .success(let res):
                    guard let res = res else { return}
                    self.createOrder.onNext(.success(res))
                }
        }.disposed(by: disposeBag)
    }
    
    func addToAmbar(id: Int,date: String, comment: String, products: [String: String],
                    currentAmbarId: Int?) {
        let productsString = products.map { "{\"\($0.key)\": \($0.value)}" }.joined(separator: ",")
        provider.rx.request(.addToAmbar(id: id, date: date, comment: comment,
                                        products: productsString, ambarId: currentAmbarId))
            .subscribe { [unowned self] (response) in
                switch self.getObject(Bool?.self, from: response) {
                case .failure(let error):
                    self.addToAmbar.onNext(.failure(error))
                case .success(let res):
                    self.addToAmbar.onNext(.success(res ?? false))
                }
        }.disposed(by: disposeBag)
    }
    
    func getOrderInfo(id: Int) {
        provider.rx.request(.orderInfo(id: id))
            .subscribe { [unowned self] (response) in
                switch self.getObject([OrderInfo].self, from: response) {
                case .failure(let error):
                    self.orderInfo.onNext(.failure(error))
                case .success(let info):
                    self.orderInfo.onNext(.success(info[0]))
                }
        }.disposed(by: disposeBag)
    }
    
    func confirmOrderPayment(id: Int) {
        provider.rx.request(.confirm(id: id))
            .subscribe { [unowned self] (response) in
                switch self.getObject(Empty.self, from: response) {
                case .failure(let error):
                    self.confirmOrder.onNext(.failure(error))
                case .success(let empty):
                    self.confirmOrder.onNext(.success(empty))
                }
        }.disposed(by: disposeBag)
    }
    
    func createFastOrder(phone: String, prodId: Int, optName: String = "", optDesc: String = "", optPrice: Int = 0, povId: Int? = nil, poId: Int? = nil) {
        provider.rx.request(.fastOrder(phone: phone, prodId: prodId, optName: optName,
                                       optDesc: optDesc, optPrice: optPrice, poId: poId, povId: povId))
            .subscribe { [unowned self] (response) in
                switch self.getObject(FastOrderResponse.self, from: response) {
                case .failure(let error):
                    self.fastOrder.onNext(.failure(error))
                case .success(let res):
                    self.fastOrder.onNext(.success(res.status))
                }
        }.disposed(by: disposeBag)
    }
    
    struct FastOrderResponse: Codable {
        let status: Bool
    }
}
