//
//  UserViewModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 25.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa


class UserViewModel: BaseViewModel {
    
    private struct TokenResponse: Codable {
           let token: String
    }
  
    //MARK: Fields
    
    private let provider: MoyaProvider<UserRequests>
    private let disposeBag: DisposeBag
    
    private let editedProfile = PublishSubject<Result<Bool, ResponseError>>()
    private let passwordChanged = PublishSubject<Result<Bool, ResponseError>>()
    
    
    //MARK: Properties
    
    var onEditedProfile: Observable<Result<Bool, ResponseError>> {
        return editedProfile.asObservable()
    }
    
    var onPasswordChanged: Observable<Result<Bool, ResponseError>> {
        return passwordChanged.asObservable()
    }
    
    init () {
        provider = MoyaProvider<UserRequests>(plugins: [NetworkLoggerPlugin(verbose: true)])
        disposeBag = DisposeBag()
    }
    
    //MARK: Methods
    
    func editProfile(firstName: String, lastName: String, phone: String, email: String) {
        
        provider.rx.request(.updateUserData(firstName: firstName, lastName: lastName,
                                            phone: phone, email: email))
            .subscribe { [unowned self] (response) in
                
                switch self.getObject(Bool?.self, from: response) {
                case .failure(let error):
                    self.editedProfile.onNext(.failure(error))
                case .success(_):
                    self.editedProfile.onNext(.success(true))
                }
        }.disposed(by: disposeBag)
    }
    
    func changePassword(oldPassword: String, newPassword: String) {
        
        provider.rx.request(.changePassword(oldPassword: oldPassword,
                                            newPassword: newPassword))
            .subscribe { (response) in
                
                switch self.getObject(TokenResponse?.self, from: response) {
                case .failure(let error):
                    self.passwordChanged.onNext(.failure(error))
                case .success(_):
                    self.passwordChanged.onNext(.success(true))
                }
        }.disposed(by: disposeBag)
    }
    
}
