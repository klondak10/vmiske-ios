//
//  MainPageViewModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa


///Класс для получения категорий, подкатегорий, банера, деталки продукта и работы с отзывами
class MainPageViewModel: BaseViewModel {
    
    //MARK: Fields
    private let provider: MoyaProvider<MainPageRequests>
    private let disposeBag: DisposeBag
    
    private let bannerList = PublishSubject<Result<[Banner], ResponseError>>()
    private let categories = PublishSubject<Result<[Category], ResponseError>>()
    private let productList = PublishSubject<Result<CategoryProductList, ResponseError>>()
    private let productDetail = PublishSubject<Result<Product, ResponseError>>()
    private let searchedProducts = PublishSubject<Result<CategoryProductList, ResponseError>>()
    private let addedReview = PublishSubject<Result<[Review], ResponseError>>()
    
    private var cancelationBag = DisposeBag()
    
    //MARK: Properties
    var onBannerList: Observable<Result<[Banner], ResponseError>> {
        return bannerList.asObservable()
    }
    
    var onCategories: Observable<Result<[Category], ResponseError>> {
        return categories.asObservable()
    }
    
    var onProductList: Observable<Result<CategoryProductList, ResponseError>> {
        return productList.asObservable()
    }
    
    var onProductDetail: Observable<Result<Product, ResponseError>> {
        return productDetail.asObservable()
    }
    
    var onSearchedProducts: Observable<Result<CategoryProductList, ResponseError>> {
        return searchedProducts.asObservable()
    }
    
    var onAddedReview: Observable<Result<[Review], ResponseError>> {
        return addedReview.asObservable()
    }
    
    init () {
        provider = MoyaProvider<MainPageRequests>.init(plugins: [NetworkLoggerPlugin(verbose: true)])
        disposeBag = DisposeBag()
    }
    
    
    //MARK: Methods
    func getBanner() {
        provider.rx.request(.getBanners)
            .retry(3)
            .subscribe { [unowned self] (response) in
                
                switch self.getObject([Banner].self, from: response) {
                case .failure(let error):
                    self.bannerList.onNext(.failure(error))

                case .success(let banners):
                    self.bannerList.onNext(.success(banners))
                }
        }.disposed(by: disposeBag)
    }
    
    func getMainCategories() {
        provider.rx.request(.getMainCategories)
            .retry(3)
            .subscribe { [unowned self] (response) in
                
                switch self.getObject([Category].self, from: response) {
                case .failure(let error):
                    self.categories.onNext(.failure(error))
                case .success(let categories):
                    self.categories.onNext(.success(categories))
                }
        }.disposed(by: disposeBag)
    }
    
    func getSubcategories(for id: Int) {
        provider.rx.request(.getSubCategories(id: id))
            .retry(3)
            .subscribe { [unowned self] (response) in
                
                switch self.getObject([Category].self, from: response) {
                case .failure(let error):
                    self.categories.onNext(.failure(error))
                case .success(let categories):
                    self.categories.onNext(.success(categories))
                }
        }.disposed(by: disposeBag)
    }
    
    func getProductList(id category: Int, pagination: PaginationModel, sort: SortingState?, filters: [Filters.FilterItem]) {
        provider.rx.request(.getProductList(id: category, page: pagination,
                                            sort: sort, filters: filters))
            .retry(3)
            .subscribe { [unowned self] (response) in
                
                switch self.getObject(CategoryProductList?.self, from: response) {
                case .failure(let error):
                    self.productList.onNext(.failure(error))
                case .success(let productList):
                    if productList != nil {
                        self.productList.onNext(.success(productList!))
                    }
                }
        }.disposed(by: disposeBag)
    }
    
    func getProductDetail(for id: Int) {
        provider.rx.request(.getProductDetail(id: id))
            .retry(3)
            .subscribe { [unowned self] (response) in
                
                switch self.getObject(Product.self, from: response) {
                case .failure(let error):
                    self.productDetail.onNext(.failure(error))
                case .success(let product):
                    self.productDetail.onNext(.success(product))
                }
        }.disposed(by: disposeBag)
    }
    
    func searchProducts(_ text: String, _ page: Int) {
        cancelationBag = DisposeBag()
        provider.rx.request(.search(text: text, page: page))
            .subscribeOn(MainScheduler.instance)
            .retry(3)
            .subscribe { [unowned self] (response) in
                
                switch self.getObject(CategoryProductList.self, from: response) {
                case .failure(let error):
                    self.searchedProducts.onNext(.failure(error))
                case .success(let productList):
                    self.searchedProducts.onNext(.success(productList))
                }
        }.disposed(by: cancelationBag)
    }
    
    func createReview(on id: Int, author: String, email: String, text: String, rate: Int) {
        provider.rx.request(.createReview(id: id, author: author,
            email: email, text: text, rate: rate))
            .retry(3)
            .subscribe { [weak self] (response) in
                
                switch self?.getObject([Review].self, from: response) {
                case .failure(let error):
                    self?.addedReview.onNext(.failure(error))
                case .success(let review):
                    self?.addedReview.onNext(.success(review))
                default:
                    break
                }
        }.disposed(by: disposeBag)
    }
}
