//
//  BasketViewModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 23.11.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa


class BasketViewModel: BaseViewModel {

      //MARK: Fields
    
    private let provider: MoyaProvider<BasketRequests>
    private let disposeBag: DisposeBag
    
    private let addedProduct = PublishSubject<Result<Bool, ResponseError>>()
    private let updatedProduct = PublishSubject<Result<Bool, ResponseError>>()
    private let deletedItem = PublishSubject<Result<Bool, ResponseError>>()
    private let basketList = PublishSubject<Result<[BasketProduct], ResponseError>>()
    private let promocodeInfo = PublishSubject<Result<Promocode, ResponseError>>()
    
    //MARK: Properties
    
    var onAddedProduct: Observable<Result<Bool, ResponseError>> {
           return addedProduct.asObservable()
    }
    var onUpdatedProduct: Observable<Result<Bool, ResponseError>> {
           return updatedProduct.asObservable()
    }
    var onDeletedItem: Observable<Result<Bool, ResponseError>> {
           return deletedItem.asObservable()
    }
    var onBasketList: Observable<Result<[BasketProduct], ResponseError>> {
           return basketList.asObservable()
    }
    var onPromocodeInfo: Observable<Result<Promocode, ResponseError>> {
           return promocodeInfo.asObservable()
    }
    
    init() {
        provider = MoyaProvider<BasketRequests>(plugins: [NetworkLoggerPlugin(verbose: true)])
        disposeBag = DisposeBag()
    }
    
    //MARK: Methods
    
    func addProduct(productId: Int, quantity: Int, povId: Int?, poId: Int?,
                    subName: String?, subDescription: String?, subPrice: Int?) {
        
        provider.rx.request(.add(productId: productId, quantity: quantity,
                                 povId: povId, poId: poId, subName: subName,
                                 subDescription: subDescription, subPrice: subPrice))
            .subscribe { [unowned self] (response) in
                
                switch self.getObject(Bool?.self, from: response) {
                case .failure(let error):
                    self.addedProduct.onNext(.failure(error))
                case .success(_):
                    self.addedProduct.onNext(.success(true))
                    //Add to userManager product if authorized
                }
        }.disposed(by: disposeBag)
    }
    
    func updateInBasket(basketId: Int, quantity: Int) {
        
        provider.rx.request(.update(basketId: basketId, quantity: quantity))
            .subscribe { [unowned self] (response) in
                
                switch self.getObject(Bool?.self, from: response) {
                case .failure(let error):
                    self.updatedProduct.onNext(.failure(error))
                case .success(_):
                    self.updatedProduct.onNext(.success(true))
                    //Add to userManager product if authorized
                }
        }.disposed(by: disposeBag)
    }
    
    func deleteFromBasket(basketId: Int) {
        
        provider.rx.request(.delete(basketId: basketId))
            .subscribe { [unowned self] (response) in
                
                switch self.getObject(Bool?.self, from: response) {
                case .failure(let error):
                    self.deletedItem.onNext(.failure(error))
                case .success(_):
                    self.deletedItem.onNext(.success(true))
                }
        }.disposed(by: disposeBag)
    }
    
    func getBasketList() {
        
        provider.rx.request(.basketList)
            .subscribe { [unowned self] (response) in
                
                switch self.getObject([BasketProduct].self, from: response) {
                case .failure(let error):
                    self.basketList.onNext(.failure(error))
                case .success(let list):
                    self.basketList.onNext(.success(list))
                    UserDataManager.shared.ProductInBasket = list
                }
        }.disposed(by: disposeBag)
    }
    
    func checkPromocode(_ promo: String) {
        
        provider.rx.request(.promocode(promo: promo))
                   .subscribe { [unowned self] (response) in
                       
                       switch self.getObject(Promocode?.self, from: response) {
                       case .failure(let error):
                        self.promocodeInfo.onNext(.failure(error))
                       case .success(let res):
                        if let info = res {
                            self.promocodeInfo.onNext(.success(info))
                        } else { self.promocodeInfo.onNext(
                            .failure(.messagableError(message: "Something wrong 😱")))}
                       }
               }.disposed(by: disposeBag)
    }
}
