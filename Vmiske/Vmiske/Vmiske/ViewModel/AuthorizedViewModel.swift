//
//  AuthorizedViewModel.swift
//  Vmiske
//
//  Created by Roman Haiduk on 24.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya
import RxSwift
import RxCocoa


///ViewModel for signUp and signIn
class AuthorizedViewModel: BaseViewModel {
    
    private struct SignResponse: Codable {
        let token: String?
        let loc: Int?
    }
    
    private struct SocialSignResponse: Codable {
        let token: String?
    }
    
    //MARK: Fields
    
    private let provider: MoyaProvider<UserRequests>
    private let disposeBag: DisposeBag
    
    private let signInResult = PublishSubject<Result<Bool, ResponseError>>()
    private let signUpResult = PublishSubject<Result<Bool, ResponseError>>()
    private let userDataReceived = PublishSubject<Result<Bool, ResponseError>>()
    private let socialSignIn = PublishSubject<Result<Bool, ResponseError>>()
    private let forgottenPassword = PublishSubject<Result<String, ResponseError>>()
    
    //MARK: Properties
    
    
    
    var onSignUpResult: Observable<Result<Bool, ResponseError>> {
        return signUpResult.asObservable()
    }
    
    var onSignInResult: Observable<Result<Bool, ResponseError>> {
        return signInResult.asObservable()
    }
    
    var onUserDataRecieved: Observable<Result<Bool, ResponseError>> {
        return userDataReceived.asObservable()
    }
    
    var onSocialSignIn: Observable<Result<Bool, ResponseError>> {
        return socialSignIn.asObservable()
    }
    
    var onForgottenPassword: Observable<Result<String, ResponseError>> {
        return forgottenPassword.asObservable()
    }
    
    init () {
        provider = MoyaProvider<UserRequests>(plugins: [NetworkLoggerPlugin(verbose: true)])
        disposeBag = DisposeBag()
    }
    
    //MARK: Methods
    
    func signIn(email: String, password: String) {
        
        provider.rx.request(.signIn( email: email, password: password))
        .retry(3)
        .subscribe { [unowned self] (response) in
            
            switch self.getObject(SignResponse?.self, from: response) {
            case .failure(let error):
                self.signInResult.onNext(.failure(error))
            case .success(let success):
                UserDataManager.shared.Token = success!.token
                self.signInResult.onNext(.success(true))
            }
        }.disposed(by: disposeBag)
    }
    
    func signUp(firstName: String, lastName: String, email: String,
                phone: String, password: String, leaderPhone: String?,
                isGoogle: Bool?, sID: String?) {
        
        provider.rx.request(.signUp(firstName: firstName, lastName: lastName,
                                    phone: phone, email: email, password: password,
                                    leaderPhone: leaderPhone, isGoogle: isGoogle, sID: sID))
        .retry(3)
        .subscribe { [unowned self] (response) in
            
            switch self.getObject(SignResponse?.self, from: response) {
            case .failure(let error):
                self.signUpResult.onNext(.failure(error))
            case .success(let success):
                UserDataManager.shared.Token = success!.token
                self.signUpResult.onNext(.success(true))
            }
        }.disposed(by: disposeBag)
    }
    
    func socialSignIn(isGoogle: Bool, id: String, email: String?) {
        
        provider.rx.request(.socialSignIn(isGoogle: isGoogle, id: id, email: email))
        .retry(3)
        .subscribe { [unowned self] (response) in
            
            switch self.getObject(SocialSignResponse.self, from: response) {
            case .failure(let error):
                self.signUpResult.onNext(.failure(error))
            case .success(let success):
                if let token = success.token {
                    UserDataManager.shared.Token =  token
                    self.socialSignIn.onNext(.success(true))
                } else {
                    self.socialSignIn.onNext(.success(false))
                }
            }
        }.disposed(by: disposeBag)
    }
    
    func forgottenPassword(_ email: String) {
        
        provider.rx.request(.forgottenPassword(email: email))
            .retry(3)
            .subscribe { [unowned self] (response) in
                
                switch response {
                    
                case .success(let result):
                    
                    struct TempResponse: Codable {
                        let success: TempTitle?
                        let error: TempErrorEmail?
                        
                        struct TempTitle: Codable {
                            let title: String
                        }
                        struct TempErrorEmail: Codable {
                            let email: String
                        }
                    }
                    
                    do {
                        if let res = try JSONDecoder()
                            .decode(TempResponse?.self, from: result.data) {
                            if let title = res.success?.title {
                                self.forgottenPassword.onNext(.success(title))
                            } else if let _ = res.error?.email {
                                self.forgottenPassword.onNext(.failure(.messagableError(message: "Користувача з таким email не існує".L)))
                            }
                            
                        } else {
                            self.forgottenPassword.onNext(.failure(.messagableError(message:
                                "Помилка получення даних".L)))
                        }
                        
                    } catch(let error) {
                        self.forgottenPassword.onNext(.failure(.messagableError(message:
                            error.localizedDescription)))
                    }
                    
                case .error(let error):
                    self.forgottenPassword.onNext(.failure(.messagableError(message:
                        error.localizedDescription)))
                }
        }.disposed(by: disposeBag)
    }
    
    func getUserData() {
        guard let _ = UserDataManager.shared.Token  else {
            let error: ResponseError = .messagableError(message: "Token is Empty!")
            userDataReceived.onNext(.failure(error))
            return
        }
        
        provider.rx.request(.getUserData)
            .subscribe { [unowned self] (response) in
                
                switch self.getObject(User?.self, from: response) {
                case .failure(let error):
                    self.userDataReceived.onNext(.failure(error))
                case .success(let success):
                    if let user = success {
                        UserDataManager.shared.regUser(user)
                    } else {
                         let error: ResponseError = .messagableError(message: "User didn't recieve")
                        self.userDataReceived.onNext(.failure(error))
                    }
                    self.userDataReceived.onNext(.success(true))
                }
        }.disposed(by: disposeBag)
    }
}
