//
//  BasketRequests.swift
//  Vmiske
//
//  Created by Roman Haiduk on 23.11.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya

enum BasketRequests {
    
    case add(productId: Int, quantity: Int, povId: Int?, poId: Int?, subName: String?,
        subDescription: String?, subPrice: Int?)
    
    case update(basketId: Int, quantity: Int)
    
    case delete(basketId: Int)
    
    case basketList
    
    case promocode(promo: String)
}


extension BasketRequests: TargetType {
    
    var baseURL: URL {
        return URL(string: ConstantProject.Network.BASE_URL)!
    }
    
    var method: Moya.Method {
        switch self {
        case .basketList:
            return .get
        default:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .add:
            return "basket/add"
        case .update:
            return "basket/update"
        case .delete:
            return "basket/delete"
        case .basketList:
            return "basket"
        case .promocode:
            return "order/coupon"
        }
    }
    
    var params: [String: Any] {
        switch self {
            
        case .add(let productId, let quantity, let povId, let poId,
                  let subName, let subDescription, let subPrice):
            var param: [String: Any] = [:]
            param["product_id"] = productId
            param["quantity"] = quantity
            if povId != nil { param["povId"] = povId! }
            if poId != nil { param["poId"] = poId! }
            if subDescription != nil { param["option_desc"] = subName! }
            if subName != nil { param["option_value"] = subName! }
            if subPrice != nil { param["option_price"] = subPrice! }
            return param
            
        case .delete(let basketId):
            return ["baskId": basketId]
        case .update(let basketId, let quantity):
            return ["baskId": basketId, "quantity": quantity]
        case .basketList:
            return [:]
        case .promocode(let promo):
            return ["vaucher": promo]
        }
    }
    
    var task: Task {
        switch self {
        case .basketList:
            return .requestPlain
        default:
            return .requestParameters(parameters: params, encoding: JSONEncoding())
        }
    }
    
    var headers: [String : String]? {
        if let token = UserDataManager.shared.Token {
            return ["Authorization":  token,
            "X-Locale": UserDataManager.shared.ServerLanguage,]
        }
        else { return ["X-Locale": UserDataManager.shared.ServerLanguage] }
    }
    
    var sampleData: Data {
        return Data()
    }
}
