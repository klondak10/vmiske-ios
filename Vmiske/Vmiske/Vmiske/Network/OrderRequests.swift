//
//  OrderRequests.swift
//  Vmiske
//
//  Created by Roman Haiduk on 08.12.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Moya

enum Ambar: Int {
    case no = 0
    case yes = 1
}

enum OrderRequests {
    
    case list
    case region
    case city(key: String)
    case newPost(key: String)
    case pickUp
    case createOrder(ambar: Ambar, address: String, cityId: String, regionId: String, comment: String, paymant: String, shippingCode: String, promocode: String?, bonuses: Int?)
    case delivery
    case payment
    case addToAmbar(id: Int, date: String, comment: String, products: String, ambarId: Int?)
    case orderInfo(id: Int)
    case confirm(id: Int)
    case fastOrder(phone: String, prodId: Int, optName: String = "", optDesc: String = "", optPrice: Int = 0, poId: Int? = nil, povId: Int? = nil)
}

extension OrderRequests: TargetType {
    var baseURL: URL {
        return URL(string: ConstantProject.Network.BASE_URL)!
    }
    
    var path: String {
        switch self {
        case .list:
            return "order"
        case .delivery:
            return "order/delivery"
        case .payment:
            return "order/paymethod"
        case .pickUp:
            return "order/pickup"
        case .region:
            return "order/zone"
        case .city:
            return "order/city"
        case .newPost:
            return "order/novaposhta"
        case .createOrder:
            return "order/make"
        case .addToAmbar(let id,_,_,_,_):
            return "order/ambar/\(id)"
        case .orderInfo(let id):
            return "order/info/\(id)"
        case .confirm(let id):
            return "order/confirm/\(id)"
        case .fastOrder:
            return "order/fastmake"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .createOrder, .addToAmbar, .fastOrder:
            return .post
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var param: [String: Any] {
        switch self {
        case .createOrder(let ambar, let address, let cityId, let regionId, let comment,
                          let paymant, let shippingCode, let promocode, let bonuses):
            var par: [String: Any] = ["ambar": ambar.rawValue, "address": address, "city_id" : cityId, "zone_id": regionId,
                    "comment": comment, "payment_code": paymant, "shipping_code": shippingCode,
                    "vaucher": promocode as Any]
            if let bon = bonuses {
                par["bonuse"] = "\(bon)"
            }
            return par
        case .addToAmbar(_, let date, let comment, let product, let ambarId):
            var par: [String: Any] = ["date": date, "products": product, "comment": comment]
            if let ambId = ambarId {
                par["ambId"] = ambId
            }
            return par
        case .fastOrder(let phone, let prodId, let optName, let optDesc, let optPrice, let povId, let poId):
            return ["telephone": phone, "quantity": 1, "product_id": prodId, "option_value": optName,
                    "option_desc": optDesc, "option_price": optPrice, "povId": povId as Any, "poId": poId as Any]
        default:
            return [:]
        }
    }
    
    var task: Task {
        switch self {
        case .city(let id), .newPost(let id) :
            return .requestCompositeData(bodyData: Data(), urlParameters: ["id": id])
        case .createOrder, .addToAmbar, .fastOrder:
            return .requestParameters(parameters: param, encoding: JSONEncoding())
        default:
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        if let token = UserDataManager.shared.Token {
            return ["Authorization":  token,
            "X-Locale": UserDataManager.shared.ServerLanguage]
        }
        else { return ["X-Locale": UserDataManager.shared.ServerLanguage] }
    }
    
    
}
