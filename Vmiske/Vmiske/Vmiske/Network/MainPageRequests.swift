//
//  MainPageRequests.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/3/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import Moya

enum MainPageRequests {
    
    case getBanners
    
    case getMainCategories
    
    case getSubCategories(id: Int)
    
    case getProductList(id: Int, page: PaginationModel, sort: SortingState? = nil, filters: [Filters.FilterItem] = [])
    
    case getProductDetail(id: Int)
    
    case search(text: String, page: Int)
    
    case createReview(id: Int, author: String, email: String, text: String, rate: Int)
    
}


extension MainPageRequests: TargetType {
    var baseURL: URL {
        return URL(string: ConstantProject.Network.BASE_URL)!
    }
    
    var path: String {
        switch self {
        case .getBanners:
            return "banners/list"
        case .getMainCategories:
            return "categories/list"
        case .getProductDetail(let id):
            return "products/info/\(id)"
        case .getProductList(let id, _, _, _):
            return "products/list/\(id)"
        case .getSubCategories(let id):
            return "categories/list/\(id)"
        case .search(_):
            return "products/list/"
        case .createReview(let id, _, _, _, _):
            return "products/reviews/\(id)"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .createReview:
            return .post
        default:
            return .get
        }
    }
    
    var params: [String: Any] {
        switch self {
        case .getProductList(_, let pagination, let sort, let filters):
            var header = [String : Any]()
            if let sort = sort {
                header["order"] = sort.sortType.rawValue
                header["stype"] = sort.direction.rawValue
            }
            header["limit"] = pagination.limit
            header["page"] = pagination.offset
            header["filter"] = filters.map { "\($0.id)" }.joined(separator: ",")
            return header
            
        case .search(let text, let page):
            return ["word": text, "page": page]
            
        case .createReview(_, let author, let email, let text, let rate):
            return [
                "author" : author,
                "email" : email,
                "text" : text == "" ? " " : text,
                "rating" : rate
            ]
        default:
            return [:]
        }
        
    }
   
    
    var task: Task {
        
        switch self {
        case .getProductList, .search:
            return .requestParameters(parameters: params, encoding: URLEncoding())
        case .createReview:
            return .requestParameters(parameters: params, encoding: JSONEncoding())
        default:
            return .requestPlain
        }
        
    }
    
    var headers: [String : String]? {
        return [
            "X-Locale": UserDataManager.shared.ServerLanguage,
            "Content-Type" : "application/json"
        ]
    }
    
    var sampleData: Data {
           return Data()
       }
}
