//
//  UserRequests.swift
//  Vmiske
//
//  Created by Roman Haiduk on 23.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import Moya

enum UserRequests {
    
    case signUp(firstName: String, lastName: String, phone: String, email: String, password: String, leaderPhone: String?, isGoogle: Bool?, sID: String?)
    
    case signIn(email: String, password: String)
    
    case updateUserData(firstName: String, lastName: String, phone: String, email: String)
    
    case changePassword(oldPassword: String, newPassword: String)
    
    case forgottenPassword(email: String)
    
    case socialSignIn(isGoogle: Bool, id: String, email: String? = nil)
    
    case getUserData
    
}


extension UserRequests: TargetType{
    
    var baseURL: URL {
        switch self {
        case .forgottenPassword:
            return URL(string: "https://vmiske.ua/")!
        default:
            return URL(string: ConstantProject.Network.BASE_URL)!
        }
    }
    
    var path: String {
        switch self {
        case .getUserData:
            return "user/profile"
        case .signUp:
            return "user/signup"
        case .signIn:
            return "user/sigin"
        case .socialSignIn:
            return "user/sigsn"
        case .updateUserData:
            return "user/update"
        case .changePassword:
            return "user/resetpwd"
        case .forgottenPassword:
            return "index.php"
        }
    }
    
    var params: [String: Any] {
        switch self {
            
        case .signUp(let firstName, let lastName, let phone,
                     let email, let password, let leaderPhone, let isGoogle, let sID):
            var par: [String: Any] =  [
                "firstname": firstName, "lastname": lastName, "email": email,
                "telephone": phone, "password": password
            ]
            
            if let leaderPhone = leaderPhone {
                par["vogak"] = leaderPhone
            }
            if let isGoogle = isGoogle, let sID = sID {
                if isGoogle { par["glId"] = sID }
                else { par["fbId"] = sID }
            }
            return par
            
        case .signIn(let email, let password):
            return ["email": email, "password": password]
            
        case .updateUserData(let firstName, let lastName, _, _):
            return ["firstname": firstName, "lastname": lastName]
                   // "telephone": phone, "email": email]
            
        case .changePassword(let oldPassword, let newPassword):
            return ["password": oldPassword, "newpassword": newPassword]
            
        case .socialSignIn(let isGoogle, let id, let email):
            
            var par: [String: Any] =  [
            "id": id, "email": email as Any]

            if isGoogle {
                par["type"] = "glId"
            } else {
                par["type"] = "fbId"
            }
            return par
            
        case .forgottenPassword(let email):
            return ["email": email]
        default:
            return [:]
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getUserData:
            return .get
        default:
            return .post
        }
    }
    
    
    
    var task: Task {
        switch self {
        case .getUserData:
            return .requestPlain
        case .forgottenPassword:
            return .requestCompositeParameters(bodyParameters: params,
                                               bodyEncoding: URLEncoding.httpBody,
                                               urlParameters: ["route": "account/forgotten/forgottenProcessing"])
        default:
            return .requestParameters(parameters: params, encoding: JSONEncoding())
        }
    }
    
    var headers: [String : String]? {
        switch self {
            
        case .getUserData, .changePassword, .updateUserData:
            if let token = UserDataManager.shared.Token {
                return ["Authorization":  token,
                "X-Locale": UserDataManager.shared.ServerLanguage]
            } else { fatalError("Token don't exist")}
        default:
            return ["X-Locale": UserDataManager.shared.ServerLanguage]
        }
    }
    
    var sampleData: Data {
        return Data()
    }
}
