//
//  FontHelper.swift
//  Masterovik
//
//  Created by iOS NotyTeam on 4/22/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

enum FontNames: String {
    case regular = "Rubik-Regular"
    case medium = "Rubik-Medium"
}

enum FontSize: CGFloat {
    case verySmall = 8.0
    case small = 10.0
    case medium = 15.0
    case large = 20.0
    case title = 26.0
}

class FontHelper: NSObject {
    
    static private var fontIncreaseForScreenSize: CGFloat {
        get {
            return trunc((UIScreen.main.bounds.width - 320.0) / 20.0)
        }
    }
    
  
    static func font(type: FontNames, size: FontSize) -> UIFont {
        if let font = UIFont(name: type.rawValue, size: size.rawValue + fontIncreaseForScreenSize) {
            return font
        } else {
            return UIFont.systemFont(ofSize: UIFont.systemFontSize)
        }
    }

}
