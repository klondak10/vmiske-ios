//
//  AppDelegate.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import CoreData
import SideMenuSwift
import FacebookCore
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

   // private let timerForEmptyBasketName = "_basketTimer"
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //FaceBook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        //Google
        GIDSignIn.sharedInstance().clientID = "457494284756-oijhgitc4ogt3pbu1r6d1d7536selghs.apps.googleusercontent.com"
        
        Bundle.setLanguage(UserDataManager.shared.Language)
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? SplashViewController {
            
            window?.rootViewController = vc
            vc.loadViewIfNeeded()
            vc.animationView.play { [unowned self] (finished) in
                if finished {
                    self.configureSideMenu()
                }
            }
            return true
        }
        
        configureSideMenu()
        return true
    }
    
    func setupSideMenu() {
        
        let contentVC = UIViewController.getFromMainStoryboard(id: "baseNavVC")
        var leftVC: UIViewController!
        if UserDataManager.shared.IsAuthorized {   //Проверка на зарегистрирован ли пользователь
            leftVC = UIStoryboard(name: "LeftMenu", bundle: nil)
                .instantiateViewController(withIdentifier: "leftRegVC")
        } else {
            leftVC = UIStoryboard(name: "LeftMenu", bundle: nil)
                .instantiateViewController(withIdentifier: "leftUnregVC")
        }
        window?.rootViewController = SideMenuController(contentViewController: contentVC,
         menuViewController: leftVC)
    }
    
    func reloadControllersOnLanguage() {
        let contentVC = BaseNavigationControllerViewController(rootViewController:
        UIViewController.getFromMainStoryboard(id: "settingsVC"))
               var leftVC: UIViewController!
               if UserDataManager.shared.IsAuthorized {   //Проверка на зарегистрирован ли пользователь settingsVC
                   leftVC = UIStoryboard(name: "LeftMenu", bundle: nil)
                       .instantiateViewController(withIdentifier: "leftRegVC")
                (leftVC as? RegisteredLeftMenuViewController)?.currentMenuIndex = 5
               } else {
                   leftVC = UIStoryboard(name: "LeftMenu", bundle: nil)
                       .instantiateViewController(withIdentifier: "leftUnregVC")
                (leftVC as? UnregisteredLeftMenuViewController)?.currentMenuIndex = 3
               }
               window?.rootViewController = SideMenuController(contentViewController: contentVC,
                menuViewController: leftVC)
    }
    
    
    private func configureSideMenu() {
        SideMenuController.preferences.basic.menuWidth = UIScreen.main.bounds.width * 0.8
        SideMenuController.preferences.basic.enableRubberEffectWhenPanning = true
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        SideMenuController.preferences.basic.supportedOrientations = .portrait
        SideMenuController.preferences.basic.position = .above
        
        setupSideMenu()
    }
    
    func logOut(_ clearTempBasket: Bool = true) {
        UserDataManager.shared.logOut()
        UserDataManager.shared.ProductInBasket = nil
        if clearTempBasket {
        UserDataManager.shared.TempProductInBasket = nil
        }
        window?.rootViewController = UIViewController.getFromAuthStoryboard(id: "authNavVC")
    }
    
//    func clearBasket() {
//
//        if let time = UserDefaults.standard.object(forKey: timerForEmptyBasketName) as? Date,
//            let _ = UserDataManager.shared.ProductInBasket {
//
//            let calendar = Calendar.current
//            let currentTime = Date()
//            var dateComponents = calendar.dateComponents([.day,.hour], from: currentTime)
//
//            dateComponents.day! += 1
//            dateComponents.hour = 1
//            let needTime = calendar.date(from: dateComponents) ?? Date()
//
//            let hoursExist = calendar.dateComponents([.hour], from: time, to: currentTime)
//            let hoursNeed = calendar.dateComponents([.hour], from: needTime, to: currentTime)
//
//            if hoursExist.hour ?? 0 >= hoursNeed.hour ?? 1 {
//                UserDataManager.shared.ProductInBasket = nil
//            }
//        }
//    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if (url.scheme?.hasPrefix("fb"))! {
            return ApplicationDelegate.shared.application(app, open: url, options: options)
        } else {
           return GIDSignIn.sharedInstance().handle(url)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {   }

    func applicationDidEnterBackground(_ application: UIApplication) {  }

    func applicationWillEnterForeground(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }

    func applicationWillTerminate(_ application: UIApplication) {
//        UserDefaults.standard.set(Date(), forKey: timerForEmptyBasketName)
//        UserDefaults.standard.synchronize()
        self.saveContext()
    }

    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
       
        let container = NSPersistentContainer(name: "Vmiske")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

