//
//  ConstantProject.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

enum ConstantProject {
    
    enum Network {
        static let BASE_URL = "http://api.vmiske.ua/"
        static let BASE_IMAGE_URL = "http://vmiske.ua/image/"
    }
    
    enum Language {
        static let UA = "uk"
        static let RU = "ru"
    }
    
    
    struct ProductCollection {
        
        enum  ListCollectionCell {
            static let width = UIScreen.main.bounds.width
            static let height: CGFloat =  UIScreen.main.bounds.width / 375 * 135
        }
        
        enum GridCollectionCell {
            static let width = (UIScreen.main.bounds.width / 2) - 12
            static let height = GridCollectionCell.width * 253 / 178
            static let lineSpace: CGFloat = 6
        }
    }
    
    enum ReviewCollection {
        static let width = UIScreen.main.bounds.width - 22
        static let lineSpacing: CGFloat = 16
    }
    
    enum BasketCollectionViewCell {
        static let width = UIScreen.main.bounds.width - 25
        static let height = BasketCollectionViewCell.width * 137 / 354
    }
    
    struct BasketPart {
        struct BasketBottomView {
            enum Default {
                static let width = UIScreen.main.bounds.width
                static let height = Default.width * 247 / 375
            }
        }
    }
    struct PurchasedPart {
        enum PurchasedCollectionViewCell {
            static let width = UIScreen.main.bounds.width - 25
            static let height = PurchasedCollectionViewCell.width / 375 * 130
        }
        
        enum OnePurchaseTableViewCell {
            static let height = UIScreen.main.bounds.width / 375 * 125
        }
        enum AmbarCollectionViewCell {
            static let width = UIScreen.main.bounds.width - 25
            static let height = AmbarCollectionViewCell.width / 375 * 176
        }
        enum EditAmbarCollectionViewCell {
            static let width = UIScreen.main.bounds.width - 25
            static let height = PurchasedCollectionViewCell.width / 375 * 138
        }
    }
    enum HelpViewController {
        static let width = UIScreen.main.bounds.width - 25
    }
}
