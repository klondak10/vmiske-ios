//
//   LocalizeHelper.h
//  Vmiske
//
//  Created by Roman Haiduk on 11.01.2020.
//  Copyright © 2020 Noty Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+ (void)setLanguage:(NSString *)language;

@end
