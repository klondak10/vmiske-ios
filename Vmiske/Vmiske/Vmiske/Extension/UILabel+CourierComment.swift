//
//  UILabel+CourierComment.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/18/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setCourierComment(delivery: DeliveryReturn?, payment: DeliveryPaymentType?) {
        var _city: String?
        var street: String?
        var appartment: String?
        var comment: String?
        
        switch delivery {
        case .pickUp(let address):
            _city = address
        case .courier(_, let city, let address):
            _city = city.name
            street = address.street! + "," + address.house!
            appartment = address.flat ?? ""
            appartment?.append(contentsOf: " кв.")
            comment = address.comment
        case .newPost(_, let city, let department):
            _city = city.name
            street = department.name
        default: break
        }
        
        font = FontHelper.font(type: .regular, size: .medium)
        numberOfLines = 10
        adjustsFontSizeToFitWidth = true
        text = """
        \(delivery?.description ?? "")
        
        \(_city ?? "")
        \(street ?? "")
        \(appartment ?? "")
        
        \(payment?.name ?? "")
        \(comment ?? "")
"""
    }
}
