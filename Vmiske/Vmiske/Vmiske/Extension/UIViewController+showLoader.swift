//
//  UIViewController+Loader.swift
//  Vmiske
//
//  Created by Roman Haiduk on 25.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit


extension UIViewController {
    
    func showLoader() {
        guard view.viewWithTag(9999) == nil else { return }
        let loader = UIActivityIndicatorView(style: .gray)
        loader.frame = view.bounds
        loader.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        loader.tag = 9999
        
        view.insertSubview(loader, at: 0)
        view.bringSubviewToFront(loader)
        loader.startAnimating()
    }
    
    func removeLoader() {
        if let loader = view.viewWithTag(9999) {
            loader.removeFromSuperview()
        }
    }
    
    @objc
    func showError(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIView {
    
    func showLoader() {
        let loader = UIActivityIndicatorView(style: .gray)
        loader.frame = self.bounds
        loader.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.2)
        loader.tag = 9999
        
        self.insertSubview(loader, at: 0)
        self.bringSubviewToFront(loader)
        loader.startAnimating()
    }
    
    func removeLoader() {
        if let loader = self.viewWithTag(9999) {
            loader.removeFromSuperview()
        }
    }
}
