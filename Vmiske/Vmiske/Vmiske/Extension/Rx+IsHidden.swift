//
//  Rx+IsHidden.swift
//  Vmiske
//
//  Created by Roman Haiduk on 09.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

extension Reactive where Base: UIView {
    /// Bindable sink for `hidden` property.
    public var isHidden: Binder<Bool> {
        return Binder(self.base) { view, hidden in
            view.isHidden = hidden
        }
    }
}
