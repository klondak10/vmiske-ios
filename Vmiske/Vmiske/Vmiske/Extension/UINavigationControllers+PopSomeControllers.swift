//
//  UINavigationControllers+PopSomeControllers.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/17/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func popBackViewControllers(_ count: Int) {
        let viewControllers: [UIViewController] = self.viewControllers
        guard viewControllers.count < count else {
            popToViewController(viewControllers[viewControllers.count - count], animated: true)
            return
        }
    }
}
