//
//  UITextView+Top.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/19/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UITextView {
    
    override open func draw(_ rect: CGRect)
    {
        super.draw(rect)
        setContentOffset(CGPoint.zero, animated: false)
    }
    
}
