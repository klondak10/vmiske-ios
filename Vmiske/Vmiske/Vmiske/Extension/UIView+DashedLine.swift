//
//  UIView+DashedLine.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/16/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIView {
    private static let lineDashPattern: [NSNumber] = [3, 3]
    private static let lineDashWidth: CGFloat = 1.0
    
    func makeDashedBorderLine() {
        let path = CGMutablePath()
        let shapeLayer = CAShapeLayer()
        shapeLayer.lineWidth = UIView.lineDashWidth
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineDashPattern = UIView.lineDashPattern
        path.addLines(between: [CGPoint(x: bounds.minX, y: 1),
                                CGPoint(x: bounds.maxX, y: 1)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}
