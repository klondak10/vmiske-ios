//
//  UIImage+Resize.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIImage {
    
    static func resizeImage(image:UIImage,scaledToSize newSize:CGSize) -> UIImage {
        UIGraphicsBeginImageContext( newSize )
        image.draw(in: CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage?.withRenderingMode(.alwaysOriginal) ?? UIImage()
    }
}
