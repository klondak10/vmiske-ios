//
//  UILabel+Price.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/12/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit


extension UILabel {
    
    func setPrice(_ price: Int) {
     
        let result = getFormattedNumberFrom(price)
        self.text = result
        adjustsFontSizeToFitWidth = true
    }
    
    func setOldPrice(_ oldPrice: Int?) {
        guard let old = oldPrice else { self.isHidden = true; return}
        
        let result = getFormattedNumberFrom(old)
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: result)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
        isHidden = false
        adjustsFontSizeToFitWidth = true
    }
    
    
    func togetherPrice(_ price: Int, with oldPrice: Int?) {
        let withOld = oldPrice != nil
        
        let razomText = "Разом: ".L
        let priceText = getFormattedNumberFrom(price)
        let oldPriceText = withOld ?" \(getFormattedNumberFrom(oldPrice!))" : nil
        
        let rAttributes = NSAttributedString(string: razomText, attributes: [
            NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.3621281683, green: 0.3621373773, blue: 0.3621324301, alpha: 1),
            NSAttributedString.Key.font: FontHelper.font(type: .medium, size: .medium)
            ])
        let pAttributes = NSAttributedString(string: priceText, attributes: [
            NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.8862745098, green: 0.2941176471, blue: 0.09019607843, alpha: 1),
            NSAttributedString.Key.font: FontHelper.font(type: .medium, size: .large)
            ])

        let mAttributes = NSMutableAttributedString(attributedString: rAttributes)
        mAttributes.append(pAttributes)
        if withOld {
            let oAttributes = NSAttributedString(string: oldPriceText!, attributes: [
                NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1),
                NSAttributedString.Key.font: FontHelper.font(type: .medium, size: .small),
                NSAttributedString.Key.strikethroughStyle: 2,
                NSAttributedString.Key.strikethroughColor: #colorLiteral(red: 0.7130386233, green: 0.7130556703, blue: 0.7130464315, alpha: 1)
                ])
            mAttributes.append(oAttributes)
        }
        self.attributedText = mAttributes
    }
    
    private func getFormattedNumberFrom(_ number: Int) -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "uk_UA")
        formatter.numberStyle = .currencyISOCode
        formatter.maximumFractionDigits = 0
        let pText = formatter.string(from: NSNumber(value: number))!
        let result = pText.replacingOccurrences(of: "UAH", with: "грн")
        
        return result
    }
}
