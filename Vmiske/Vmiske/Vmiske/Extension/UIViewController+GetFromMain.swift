//
//  UIViewController+GetFromMain.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/11/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UIViewController {
    
    
    static func getFromMainStoryboard(id: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: id) 
        return vc
    }
    
    static func getFromAuthStoryboard(id: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Autorization", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: id)
        return vc
    }
}
