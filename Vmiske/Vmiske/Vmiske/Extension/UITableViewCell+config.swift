//
//  UITableViewCell+config.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/10/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit
import SDWebImage

extension UITableViewCell {
    
    func config(_ url: URL?, text: String) {
        if let url = url {
            self.imageView?.sd_setImage(with: url)
        }
        self.textLabel?.text = text
        self.accessoryType = .disclosureIndicator
        self.selectionStyle = .none
    }
}
