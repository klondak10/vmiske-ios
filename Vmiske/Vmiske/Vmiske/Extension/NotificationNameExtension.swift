//
//  NotificationNameExtension.swift
//  Vmiske
//
//  Created by Roman Haiduk on 24.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation


extension Notification.Name {
    
    static let UpdatedUserName = Notification.Name.init("UpdatedUserName")
    static let UpdateUserData = Notification.Name.init("UpdateUserData")
    static let UpdateBasketCountItems = Notification.Name.init("UpdateBasketCountItems")
}
