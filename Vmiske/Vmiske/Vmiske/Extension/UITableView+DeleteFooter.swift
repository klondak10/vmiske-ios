//
//  UITableView+DeleteFooter.swift
//  Vmiske
//
//  Created by Roman Haiduk on 9/13/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension UITableView {
    
    func deleteSeparatorOnEmptyCell() {
        tableFooterView = UIView()
    }
}
