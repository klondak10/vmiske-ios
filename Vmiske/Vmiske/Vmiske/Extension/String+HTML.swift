//
//  String+HTML.swift
//  Vmiske
//
//  Created by Roman Haiduk on 31.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func htmlAttributed(family: String?, size: CGFloat, color: UIColor, onLeft: Bool = false) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(size)pt !important;" +
                "text-align:\(onLeft ? "left" : "center")  !important;" +
                "color: #\(color.hexString) !important;" +
                "font-family: \(family ?? "Helvetica") !important;" +
            "}</style> \(self)"

            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }

            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}


extension UIColor {
    var hexString: String {
        
           let colorRef = cgColor.components
           let r = colorRef?[0] ?? 0
           let g = colorRef?[1] ?? 0
           let b = ((colorRef?.count ?? 0) > 2 ? colorRef?[2] : g) ?? 0
           let a = cgColor.alpha

           var color = String(
               format: "#%02lX%02lX%02lX",
               lroundf(Float(r * 255)),
               lroundf(Float(g * 255)),
               lroundf(Float(b * 255))
           )

           if a < 1 {
               color += String(format: "%02lX", lroundf(Float(a)))
           }

           return color
       }
}
