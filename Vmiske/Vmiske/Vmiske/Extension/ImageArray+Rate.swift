//
//  ImageArray+Rate.swift
//  Vmiske
//
//  Created by Roman Haiduk on 08.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import UIKit

extension Array where Element: UIImageView {
    
    func setupReviewEstimate(_ estimate: Int) {
        
        for (_index, element) in self.enumerated() {
            if _index < estimate {
                element.image = #imageLiteral(resourceName: "star-orange")
            } else { element.image = #imageLiteral(resourceName: "star-gray") }
        }
    }
}
