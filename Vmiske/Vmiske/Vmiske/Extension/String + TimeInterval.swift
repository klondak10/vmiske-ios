//
//  String + TimeInterval.swift
//  Vmiske
//
//  Created by Roman Haiduk on 10/4/19.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation

extension String {
    
    func getTimeInterval() -> TimeInterval? {
        let formatter = DateFormatter()
        
        formatter.locale = Locale(identifier: "uk_UA")
        formatter.dateStyle = .long
        return formatter.date(from: self)?.timeIntervalSince1970
    }
}
