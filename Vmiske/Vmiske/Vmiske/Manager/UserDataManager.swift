//
//  UserDataManager.swift
//  Vmiske
//
//  Created by Roman Haiduk on 24.10.2019.
//  Copyright © 2019 Noty Team. All rights reserved.
//

import Foundation


class UserDataManager {
    
    //MARK: - Singelton
    private var _basketAuthProducts: [BasketProduct]?
    private var _basketUnAuthProducts: [Product]?
    
    private var basketAuthProducts: [BasketProduct] {
        get { if _basketAuthProducts == nil {
            _basketAuthProducts = ProductInBasket
            return _basketAuthProducts ?? []
        } else {
            return _basketAuthProducts ?? []
            }
        }
        set { _basketAuthProducts = newValue }
    }
    private var basketUnAuthProducts: [Product] {
        get { if _basketUnAuthProducts == nil {
            _basketUnAuthProducts = TempProductInBasket
            return _basketUnAuthProducts ?? []
        } else {
            return _basketUnAuthProducts ?? []
            }
        }
        set { _basketUnAuthProducts = newValue }
    }
    
    static let shared = UserDataManager()
    private init() {
    }
    
    //MARK: - UserDefaults Paths
    
    private let keyLanguage = "keyLanguage"
    private let keyToken = "keyToken"
    private let keyAuthorized = "keyAuthorized"
    private let keyUserName = "keyUserName"
    private let keyLastName = "keyLastName"
    private let keyPhone = "keyPhone"
    private let keyEmail = "keyEmail"
    private let keyStatus = "keyStatus"
    private let keyLeaderId = "ketLeaderId"
    private let keyLeaderName = "ketLeaderName"
    private let keyBasketList = "keyBasketList"
    private let keyTempBasketList = "keyTempBasketList"
    private let keyBonuses = "keyBonuses"
    private let keyUsingBonuses = "keyUsingBonuses"
    
    
    //MARK: - Properties
    
    var Language: String {
        get {
            if let _lang = UserDefaults.standard.string(forKey: keyLanguage){
                return _lang
            } else {
                let _lang = ConstantProject.Language.UA
                UserDefaults.standard.set(_lang, forKey: _lang)
                return _lang
            }
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyLanguage)
            UserDefaults.standard.synchronize()
        }
    }
    
    var ServerLanguage: String {
        return Language == ConstantProject.Language.UA ? "ua" : "ru"
    }
    
    var Token: String? {
        get {
            if let _token = UserDefaults.standard.string(forKey: keyToken){
                return "token \(_token)"
            } else { return nil }
        }
        set {
            if newValue != nil { IsAuthorized = true }
            else { IsAuthorized = false}
            
            UserDefaults.standard.set(newValue, forKey: keyToken)
            UserDefaults.standard.synchronize()
        }
    }
    
    var IsAuthorized: Bool {
        get {
            return UserDefaults.standard.bool(forKey: keyAuthorized)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyAuthorized)
            UserDefaults.standard.synchronize()
        }
    }
    
    var FirstName: String? {
        get {
            return UserDefaults.standard.string(forKey: keyUserName)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyUserName)
            UserDefaults.standard.synchronize()
            if let newName = newValue {
                NotificationCenter.default.post(name: .UpdatedUserName, object: nil,
                                                userInfo: ["name": newName])
            }
        }
    }
    
    var LastName: String? {
        get {
            return UserDefaults.standard.string(forKey: keyLastName)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyLastName)
            UserDefaults.standard.synchronize()
        }
    }
    
    var Phone: String? {
        get {
            return UserDefaults.standard.string(forKey: keyPhone)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyPhone)
            UserDefaults.standard.synchronize()
        }
    }
    
    var Email: String? {
        get {
            return UserDefaults.standard.string(forKey: keyEmail)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyEmail)
            UserDefaults.standard.synchronize()
        }
    }
    
    var Status: String? {
        get {
            return UserDefaults.standard.string(forKey: keyStatus)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyStatus)
            UserDefaults.standard.synchronize()
        }
    }
    
    var LeaderName: String? {
        get {
            return UserDefaults.standard.string(forKey: keyLeaderName)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyLeaderName)
            UserDefaults.standard.synchronize()
        }
    }
    
    var LeaderId: String? {
        get {
            return UserDefaults.standard.string(forKey: keyLeaderId)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: keyLeaderId)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var Bonuses: String? {
        get {
            return UserDefaults.standard.string(forKey: keyBonuses)
        }
        set {
            if newValue == nil { UserDefaults.standard.set("0", forKey: keyBonuses) }
            else { UserDefaults.standard.set(newValue, forKey: keyBonuses) }
            UserDefaults.standard.synchronize()
        }
    }
    
    var UsingBonuses: String? {
        get {
            return UserDefaults.standard.string(forKey: keyUsingBonuses)
        } set {
           UserDefaults.standard.set(newValue, forKey: keyUsingBonuses)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var ProductInBasket: [BasketProduct]? {
        get {
            
            return try? JSONDecoder().decode([BasketProduct].self,
                                             from: UserDefaults.standard
                                                .data(forKey: keyBasketList) ?? Data())
        }
        set {
            basketAuthProducts = newValue ?? []
            if newValue == nil {
                UserDefaults.standard.set(newValue, forKey: keyBasketList)
                UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: .UpdateBasketCountItems, object: nil,
                userInfo: ["items": 0])
                return
            }
            if let data = try? JSONEncoder().encode(newValue) {
                UserDefaults.standard.set(data, forKey: keyBasketList)
                 UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: .UpdateBasketCountItems, object: nil,
                                                userInfo: ["items": ProductInBasket?.count ?? 0])
            } else { print("Not Save!") }
        }
    }
    
    var TempProductInBasket: [Product]? {
        get {
            return try? JSONDecoder().decode([Product].self,
                                             from: UserDefaults.standard
                                                .data(forKey: keyTempBasketList) ?? Data())
        }
        set {
            basketUnAuthProducts = newValue ?? []
            if newValue == nil {
                UserDefaults.standard.set(newValue, forKey: keyTempBasketList)
                UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: .UpdateBasketCountItems, object: nil,
                userInfo: ["items": 0])
                return
            }
            if let data = try? JSONEncoder().encode(newValue), !IsAuthorized {
                UserDefaults.standard.set(data, forKey: keyTempBasketList)
                 UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: .UpdateBasketCountItems, object: nil,
                                                userInfo: ["items": TempProductInBasket?.count ?? 0])
            } else { print("Not Save!") }
        }
    }
    
    //MARK: Methods
    
    func regUser(_ user: User) {
        FirstName = user.firstName
        LastName = user.lastName
        Phone = user.phone
        Email = user.email
        Status = user.status
        LeaderId = user.vogakId
        LeaderName = user.vogakName
        Bonuses = user.bonus
        
        NotificationCenter.default.post(name: .UpdateUserData, object: nil,
                                        userInfo: ["user": user])
    }
    
    func hasInBasket(_ prod: CategoryProductList.ProductList) -> Bool {
        let _prod = prod.convertToProduct()
        if IsAuthorized {
            return basketAuthProducts.first(where: {$0 == _prod}) != nil
        } else { return basketUnAuthProducts.first(where: {$0 == _prod}) != nil}
    }
    
    func hasInBasket(_ prod: BasketProduct) -> Bool {
        if IsAuthorized {
            return basketAuthProducts.first(where: {$0 == prod}) != nil
        } else { return basketUnAuthProducts.first(where: {$0 == prod}) != nil}
        
    }
    
    func hasInBasket(_ prod: Product) -> Bool{
        if IsAuthorized {
            return basketAuthProducts.first(where: {$0 == prod}) != nil
        } else { return basketUnAuthProducts.first(where: {$0 == prod}) != nil}
        
    }
    
    func logOut() {
        Token = nil
        FirstName = nil
        LastName = nil
        Phone = nil
        Email = nil
        Status = nil
        LeaderName = nil
        UsingBonuses = nil
    }
}
